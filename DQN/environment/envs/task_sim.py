# from asyncio.windows_events import NULL
import math
from operator import is_
import random
import json
import sys
import ast
import numpy as np
from time import sleep
from kafka import KafkaProducer,  KafkaConsumer
from confluent_kafka import Consumer, KafkaError
from kafka.admin import KafkaAdminClient, NewTopic

from collections import deque

from sympy import false


class Task():
    def __init__(self, state, action, task_num, is_done):
        self.state = state
        self.action = action
        self.task_num = task_num
        self.is_done = is_done


class TaskDistribution:

    def __init__(self):
        # self.MEMORY_SIZE = 500
        self.current_step = 0
        self.task_count = 0
        self.reward_counter = 0
        self.incomplete_task = []
        self.temp_memory = []
        bootstrap_servers = ['127.0.0.1:9092']
        admin_client = KafkaAdminClient(bootstrap_servers=bootstrap_servers)
        value_serializer = lambda v: json.dumps(v).encode('utf-8')
        topics = ['state', 'action', 'reward']
        self.create_topics(topics, admin_client)

        settings = {
            'bootstrap.servers': 'localhost:9092',
            'group.id': 'mygroup',
            'enable.auto.commit': True,
            'session.timeout.ms': 6000,
            'default.topic.config': {'auto.offset.reset': 'earliest'},
        }
        self.consumer_state = Consumer(settings)
        self.consumer_state.subscribe(['state'])
        self.consumer_reward = Consumer(settings)
        self.consumer_reward.subscribe(['reward'])
        self.producer = KafkaProducer(bootstrap_servers=bootstrap_servers, api_version=(0, 11, 5), value_serializer=value_serializer)

    def create_topics(self, topic_names, admin_client):

        topic_list = []
        self.delete_topics(topic_names, admin_client)
        sleep(15)
        for topic in topic_names:
            print('Topic : {} added '.format(topic))
            topic_list.append(NewTopic(name=topic, num_partitions=1, replication_factor=1))
        try:
            if topic_list:
                admin_client.create_topics(new_topics=topic_list, validate_only=False)
                print("Topic Created Successfully")
            else:
                print("Topic Exist")
        except Exception as e:
            print(e)

    def delete_topics(self, topic_names , admin_client):
        try:
            admin_client.delete_topics(topics=topic_names)
            print("Topic Deleted Successfully")
        except Exception as e:
            print(e)

    # def update_incompletetask(self, task_id):
    #     self.incomplete_task.append(task_id)

    def observe_received(self):
        # call random_obs and process the obs here
        # later instead of random obs receive from the simulator

        # self.task_count += 1
        # # init is_done for the new state
        # # is_done = False
        # is_done = 0

        # print("is_done init: ", is_done)

        # # create new state and parse it as JSON object
        # task = {"taskNum": self.task_count, "task_id": self.task_count,
        #         "task_length": 0, "task_latency": 0, "task_mobility": 0, "done": is_done}
        # VMs = {"cpu_utilization": 0, "CPU_Mips": 0, "Remaining_battery": 0,
        #        "destination_mobility": 0, "vm_id": 0, "offloadable": 0}

        # new_state = self.random_obs()

        # new_state = {
        #     #  "FuzzyTask":[self.task_count, self.task_count, 0, 0, 0, is_done],
        #     #  "FuzzyVMs": [[3, 3, 1, 2, 32, 1], [3, 3, 1, 2, 33, 0], [3, 3, 1, 2, 34, 0], [3, 3, 1, 2, 35, 0], [3, 3, 1, 2, 36, 0], [3, 3, 1, 2, 37, 0], [3, 3, 1, 2, 38, 0], [3, 3, 1, 2, 39, 0], [3, 3, 1, 2, 40, 0], [3, 3, 1, 2, 41, 0], [3, 3, 1, 2, 42, 0], [3, 3, 1, 2, 43, 0], [3, 3, 1, 2, 44, 0], [3, 3, 1, 2, 45, 0], [3, 3, 1, 2, 46, 1], [3, 3, 1, 2, 47, 0], [3, 3, 1, 2, 48, 0], [3, 3, 1, 2, 49, 0], [3, 3, 1, 2, 50, 0], [3, 3, 1, 2, 51, 0], [3, 3, 1, 3, 52, 0], [3, 3, 1, 3, 53, 0], [3, 3, 1, 3, 54, 1], [3, 3, 1, 3, 55, 0], [3, 3, 1, 3, 56, 0], [3, 3, 1, 3, 57, 0], [3, 3, 1, 3, 58, 0], [3, 3, 1, 3, 59, 1], [3, 3, 1, 3, 60, 1], [3, 3, 1, 3, 61, 0], [3, 1, 1, 3, 62, 0], [3, 1, 1, 3, 63, 0], [3, 1, 1, 3, 64, 0], [3, 1, 1, 3, 65, 0], [3, 1, 1, 3, 66, 0], [3, 1, 1, 3, 67, 0], [3, 1, 1, 3, 68, 0], [3, 1, 1, 3, 69, 1], [3, 1, 1, 3, 70, 0], [3, 1, 1, 3, 71, 0]],
        #     "ActualVMs": [[0.0, 25000.0, 100.0, 2.0, 32.0, 1.0], [0.0, 25000.0, 100.0, 2.0, 33.0, 0.0], [0.0, 25000.0, 100.0, 2.0, 34.0, 0.0], [0.0, 25000.0, 100.0, 2.0, 35.0, 0.0], [0.0, 25000.0, 100.0, 2.0, 36.0, 0.0], [0.0, 25000.0, 100.0, 2.0, 37.0, 0.0], [0.0, 25000.0, 100.0, 2.0, 38.0, 0.0], [0.0, 25000.0, 100.0, 2.0, 39.0, 0.0], [0.0, 25000.0, 100.0, 2.0, 40.0, 0.0], [0.0, 25000.0, 100.0, 2.0, 41.0, 0.0], [0.0, 25000.0, 100.0, 2.0, 42.0, 0.0], [0.0, 25000.0, 100.0, 2.0, 43.0, 0.0], [0.0, 25000.0, 100.0, 2.0, 44.0, 0.0], [0.0, 25000.0, 100.0, 2.0, 45.0, 0.0], [0.0, 25000.0, 100.0, 2.0, 46.0, 1.0], [0.0, 25000.0, 100.0, 2.0, 47.0, 0.0], [0.0, 25000.0, 100.0, 2.0, 48.0, 0.0], [0.0, 25000.0, 100.0, 2.0, 49.0, 0.0], [0.0, 25000.0, 100.0, 2.0, 50.0, 0.0], [0.0, 25000.0, 100.0, 2.0, 51.0, 0.0], [0.0, 16000.0, 100.0, 3.0, 52.0, 0.0], [0.0, 16000.0, 100.0, 3.0, 53.0, 0.0], [0.0, 16000.0, 100.0, 3.0, 54.0, 1.0], [0.0, 16000.0, 100.0, 3.0, 55.0, 0.0], [0.0, 16000.0, 100.0, 3.0, 56.0, 0.0], [0.0, 16000.0, 100.0, 3.0, 57.0, 0.0], [0.0, 16000.0, 100.0, 3.0, 58.0, 0.0], [0.0, 16000.0, 100.0, 3.0, 59.0, 1.0], [0.0, 16000.0, 100.0, 3.0, 60.0, 1.0], [0.0, 16000.0, 100.0, 3.0, 61.0, 0.0], [0.0, 110000.0, 100.0, 3.0, 62.0, 0.0], [0.0, 110000.0, 100.0, 3.0, 63.0, 0.0], [0.0, 110000.0, 100.0, 3.0, 64.0, 0.0], [0.0, 110000.0, 100.0, 3.0, 65.0, 0.0], [0.0, 110000.0, 100.0, 3.0, 66.0, 0.0], [0.0, 110000.0, 100.0, 3.0, 67.0, 0.0], [0.0, 110000.0, 100.0, 3.0, 68.0, 0.0], [0.0, 110000.0, 100.0, 3.0, 69.0, 1.0], [0.0, 110000.0, 100.0, 3.0, 70.0, 0.0], [0.0, 110000.0, 100.0, 3.0, 71.0, 0.0]],
        #     "ActualTask": [self.task_count, self.task_count, 0, 0, 0, is_done]
        # }

        # state = json.dumps(new_state)

        #receiving observation from simulator
        try:
            new_obs = self.consumer_state.poll()
        except KeyboardInterrupt:
            sys.exit()

        new_state = json.loads(new_obs.value().decode('utf-8'))
        
        is_done = ast.literal_eval(new_state["ActualTask"])[5]
        # print("obs_received is_done: ", is_done)

        state = json.dumps(new_state)
        # print("state received:", state)
        return state, is_done

    # def remove(self, task_id):
    #     self.incomplete_task.remove(task_id)

    def next_state(self):
        state, is_done = self.observe_received()
        state_data = json.loads(state)
        # print("Created new state with TaskID: ", (ast.literal_eval(state_data["ActualTask"])[1]))
        # processing and adding to temp memory
        task_num = ast.literal_eval(state_data["ActualTask"])[0]
        # print("Updating Temp Memory on task_num: ", task_num)
        self.temp_memory.append(Task(state, None, task_num, is_done))

        return state, task_num, is_done

    def take_action(self, action, task_num):
        # print("taking action...")
        for task in self.temp_memory:
            if task.task_num == task_num:
                state = task.state
                selected_action = -1

        offloadable = self.check_action(state, action)
        # print("Offloadability: ", offloadable)
        state = json.loads(state)
        # final_score = np.multiply(action, offloadables)

        # if len(final_score[np.nonzero(final_score)]) > 0:
        #     max_value = np.max(final_score[np.nonzero(final_score)])
        #     max_index = np.where(final_score == max_value)[0][0]
        #     state = json.loads(state)
        #     selected_action = ast.literal_eval(state["FuzzyVMs"])[max_index][4]
        if offloadable:
            selected_action = action
            # print("selected action: ", selected_action)
            # send action to the simulator
            action_json = {'taskID': int(ast.literal_eval(state["ActualTask"])[1]), 'action': str(selected_action)}
            ack = self.producer.send('action', json.dumps(action_json))
            metadata = ack.get()
            # print("action is sent to topic ", metadata.topic)

            # process action and add to temp memo
            for task in self.temp_memory:
                if task.task_num == task_num:
                    # print("Updating action in temp memory for TaskID: ", ast.literal_eval(state["ActualTask"])[1])
                    task.action = selected_action
                    break
        else:
            vms = ast.literal_eval(state["ActualVMs"])
            selected_action = 0
            for vm in vms:
                if vm[5] == 0.0:
                    selected_action += 1
                if vm[5] == 1.0:
                    # print("selected action: ", selected_action)
                    break

            # send action to the simulator
            action_json = {'taskID': int(ast.literal_eval(state["ActualTask"])[1]), 'action': str(selected_action)}
            ack = self.producer.send('action', json.dumps(action_json))
            metadata = ack.get()
            # print("action is sent to topic ", metadata.topic)

            # process action and add to temp memo
            for task in self.temp_memory:
                if task.task_num == task_num:
                    # print("Updating action in temp memory for TaskID: ", ast.literal_eval(state["ActualTask"])[1])
                    task.action = selected_action
                    break

        return selected_action, task_num 

    def check_action(self, state, action):
        load_state = json.loads(state)
        
        # vms = ast.literal_eval(load_state["ActualVMs"])
        # print(vms)
        # offloadables = []

        # for i in range(action):
        #     offloadables.append(vms[i][5])
        # # print("offloadable matrix: ", offloadables)
        # return offloadables

        # check offloadability
        # print("Action from CheckAction: ", action)
        if ast.literal_eval(load_state["ActualVMs"])[action][5] == 0.0:
            return False
        else:
            return True

    #when memorysize is reached, set done to 1
    # def is_done(self):
    #     done = 0
    #     if self.task_count == self.MEMORY_SIZE:
    #         for task in self.temp_memory:
    #             mem_task = json.loads(task.state)
    #             if task.task_num == self.MEMORY_SIZE:
    #                 done = 1
    #                 mem_task["ActualTask"][5] = done
    #         print("Actual Task from is_done: ", mem_task["ActualTask"])           
    #     return done
       
                
    def step_forward(self, action, task_num):

        self.take_action(action, task_num)
        self.task_count += 1
        self.current_step = self.current_step + 1
        # create new state
        state_, task_num_, is_done_ = self.next_state()
        
        # # update temp_memory 
        # for task in self.temp_memory:
        #     if task.task_num == task_num:
        #         state_data = json.loads(task.state)
        #         self.update_incompletetask(ast.literal_eval(state_data["ActualTask"])[0])
        #         break

        return state_, task_num_, is_done_

    # def random_obs(self):
    #     new_state = []
    #     return new_state

    # def random_reward(self):
    #     reward = None
    #     task_id = None
        
    #     received = random.choices([True, False], [0.9, 0.1])
    #     print("random reward - received:", received[0])

    #     if received[0]:

    #         # get a random incomplete task to assign reward to
    #         task_id = random.sample(self.incomplete_task, 1)[0]
    #         print("random reward task_id: ", task_id)

    #         for task in self.temp_memory:
    #             mem_task = json.loads(task.state)
    #             if task_id == mem_task["ActualTask"][0]:
    #                 print("state for new reward: ", task.state)
    #                 print("action for new reward: ", task.action)

    #                 if self.check_action(task.state, task.action):
    #                     reward = random.choices([0, 1], [0.3, 0.7])[0]
    #                 else:
    #                     reward = 0

    #                 self.incomplete_task.remove(task_id)

    #     print("Random reward of {} given to TaskID {} ".format(reward, task_id))
    #     return reward, task_id

    def reward_received(self):
        # reward, task_id = self.random_reward()
        # return reward, task_id
        # pop up a reward message from reward topic
        reward = 0
        task_id = None
        task_num = None
        # print("reward received")
        try:
            new_reward = self.consumer_reward.poll(0.001)
            if new_reward is not None:
                # print('Reward Received message: {0}'.format(new_reward.value()))
                # print('new_reward in obs_receive ', new_reward.values())
                # print("type reward val", type(new_reward.value()))
                reward_json = json.loads(new_reward.value().decode('utf-8'))
                task_id = reward_json["taskID"]
                task_num = int(reward_json["taskNum"])
                reward = reward_json["reward"]
                # print("Rewards for task_id: ", task_id)
                # print("Rewards for task_num: ", task_num)
                # print("Rewards received: ", reward)
                self.reward_counter = self.reward_counter + 1
                # print("self.reward_counter: ", self.reward_counter)
        except KeyboardInterrupt:
            sys.exit()
        # reward, task_id = self.random_reward()
        return reward, task_id, task_num
    
