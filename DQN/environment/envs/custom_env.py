# import gym
from environment.envs.task_sim import TaskDistribution

class CustomEnv():

    def __init__(self):
        self.task = TaskDistribution()
        # #number of devices
        self.action_space = 40
        self.observation_space = (41, 6)
        # self.observation_space = (246,)

    def reset(self):
        self.task.current_step = 0
        self.task.task_count = 0
        self.task.reward_counter = 0
        self.task.incomplete_task = []
        self.task.temp_memory = []
        return self.task.next_state()
    
    def step(self, action, task_num):
        state, task_num, done = self.task.step_forward(action, task_num)
        r_reward, r_task_id, r_task_num= self.task.reward_received()
          
        # while reward == None:
        #     reward, task_id = self.task.reward_received()
        
        # done = self.task.is_done() 
           
        return state, task_num, done, r_reward, r_task_id, r_task_num
    
    def render(self, mode = 'human', close=False):
        # Render the environment to the screen

        print(f'Step: {self.task.current_step}')
        print(f'Task Count: {self.task.task_count}')
        print(f'Reward: {self.task.reward_counter}')
        # print(f'Memory: {self.task.temp_memory}')
    
