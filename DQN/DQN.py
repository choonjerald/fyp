import stringprep
from keras.models import Sequential
from keras.layers import Dense, Dropout, Conv2D, MaxPooling2D, Activation, Flatten, InputLayer
from keras.callbacks import TensorBoard
from collections import deque
from sympy import Q
from keras.optimizers import adam_v2
import os
import json
import pandas as pd
from itertools import chain
import ast 

from environment.envs import CustomEnv
# import gym

import tensorflow as tf
import numpy as np

import time
import random

from tqdm import tqdm 


REPLAY_MEMORY_SIZE = 50_000 # How many last steps to keep for model training
MIN_REPLAY_MEMORY_SIZE = 1_000 # Minimum number of steps in a memory to start training
MODEL_NAME = "256x3 64 40" 
MINIBATCH_SIZE = 100 # How many steps (samples) to use for training
DISCOUNT = 0.99
MIN_REWARD = 200 # For model save 
MEMORY_FRACTION = 0.20
UPDATE_TARGET_EVERY = 1 # Terminal states (end of run)

# # Environment settings
# EPISODES = 100

# Exploration settings
epsilon = 0.99 # not a constant,going to be decayed
EPSILON_DECAY = 0.9988
MIN_EPSILON = 0.1

# stats settings
AGGREGATE_STATS_EVERY = 200 # steps
SHOW_PREVIEW = False

q_counter = 0

ep_rewards = [200]

# Own Tensorboard class
class ModifiedTensorBoard(TensorBoard):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.step = 1
        self.writer = tf.summary.create_file_writer(self.log_dir)
        self._log_write_dir = self.log_dir


    def set_model(self, model):
        self.model = model

        self._train_dir = os.path.join(self._log_write_dir, 'train')
        self._train_step = self.model._train_counter

        self._val_dir = os.path.join(self._log_write_dir, 'validation')
        self._val_step = self.model._test_counter

        self._should_write_train_graph = False


    def on_epoch_end(self, epoch, logs=None):
        self.update_stats(**logs)


    def on_batch_end(self, batch, logs=None):
        pass


    def on_train_end(self, _):
        pass


    def update_stats(self, **stats):
        with self.writer.as_default():
            for key, value in stats.items():
                tf.summary.scalar(key, value, step=self.step)
                self.writer.flush()

env = CustomEnv()

class DQNAgent:
    def __init__(self):

        #main model (gets trained every step)
        self.model = self.create_model()

        # Target model (used to predict against every step) 
        # this target model allows for more consistency
        self.target_model = self.create_model()
        self.target_model.set_weights(self.model.get_weights())

        self.replay_memory = deque(maxlen=REPLAY_MEMORY_SIZE)
        self.tensorboard = ModifiedTensorBoard(log_dir=f"logs/{MODEL_NAME}-{int(time.time())}")
        self.target_update_counter = 0
        
    def create_model(self):
        model = Sequential()
        model.add(InputLayer(input_shape=env.observation_space))
        model.add(Activation("relu"))
        model.add(Dropout(0.2))

        model.add(Dense(128))
        model.add(Activation("relu"))

        model.add(Dense(128))
        model.add(Activation("relu"))
        
        model.add(Dense(env.action_space, activation="linear"))
        optimizer = adam_v2.Adam(learning_rate=0.001)
        model.compile(loss="mse", optimizer=optimizer, metrics=['accuracy'])
        
        return model

    # Adds step's data to a memory replay array
    # (observation space, action, reward, new observation space, done)  
    def update_replay_memory(self ,transition):
        self.replay_memory.append(transition)

    def train(self, terminal_state, step):
        if len(self.replay_memory) < MIN_REPLAY_MEMORY_SIZE:
            return
        
         # Get a minibatch of random samples from memory replay table
        # print("Current replay_memory length: ", len(self.replay_memory))
        
        minibatch = random.sample(self.replay_memory, MINIBATCH_SIZE)
        
        # Get current states from minibatch, then query NN model for Q values
        
        # current_states = np.array([transition[0] for transition in minibatch])
        # print(current_states)
        # print(type(current_states)) 
        
            
        # current_states_values = np.array(current_states_values)
        
        current_states_ActualVMs = np.array([transition[0] for transition in minibatch])     
        current_states_ActualTask = np.array([transition[1] for transition in minibatch]) 
        
        # print(current_states_ActualVMs)
        # print(current_states_ActualTask)
        
        # print("Type of current_states_ActualVMs: ", type(current_states_ActualVMs))
        # print("Type of current_states_ActualTask: ",type(current_states_ActualTask))
        
        current_states_values = []
        i = 0
        for task in current_states_ActualTask:
            temp = []
            temp.append(task)
            for vm in current_states_ActualVMs[i]:
                temp.append(vm)
            current_states_values.append(temp)
            i = i + 1
            
        # print(current_states_values)  
        # print(np.array(current_states_values).shape)
        
        # print("#############RUNNING PREDICTION ON CURRENT VALUES#############")
        current_qs_list = self.model.predict(np.array(current_states_values))

        # new_current_ActualVMs = np.array([transition[8] for transition in minibatch])
        # new_current_ActualTask = np.array([transition[9] for transition in minibatch])
        
        # Get future states from minibatch, then query NN model for Q values
        # When using target network, query it, otherwise main network should be queried
        new_states_ActualVMs = np.array([transition[4] for transition in minibatch])
        new_states_ActualTask = np.array([transition[5] for transition in minibatch])
        
        # print(new_states_ActualVMs)
        # print(new_states_ActualTask)
        
        # print("Type of new_states_ActualVMs: ", type(new_states_ActualVMs))
        # print("Type of new_states_ActualTask: ",type(new_states_ActualTask))
        
        new_states_values = []
        i = 0
        for task in new_states_ActualTask:
            temp = []
            temp.append(task)
            for vm in new_states_ActualVMs[i]:
                temp.append(vm)
            new_states_values.append(temp)
            i = i + 1
            
        # print(new_states_values)
        # print("#############RUNNING PREDICTION FOR FUTURE VALUES#############")
        future_qs_list = self.target_model.predict(np.array(new_states_values))

        X = [] #input from environment
        y = [] #action

         # Now we need to enumerate our batches
        for index, ((current_ActualVMs, current_ActualTask, action, reward, new_ActualVMs, new_ActualTask, done)) in enumerate(minibatch):

            # If not a terminal state, get new q from future states, otherwise set it to 0
            # almost like with Q Learning, but we use just part of equation here
            if not done:
                max_future_q = np.max(future_qs_list[index])
                new_q = reward + DISCOUNT * max_future_q
            else:
                new_q = reward

            # Update Q value for given state
            current_qs = current_qs_list[index]
            current_qs[action] = new_q

            # And append to our training data
            current_states_ActualVMs = np.array(current_ActualVMs)     
            current_states_ActualTask = np.array([current_ActualTask])   
            
            # print(current_states_ActualVMs)
            # print(current_states_ActualTask)
            
            current_states = []

            for task in current_states_ActualTask:
                current_states.append(task)
                for vm in current_states_ActualVMs:
                    current_states.append(vm)    
            
            X.append(current_states)
            y.append(current_qs)
            
        # Fit on all samples as one batch, log only on terminal state
        self.model.fit(np.array(X), np.array(y), batch_size=MINIBATCH_SIZE, verbose=0, shuffle=False, callbacks=[self.tensorboard] if terminal_state else None)

        # Update target network counter every episode
        if terminal_state:
            # print("##################ADDING 1 TO TARGET UPDATE COUNTER########################")
            self.target_update_counter += 1

        if self.target_update_counter > UPDATE_TARGET_EVERY:
            self.target_model.set_weights(self.model.get_weights())
            self.target_update_counter = 0

    def get_qs(self, state):
        # q_value = self.model.predict(np.array(state).reshape(-1, 41, 6))[0]
        state = np.array(state).reshape(-1, 41, 6)
        # print("-----------------------------------------------")
        # print("Q-Value obtained form Q-table: ", q_value)
        # print("Q-Value[0] array size: ", len(q_value[0]))
        # print("-----------------------------------------------")
        # print("-----------------------------------------------")
        # print("Length of state[0]: ", len(state[0]))
        # print("state: ", state)
        # print(len(state))
        q_value = self.model.predict(state)[0]
        # print("All Q-Value: ", q_value)
        i = 0
        action = 0
        while i < 40:
            if action < np.argmax(q_value[i]):
                action = np.argmax(q_value[i])

            i += 1
        # print("Selected Action from Q-Table: ", action)
        # print("-----------------------------------------------")
        
        return action
        # return self.model.predict(np.array(state).reshape(-1, 41, 6))[0][0]
        # return self.model.predict(np.array(state))[0]

agent = DQNAgent()

episode_reward = 0
step = 1
tensorboard_step = 1

current_state, task_num, is_done = env.reset()

# Reset flag and start iterating until episode ends
done = False
while not done:

    # Decay epsilon
    if epsilon > MIN_EPSILON:
        epsilon *= EPSILON_DECAY
        epsilon = max(MIN_EPSILON, epsilon)

    # This part stays mostly the same, the change is to query a model for Q values
    if np.random.random() > epsilon:
        # Get action from Q table
        dict_current_state = json.loads(current_state)
            
        current_ActualVMs = np.asarray(ast.literal_eval(dict_current_state['ActualVMs'])).astype("int")
        current_ActualTask = np.asarray([ast.literal_eval(dict_current_state['ActualTask'])]).astype("int")
            
        data = []
        for task in current_ActualTask:
            data.append(task)
            for vm in current_ActualVMs:
                data.append(vm)  
                    
        # action = np.argmax(agent.get_qs(np.array(data)))
        action = agent.get_qs(np.array(data))

        # print("Action from Q-Table DQN: ", action)
        q_counter = q_counter + 1
    else:
        # Get random action
        action = np.random.randint(0, env.action_space)
        # print("Random Action from DQN: ", action)

            
    new_state, task_num, done, r_reward, r_task_id, r_task_num = env.step(action, task_num)
    
    # Transform new continous state to new discrete state and count reward
    if r_task_id is not None:
        episode_reward += r_reward
            
    dict_current_state = json.loads(current_state)
    current_ActualVMs = np.asarray(ast.literal_eval(dict_current_state['ActualVMs'])).astype("int")
    current_ActualTask = np.asarray(ast.literal_eval(dict_current_state['ActualTask'])).astype("int")
            
    dict_new_state = json.loads(new_state)
    new_ActualVMs = np.asarray(ast.literal_eval(dict_new_state['ActualVMs'])).astype("int")
    new_ActualTask = np.asarray(ast.literal_eval(dict_new_state['ActualTask'])).astype("int")
    
    # Every step we update replay memory and train main network
    agent.update_replay_memory((current_ActualVMs, current_ActualTask, action, r_reward, new_ActualVMs, new_ActualTask, done))
    agent.train(done, step)
        
    current_state = new_state
    step += 1

    if env.task.task_count % 1000 == 0:
        agent.tensorboard.step = tensorboard_step
        tensorboard_step += 1
        # Append episode reward to a list and log stats (every given number of episodes)
        ep_rewards.append(episode_reward)
        average_reward = sum(ep_rewards[-AGGREGATE_STATS_EVERY:])/len(ep_rewards[-AGGREGATE_STATS_EVERY:])
        min_reward = min(ep_rewards[-AGGREGATE_STATS_EVERY:])
        max_reward = max(ep_rewards[-AGGREGATE_STATS_EVERY:])
        agent.tensorboard.update_stats(reward_avg=average_reward, reward_min=min_reward, reward_max=max_reward, epsilon=epsilon)

        # Save model, but only when min reward is greater or equal a set value
        if min_reward >= MIN_REWARD:
            agent.model.save(f'models/{MODEL_NAME}__{max_reward:_>7.2f}max_{average_reward:_>7.2f}avg_{min_reward:_>7.2f}min__{int(time.time())}.model')
        
        env.render('human')

        step = 0
        episode_reward = 0

#action for last task
if done == 1:
    print("All task completed, is_done: ", done)

    # Decay epsilon
    if epsilon > MIN_EPSILON:
        epsilon *= EPSILON_DECAY
        epsilon = max(MIN_EPSILON, epsilon)

    # This part stays mostly the same, the change is to query a model for Q values
    if np.random.random() > epsilon:
        # Get action from Q table
        dict_current_state = json.loads(current_state)
            
        current_ActualVMs = np.asarray(ast.literal_eval(dict_current_state['ActualVMs'])).astype("int")
        current_ActualTask = np.asarray([ast.literal_eval(dict_current_state['ActualTask'])]).astype("int")
            
        data = []
        for task in current_ActualTask:
            data.append(task)
            for vm in current_ActualVMs:
                data.append(vm)  
                    
        # action = np.argmax(agent.get_qs(np.array(data)))
        action = agent.get_qs(np.array(data))

        q_counter = q_counter + 1
    else:
        # Get random action
        action = np.random.randint(0, env.action_space)
    
    env.task.take_action(action, task_num)

    # Append episode reward to a list and log stats (every given number of episodes)
    ep_rewards.append(episode_reward)
    average_reward = sum(ep_rewards[-AGGREGATE_STATS_EVERY:])/len(ep_rewards[-AGGREGATE_STATS_EVERY:])
    min_reward = min(ep_rewards[-AGGREGATE_STATS_EVERY:])
    max_reward = max(ep_rewards[-AGGREGATE_STATS_EVERY:])
    agent.tensorboard.update_stats(reward_avg=average_reward, reward_min=min_reward, reward_max=max_reward, epsilon=epsilon)

    env.render('human')
    agent.model.save(f'models/{MODEL_NAME}__{max_reward:_>7.2f}max_{average_reward:_>7.2f}avg_{min_reward:_>7.2f}min__{int(time.time())}.model')



        

# # Iterate over episodes
# for episode in tqdm(range(1, EPISODES + 1), ascii=True, unit='episodes'):

#     # Update tensorboard step every episode
#     agent.tensorboard.step = episode

#     # Restarting episode - reset episode reward and step number
#     episode_reward = 0
#     step = 1

#     # print("resetting environment for new episode: ", reset_counter)
#     # Reset environment and get initial state
#     current_state, task_num, is_done = env.reset()
        
#     # Reset flag and start iterating until episode ends
#     done = False
#     while not done:

#         # This part stays mostly the same, the change is to query a model for Q values
#         if np.random.random() > epsilon:
#             # Get action from Q table
#             # print("##############GETTING ACTION FROM Q TABLE###############")
#             dict_current_state = json.loads(current_state)
            
#             current_ActualVMs = np.asarray(ast.literal_eval(dict_current_state['ActualVMs'])).astype("int")
#             current_ActualTask = np.asarray([ast.literal_eval(dict_current_state['ActualTask'])]).astype("int")
            
#             data = []
#             for task in current_ActualTask:
#                 data.append(task)
#                 for vm in current_ActualVMs:
#                     data.append(vm)  
                    
#             action = np.argmax(agent.get_qs(np.array(data)))
#             # print("Action Selected: ", action)
#             q_counter = q_counter + 1
#         else:
#             # print("##############GETTING RANDOM ACTION###############")
#             # Get random action
#             action = np.random.randint(0, env.action_space)
#             # print("Action Selected: ", action)
            
#         new_state, task_num, done, r_reward, r_task_id, r_task_num = env.step(action, task_num)
#         # print("----------------------------Q-COUNTER: {}----------------------------".format(q_counter))

#         # Transform new continous state to new discrete state and count reward
#         if r_task_id is not None:
#             episode_reward += r_reward

#         if SHOW_PREVIEW and not episode % AGGREGATE_STATS_EVERY:
#             env.render()
            
#         dict_current_state = json.loads(current_state)
#         current_ActualVMs = np.asarray(ast.literal_eval(dict_current_state['ActualVMs'])).astype("int")
#         current_ActualTask = np.asarray(ast.literal_eval(dict_current_state['ActualTask'])).astype("int")
            
#         dict_new_state = json.loads(new_state)
#         new_ActualVMs = np.asarray(ast.literal_eval(dict_new_state['ActualVMs'])).astype("int")
#         new_ActualTask = np.asarray(ast.literal_eval(dict_new_state['ActualTask'])).astype("int")
        
#         # print("##############CURRENT STATE#################")
#         # print(current_state)
#         # print("----------------NEW STATE------------------")
#         # print(dict_new_state)
#         # print("############################################\n")
        
#         # Every step we update replay memory and train main network
#         agent.update_replay_memory((current_ActualVMs, current_ActualTask, action, r_reward, new_ActualVMs, new_ActualTask, done))
#         # agent.update_replay_memory((current_state, action, reward, new_state, done))
#         agent.train(done, step)
        
#         current_state = new_state
#         step += 1


    # # Append episode reward to a list and log stats (every given number of episodes)
    # ep_rewards.append(episode_reward)
    # if not episode % AGGREGATE_STATS_EVERY or episode == 1:
    #     average_reward = sum(ep_rewards[-AGGREGATE_STATS_EVERY:])/len(ep_rewards[-AGGREGATE_STATS_EVERY:])
    #     min_reward = min(ep_rewards[-AGGREGATE_STATS_EVERY:])
    #     max_reward = max(ep_rewards[-AGGREGATE_STATS_EVERY:])
    #     agent.tensorboard.update_stats(reward_avg=average_reward, reward_min=min_reward, reward_max=max_reward, epsilon=epsilon)

    #     # Save model, but only when min reward is greater or equal a set value
    #     if min_reward >= MIN_REWARD:
    #         agent.model.save(f'models/{MODEL_NAME}__{max_reward:_>7.2f}max_{average_reward:_>7.2f}avg_{min_reward:_>7.2f}min__{int(time.time())}.model')

    # # Decay epsilon
    # if epsilon > MIN_EPSILON:
    #     epsilon *= EPSILON_DECAY
    #     epsilon = max(MIN_EPSILON, epsilon)
        