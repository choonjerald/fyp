package MyWork;

import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.TopicPartition;

import java.util.Collection;
import java.util.Collections;
import java.util.Properties;

public class ProducerConsumer {
    public static String brokers = "localhost:9092";

    public ProducerConsumer() {
        super();
    }

    public static String consumer(String topic) {
        //run the program with args: producer/consumer broker:port
        String groupId = "my-group";
        Properties props = new Properties();
        String action = null;
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "1");
        KafkaConsumer < byte[], byte[] > consumer = new KafkaConsumer < > (props);
        TestConsumerRebalanceListener rebalanceListener = new TestConsumerRebalanceListener();
        consumer.subscribe(Collections.singletonList(topic), rebalanceListener);
        final ConsumerRecords < byte[], byte[] > consumerRecords =
                consumer.poll(1000);
        for (ConsumerRecord<byte[], byte[]> record : consumerRecords){
            action = String.valueOf(record.value());
            //String timeStamp = String.valueOf(record.key());
        }
//        consumerRecords.forEach(record -> {
//            System.out.printf("Consumer Record:(%d, %s, %d, %d)\n",
//                    record.key(), record.value(),
//                    record.partition(), record.offset());
//        });

        consumer.commitAsync();
        consumer.close();
//        System.out.println("DONE");
        return action;
    }
    public static String producer(String data, String topic) {
        Properties props = new Properties();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers);
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        props.put(ProducerConfig.RETRIES_CONFIG, 0);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

        KafkaProducer<String, String> producer = new KafkaProducer<>(props);
        TestCallback callback = new TestCallback();
        ProducerRecord < String, String> record = new ProducerRecord < String, String> (
            topic, data);
        long startTime = System.currentTimeMillis();
        producer.send(record, callback);
        long elapsedTime = System.currentTimeMillis() - startTime;
       // System.out.println("Sent this sentence: " + data+ " in " + elapsedTime + " ms");
        //System.out.println("Done");
        producer.flush();
        producer.close();
        return "Done";
        }


    private static class TestConsumerRebalanceListener implements ConsumerRebalanceListener {
        @Override
        public void onPartitionsRevoked(Collection < TopicPartition > partitions) {
            //System.out.println("Called onPartitionsRevoked with partitions:" + partitions);
        }

        @Override
        public void onPartitionsAssigned(Collection < TopicPartition > partitions) {
           // System.out.println("Called onPartitionsAssigned with partitions:" + partitions);
        }
    }

    private static class TestCallback implements Callback {
        @Override
        public void onCompletion(RecordMetadata recordMetadata, Exception e) {
            if (e != null) {
                System.out.println("Error while producing message to topic :" + recordMetadata);
                e.printStackTrace();
            } else {
                String message = String.format("sent message to topic:%s partition:%s  offset:%s", recordMetadata.topic(), recordMetadata.partition(), recordMetadata.offset());
               // System.out.println(message);
            }
        }
    }

}
