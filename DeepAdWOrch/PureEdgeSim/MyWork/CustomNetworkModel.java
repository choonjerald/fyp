package MyWork;

import com.mechalikh.pureedgesim.Network.NetworkModel;
import com.mechalikh.pureedgesim.Network.FileTransferProgress; 
import com.mechalikh.pureedgesim.ScenarioManager.SimulationParameters;
import com.mechalikh.pureedgesim.ScenarioManager.SimulationParameters.TYPES;
import com.mechalikh.pureedgesim.SimulationManager.SimulationManager;
import com.mechalikh.pureedgesim.TasksGenerator.Task;

public class CustomNetworkModel extends NetworkModel {

	private static final int MAX_NUMBER_OF_REPLICAS = 5;
	private static final int DOWNLOAD_CONTAINER_FROM_NEARBY_DEVICE = 3000;

	public CustomNetworkModel(SimulationManager simulationManager) {
		super(simulationManager);
	}

	@Override
	protected void transferFinished(FileTransferProgress transfer) {
		if (transfer.getTask().getLength() == 500) {
			if (SimulationParameters.DEPLOY_ORCHESTRATOR.equals("CLUSTER")) {
				if (((CustomEdgeDevice) transfer.getTask().getOrchestrator()).getFather() == null) {
					transfer.getTask().setVm(((CustomEdgeDevice) transfer.getTask().getOrchestrator()).getVM());
					scheduleNow(simulationManager, SimulationManager.EXECUTE_TASK, transfer.getTask());
				} else {
					if (transfer.getTransferType() != FileTransferProgress.Type.TASK)
						return;
					transfer.getTask()
							.setOrchestrator(((CustomEdgeDevice) transfer.getTask().getOrchestrator()).getFather());
					scheduleNow(this, NetworkModel.SEND_REQUEST_FROM_DEVICE_TO_ORCH, transfer.getTask());
				}
			} else if (transfer.getTask().getOrchestrator().getType().equals(TYPES.CLOUD)) {
				transfer.getTask().setVm(((CustomEdgeDevice) transfer.getTask().getOrchestrator()).getVM());
				schedule(simulationManager, SimulationParameters.WAN_PROPAGATION_DELAY, SimulationManager.EXECUTE_TASK,
						transfer.getTask());
			} else {
				transfer.getTask().setVm(((CustomEdgeDevice) transfer.getTask().getOrchestrator()).getVM());
				scheduleNow(this.simulationManager, SimulationManager.EXECUTE_TASK, transfer.getTask());
			}
			// Update logger parameters
			simulationManager.getSimulationLogger().updateNetworkUsage(transfer);

			// Delete the transfer from the queue
			transferProgressList.remove(transfer);

		} else if (transfer.getTransferType() == FileTransferProgress.Type.TASK) { // the offloading request has been
																					// received, now pull the container
																					// in order to
			// execute the task
			pullContainer(transfer.getTask());

			transfer.getTask().setReceptionTime(simulationManager.getSimulation().clock());
			updateEnergyConsumption(transfer, "Destination");

			// Update logger parametersa
			simulationManager.getSimulationLogger().updateNetworkUsage(transfer);

			// Delete the transfer from the queue
			transferProgressList.remove(transfer);
		} else if (transfer.getTransferType() == FileTransferProgress.Type.CONTAINER) {
			// the container has been downloaded, keep it in cache
			keepReplica(transfer.getTask());

			// Update logger parameters
			simulationManager.getSimulationLogger().updateNetworkUsage(transfer);

			transfer.getTask().setReceptionTime(simulationManager.getSimulation().clock());

			// execute the task
			containerDownloadFinished(transfer);

			updateEnergyConsumption(transfer, "Container");

			// Delete the transfer from the queue
			transferProgressList.remove(transfer);
		} else // use the default method to handle everything else
			super.transferFinished(transfer);
	}

	private void keepReplica(Task task) {

		// Check if there are enough replicas before keeping a new one
		CustomEdgeDevice edgeDevice = (CustomEdgeDevice) task.getEdgeDevice();
		if (SimulationParameters.registry_mode.equals("CACHE") && ((CustomEdgeDevice) edgeDevice.getOrchestrator())
				.countContainer(task.getApplicationID()) < MAX_NUMBER_OF_REPLICAS) {
			if (edgeDevice.getResources().getAvailableStorage() > task.getContainerSize()) {

				edgeDevice.getResources().setAvailableMemory(edgeDevice.getResources().getAvailableStorage() - task.getContainerSize());
				edgeDevice.cache.add(task);
				double[] array = new double[2];
				array[0] = task.getApplicationID();
				array[1] = task.getEdgeDevice().getId();
				((CustomEdgeDevice) edgeDevice.getOrchestrator()).Remotecache.add(array);
			} else
				// while the memory is not enough
				while (edgeDevice.getResources().getAvailableStorage() < task.getContainerSize()
						&& edgeDevice.getResources().getStorageMemory() > task.getContainerSize()) {

					double min = edgeDevice.getMinContainerCost();
					if (edgeDevice.getCost(task) < min || min == -1) {
						// delete the app with the highest cost
						edgeDevice.deleteMinAapp();
					} else {
						break;
					}
				}
			// if the memory is enough
			if (edgeDevice.getResources().getStorageMemory() > task.getContainerSize()) {
				edgeDevice.getResources().setAvailableMemory(edgeDevice.getResources().getAvailableStorage() - task.getContainerSize());
				edgeDevice.cache.add(task);
				double[] array = new double[2];
				array[0] = task.getApplicationID();
				array[1] = task.getEdgeDevice().getId();
				((CustomEdgeDevice) edgeDevice.getOrchestrator()).Remotecache.add(array);
			}
		}
	}

	private void pullContainer(Task task) {

		if (SimulationParameters.registry_mode.equals("CACHE")) {
			if (!((CustomEdgeDevice) task.getEdgeDevice().getOrchestrator())
					.hasRemoteContainer(task.getApplicationID())) {
				// No replica found
				scheduleNow(this, NetworkModel.DOWNLOAD_CONTAINER, task);
			} else {

				if (((CustomEdgeDevice) task.getVm().getHost().getDatacenter()).hasContainer(task.getApplicationID())
						|| ((CustomEdgeDevice) task.getVm().getHost().getDatacenter()).getType() == TYPES.CLOUD) {
					// This device has a replica in its cache, so execute a task directly
					scheduleNow(simulationManager, SimulationManager.EXECUTE_TASK, task);
				} else {
					double from = ((CustomEdgeDevice) task.getEdgeDevice().getOrchestrator())
							.findReplica(task.getApplicationID());
					task.setRegistry(simulationManager.getServersManager().getDatacenterList().get((int) from - 5));
					// Pull container from another edge device
					scheduleNow(this, NetworkModel.DOWNLOAD_CONTAINER, task);
				}
			}
		}
	}

}
