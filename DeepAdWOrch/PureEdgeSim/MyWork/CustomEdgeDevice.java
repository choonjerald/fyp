package MyWork;

import com.mechalikh.pureedgesim.DataCentersManager.DataCenter;
import com.mechalikh.pureedgesim.ScenarioManager.SimulationParameters;
import com.mechalikh.pureedgesim.SimulationManager.SimLog;
import com.mechalikh.pureedgesim.SimulationManager.SimulationManager;
import com.mechalikh.pureedgesim.TasksGenerator.Task;
import net.sourceforge.jFuzzyLogic.FIS;
import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import org.cloudbus.cloudsim.core.events.SimEvent;
import org.cloudbus.cloudsim.hosts.Host;
import org.cloudbus.cloudsim.vms.Vm;
import src.com.fdtkit.fuzzy.data.Attribute;
import src.com.fdtkit.fuzzy.data.Dataset;
import src.com.fdtkit.fuzzy.data.Row;
import src.com.fdtkit.fuzzy.fuzzydt.FuzzyDecisionTree;
import src.com.fdtkit.fuzzy.fuzzydt.TreeNode;
import src.com.fdtkit.fuzzy.utils.AmbiguityMeasure;
import src.com.fdtkit.fuzzy.utils.LeafDeterminer;
import src.com.fdtkit.fuzzy.utils.LeafDeterminerBase;
import src.com.fdtkit.fuzzy.utils.PreferenceMeasure;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Vector;

public class
CustomEdgeDevice extends DataCenter {
	private static final int UPDATE_CLUSTERS = 11000; // Avoid conflicting with CloudSim Plus Tags
	private static final boolean Transfer = false; // "off" or "on"
	private static final int GET_FITNESS = 2001;
	private static final int UPDATE_STATUS = 2000; // Avoid conflicting with CloudSim Plus Tags

	private double weight = 0;
	private CustomEdgeDevice parent;
	private CustomEdgeDevice Orchestrator;
	private double originalWeight = 0;
	private double weightDrop = 0.7; 
	static double alpha[] = new double[6];
	static double alpha_fitness = -1000;
	static double beta_fitness = -1000;
	static double gama_fitness = -1000;
	static int max = 10;
	static double beta[] = new double[6];
	static double delta[] = new double[6];
	double a = 0;
	static double omega[][] = new double[max][6];
	int iterator = 0;
	int big_iterations = 5;
	private List<Task> tasksList;
	static boolean start = true;
	static long id = -1;
	private int iterations = max;
	private double mips;
	private double avg_distance = 0;
	private int neighbors;
	private double rate = 0;
	boolean old = true;
	boolean mine = true;
	int time = 0;
	private double totDistance = 0;
	private CustomEdgeDevice father;

	private BufferedWriter writer;
	boolean firstTime = true;
	public List<Task> cache = new ArrayList<Task>();
	public List<CustomEdgeDevice> cluster;

	public List<double[]> Remotecache = new ArrayList<double[]>();
	public List<double[]> probability = new ArrayList<double[]>();

	public CustomEdgeDevice(SimulationManager simulationManager, List<? extends Host> hostList,
			List<? extends Vm> vmList) {
		super(simulationManager, hostList, vmList);
		cluster = new ArrayList<CustomEdgeDevice>();
		String fileName = "PureEdgeSim/A/treeS1" + this.getId() + ".txt";
		String fileName2 = "PureEdgeSim/A/treeS2" + this.getId() + ".txt";
		File f = new File(fileName); 
		File f2 = new File(fileName2); 
		f.delete(); // what happen if I delete this
		f2.delete();
		writeTreeFile("S1",true);
		writeTreeFile("S2",false);
	}

	@Override
	protected void startEntity() {

		schedule(this, SimulationParameters.INITIALIZATION_TIME + 1, UPDATE_CLUSTERS); // must be 0.1 because we are 
		// using one decimal digit in
		// time value, see the
// time variable in mobilityManager class
		schedule(this, SimulationParameters.INITIALIZATION_TIME + 5, GET_FITNESS);

		super.startEntity();
	}

	@Override
	public void processEvent(SimEvent ev) {
		switch (ev.getTag()) {
		case GET_FITNESS:
			if ((id == -1 || id == this.getId()) && this.getType() == SimulationParameters.TYPES.EDGE_DEVICE) {
				id = this.getId();
				updateFitness();
			}
			if (iterator == max - 1 && (id == -1 || id == this.getId())
					&& this.getType() == SimulationParameters.TYPES.EDGE_DEVICE) {
				id = this.getId();
				iterator = 0;
				// System.out.println( this.getId() + "," + cluster.size() + "," + (10 -
				// big_iterations) + "," + alpha_fitness);

				if (big_iterations > 0) {
					updatePosition();
					big_iterations--;
				} else {
					for (int i = 0; i < max; i++) {
						for (int j = 0; j < alpha.length; j++)
							omega[i][j] = alpha[j];
					}
				}
			}
			schedule(this, 30, GET_FITNESS);
			break;
		case UPDATE_CLUSTERS:
			
			this.tasksList = simulationManager.getTasksList(); 
			if (this.getType() == SimulationParameters.TYPES.EDGE_DEVICE
					&& SimulationParameters.DEPLOY_ORCHESTRATOR.equals("CLUSTER")
					&& (getSimulation().clock() - time > 30)) {//
				time = (int) getSimulation().clock();
				cluster();
			} 
			if (!isDead()) {
				schedule(this, SimulationParameters.UPDATE_INTERVAL, UPDATE_CLUSTERS);
			}

			break;
		default:
			super.processEvent(ev);
			break;
		}
	}

	private void updatePosition() {
		double a = 2 - ((10 - this.iterations) * (2 / 10));

		// Update the Position of search agents including omegas
		for (int i = 0; i < alpha.length; i++) {
			for (int j = 0; j < alpha.length; j++) {

				double r1 = new Random().nextDouble(); // r1 is a random number in [0,1]
				double r2 = new Random().nextDouble(); // r2 is a random number in [0,1]

				double A1 = 2 * a * r1 - a; // Equation (3.3)
				double C1 = 2 * r2; // Equation (3.4)

				double D_alpha = Math.abs(C1 * alpha[j] - omega[i][j]); // Equation (3.5)-part 1
				double X1 = alpha[j] - A1 * D_alpha; // Equation (3.6)-part 1

				r1 = new Random().nextDouble();
				r2 = new Random().nextDouble();

				double A2 = 2 * a * r1 - a;// Equation (3.3)
				double C2 = 2 * r2; // Equation (3.4)

				double D_beta = Math.abs(C2 * beta[j] - omega[i][j]); // Equation (3.5)-part 2
				double X2 = beta[j] - A2 * D_beta; // Equation (3.6)-part 2

				r1 = new Random().nextDouble();
				r2 = new Random().nextDouble();

				double A3 = 2 * a * r1 - a; // Equation (3.3)
				double C3 = 2 * r2;// Equation (3.4)

				double D_delta = Math.abs(C3 * delta[j] - omega[i][j]);// Equation (3.5)-part 3
				double X3 = delta[j] - A3 * D_delta; // Equation (3.5)-part 3

				omega[i][j] = (X1 + X2 + X3) / 3;// Equation (3.7)

				if (mine) {
					if (omega[i][j] > 1)
						omega[i][j] = 1 - omega[i][j];
					if (omega[i][j] < 0)
						omega[i][j] = -omega[i][j];
				}
			}

			if (mine)
				omega[i][2] = 1 - omega[i][0] - omega[i][1];
			// System.err.println(omega[i][0] + " " + omega[i][1] + " " + omega[i][2] + " "
			// + omega[i][5]);
		}
	}

	private void updateFitness() {
		// TODO Auto-generated method stub
		// if(this.simulation.clock()<60) return;
		double fitness = getFitness();
		if (fitness > alpha_fitness) {
			alpha_fitness = fitness;
			alpha[0] = omega[iterator][0];
			alpha[1] = omega[iterator][1];
			alpha[2] = omega[iterator][2];
			alpha[3] = omega[iterator][3];
			alpha[4] = omega[iterator][4];
			alpha[5] = omega[iterator][5];
		} else if (fitness > beta_fitness) {
			beta_fitness = fitness;
			beta[0] = omega[iterator][0];
			beta[1] = omega[iterator][1];
			beta[2] = omega[iterator][2];
			beta[3] = omega[iterator][3];
			beta[4] = omega[iterator][4];
			beta[5] = omega[iterator][5];
		} else if (fitness > gama_fitness) {
			gama_fitness = fitness;
			delta[0] = omega[iterator][0];
			delta[1] = omega[iterator][1];
			delta[2] = omega[iterator][2];
			delta[3] = omega[iterator][3];
			delta[4] = omega[iterator][4];
			delta[5] = omega[iterator][5];
		}

		// System.err.println(alpha_fitness + " " + alpha[0] + " " + alpha[1] + " " +
		// alpha[2] + " " + alpha[3] + " "
		// + alpha[4] + " " + alpha[5] + " " + iterator + " " + this.big_iterations);
	}

	private double getFitness() {
		// TODO
		return -this.simulationManager.getServersManager().getOrchestratorsList().size()
				* this.simulationManager.getFailureRate();// /
		// (this.getTotalCpuUtilization())
	}

	public double getOriginalWeight() {

		neighbors = 0;
		double distance = 0;
		avg_distance = 0;
		for (int i = 0; i < simulationManager.getServersManager().getDatacenterList().size(); i++) {
			if (simulationManager.getServersManager().getDatacenterList().get(i)
					.getType() == SimulationParameters.TYPES.EDGE_DEVICE) {
				distance = Math.abs(Math.sqrt(Math
						.pow((this.getMobilityManager().getCurrentLocation().getXPos() - simulationManager.getServersManager().getDatacenterList()
								.get(i).getMobilityManager().getCurrentLocation().getXPos()), 2)
						+ Math.pow((this.getMobilityManager().getCurrentLocation().getYPos() - simulationManager.getServersManager()
								.getDatacenterList().get(i).getMobilityManager().getCurrentLocation().getYPos()), 2)));
				if (distance < SimulationParameters.EDGE_DEVICES_RANGE) {
					// neighbor
					neighbors++;
					totDistance += distance;
					avg_distance += distance;
				}
			}
		}
		double battery = 0;
		double mobility = 1;
		if (this.getMobilityManager().isMobile())
			mobility = 0;
		if (!this.getEnergyModel().isBatteryPowered())
			battery = 1;
		else
			battery = this.getEnergyModel().getBatteryLevel() / 100;
		if (this.getVmList().size() > 0)
			mips = this.getVmList().get(0).getMips();
		else
			mips = 0;
		if (neighbors > 0)
			avg_distance = avg_distance / neighbors;
		else
			avg_distance = 0;
		avg_distance = avg_distance / SimulationParameters.EDGE_DEVICES_RANGE;
		// double weight = mobility * battery * 0.2 + mips * 0.4 / (n) + n * 0.2 + n *
		// 0.1 + avg * 0.2;

		// TODO
		randomPopulation();
		normalize();
		// check();
		double weight;
		// (neighbors * delta[iterator][2])
		// + (mobility * neighbors * delta[iterator][3]) +
		if (old)
			weight = (battery * mips / 200000 * 0.5 / neighbors) + (neighbors * 0.2) + (mobility * 0.3) ;
		else
			weight = (mobility * omega[iterator][0]) + (battery * mips / 300000 * omega[iterator][1] / neighbors)
					+ (neighbors * omega[iterator][2]);

		// (neighbors * 0.2) + (neighbors *
		// 0.1)
		if (id == -1 || id == this.getId())
			iterator++;

		/*
		 * if (iterator > 9) { iterator = 0; }
		 */
		if (mips == 0)
			weight = 0;
		return weight;
	}

	private void normalize() {
		if (start) {
			for (int i = 0; i < max; i++) {
				for (int j = 0; j < 6; j++) {
					omega[i][j] /= 100.0;
				}
			}
			start = false;
		}
	}

	private void randomPopulation() {
		if (start) {
			for (int i = 0; i < max; i++) {
				omega[i][0] = 1 + new Random().nextInt(99);

				omega[i][1] = new Random().nextInt(101 - sum(i, 1));

				omega[i][2] = 100 - sum(i, 2);

				// delta[i][3] = new Random().nextInt(100 - sum(i, 3));

				// delta[i][4] = 100 - sum(i, 4);

				omega[i][5] = 1 + new Random().nextInt(100);
			}
		}
	}

	private int sum(int i2, int j2) {
		int sum = 0;
		for (int j = j2; j >= 0; j--) {
			sum += omega[i2][j];
		}
		return sum;
	}

	private double getOrchestratorWeight() {
		if (this.isOrchestrator)
			return this.originalWeight;
		if (this.Orchestrator == null)
			return 0;
		if(!this.Orchestrator.isOrchestrator)
			return this.originalWeight;
		return this.getOrchestrator().getOrchestratorWeight();
	}

	private void deleteFile(String stage) {
		String fileName = "PureEdgeSim/A/tree"+stage + this.getId() + ".txt";
		File f = new File(fileName);
		// TODO
		// if (!Transfer)
		// f.delete();
	}

	public void setOrchestrator(CustomEdgeDevice DataCenter) {
		if (!this.isOrchestrator && DataCenter == this) {
			
		 	if (Transfer) {
			 	if (getSimulation().clock() > 20)
			 		transferLearning2("S1");
		 			transferLearning2("S2");
			}
		}

		if (this == DataCenter) {
			if (old)
				this.rate = 0.5;
			else
				this.rate = this.omega[iterator][5];

			if (this.Orchestrator != null)
				this.Orchestrator.cluster.remove(this);
			this.Orchestrator = this;
			this.isOrchestrator = true;
			this.setFather(null);
			if (!this.cluster.contains(this))
				this.cluster.add(this);
			if (!simulationManager.getServersManager().getOrchestratorsList().contains(this))
				simulationManager.getServersManager().getOrchestratorsList().add(this);

		}

		else {
			if (this.isOrchestrator) {
				for (int i = 0; i < cluster.size(); i++) {
					if (!DataCenter.cluster.contains(this.cluster.get(i)))
						DataCenter.cluster.add(this.cluster.get(i));
				}
			}
			this.cluster.clear();
			if (Orchestrator != null)
				this.Orchestrator.cluster.remove(this);
			simulationManager.getServersManager().getOrchestratorsList().remove(this);

			this.setFather(DataCenter);
			this.Orchestrator = DataCenter;
			this.isOrchestrator = false;
			if (!DataCenter.cluster.contains(this))
				DataCenter.cluster.add(this);

			if (!DataCenter.cluster.contains(DataCenter))
				DataCenter.cluster.add(DataCenter);

			DataCenter.Orchestrator = DataCenter;
			DataCenter.isOrchestrator = true;
			DataCenter.setFather(null);
			if (!simulationManager.getServersManager().getOrchestratorsList().contains(DataCenter))
				simulationManager.getServersManager().getOrchestratorsList().add(DataCenter);
		}

		if (DataCenter != this) {
			deleteFile("S1");
			deleteFile("S2");
		}
	}

	private void transferLearning2(String stage) {
		String trust;
		List<List<String>> Forest = RequestDecisionTrees(stage);
		List<String> tree = Forest.get(0); // initialize the decision tree
		for (int i = 1; i < Forest.size(); i++) // browse all the trees

			if (Forest.get(i) == null)
				return;
			else
				for (int j = 0; j < Forest.get(i).size(); j++) // browse all rules
					if (HasRule(tree, Forest.get(i).get(j)) == false)
						tree.add(Forest.get(i).get(j));
		for (int i = 0; i < tree.size(); i++) {
			int high = 0;
			int medium = 0;
			int low = 0;
			for (int k = 0; k < Forest.size(); k++)
				for (int l = 0; l < Forest.get(k).size(); l++) {
					if (sameRule(tree.get(i),Forest.get(k).get(l))) 
						if (getTrust(Forest.get(k).get(l)).equals("high"))
							high++;
						else if (getTrust(Forest.get(k).get(l)).equals("medium"))
							medium++;
						else
							low++;
				} 
			if (high > medium && high > low)
				trust = "high";
			else if (medium > low)
				trust = "medium";
			else
				trust = "low";
			updateTrust(tree, i, trust);
		}
		try {
			save(tree, stage);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // save the obtained tree
	}

	private boolean sameRule(String rule1, String rule2) { 
		String[] a = rule1.split(",");
		String[] b = rule2.split(","); 
		if (a.length != b.length)
			return false;
		for (int i = 0; i < a.length - 1; i++) {
			if (!a[i].equals(b[i])) {
				 return false;
			}
		}
		return true;
	}

	private void save(List<String> tree, String stage) throws IOException {
		// TODO Auto-generated method stub
		StringBuffer inputBuffer = new StringBuffer();
		for (int i = 0; i < tree.size(); i++) {
			inputBuffer.append(tree.get(i));
			inputBuffer.append('\n');

		}
		String inputStr = inputBuffer.toString();
		// write the new String with the replaced line OVER the same file
		FileOutputStream fileOut = new FileOutputStream("PureEdgeSim/A/tree"+stage + this.getId() + ".txt");
		fileOut.write(inputStr.getBytes());
		fileOut.close();

	}

	private void updateTrust(List<String> tree, int i, String trust) {
		String[] b = tree.get(i).split(",");
		b[b.length - 1] = trust;
		tree.remove(i);
		String s = "";
		for (int j = 0; j < b.length - 1; j++)
			s += b[j] + ",";
		s += b[b.length - 1];
		tree.add(s);
	}

	private String getTrust(String rule) {
		String[] b = rule.split(",");
		return b[b.length - 1];
	}

	private boolean HasRule(List<String> tree, String rule) {
		for (int j = 0; j < tree.size(); j++) {
			if (tree.get(j).equals(rule))
				return true;

			String[] a = tree.get(j).split(",");
			String[] b = rule.split(",");
			boolean same = true;
			if (a.length != b.length)
				same = false;
			for (int i = 0; i < a.length - 1; i++) {
				if (!a[i].equals(b[i])) {
					same = false;
				}
			}
			if (same) {
				return true;

			}
		}
		return false;
	}

	private List<List<String>> RequestDecisionTrees(String stage) {
		List<List<String>> forest = new ArrayList<List<String>>();
		for (int i = 2; i < simulationManager.getServersManager().getDatacenterList().size(); i++) {
		if (simulationManager.getServersManager().getDatacenterList().get(i)
				.getType() == SimulationParameters.TYPES.EDGE_DEVICE) {
				double distance = Math.abs(Math.sqrt(Math
						.pow((this.getMobilityManager().getCurrentLocation().getXPos() - simulationManager.getServersManager().getDatacenterList()
								.get(i).getMobilityManager().getCurrentLocation().getXPos()), 2)
						+ Math.pow((this.getMobilityManager().getCurrentLocation().getYPos() - simulationManager.getServersManager()
								.getDatacenterList().get(i).getMobilityManager().getCurrentLocation().getYPos()), 2)));
				if (distance <= SimulationParameters.EDGE_DEVICES_RANGE) {
					forest.add(toArrayList(simulationManager.getServersManager().getDatacenterList().get(i), stage));
				}
			}
		}

		return forest;
	}

	private List<String> toArrayList(DataCenter DataCenter, String stage) {
		String filetoadd = "PureEdgeSim/A/tree"+stage + DataCenter.getId() + ".txt";
		List<String> tree = new ArrayList<String>();
		File f2 = new File(filetoadd);
		if (!f2.exists())
			return null;

		try {
			BufferedReader file = new BufferedReader(new FileReader(filetoadd));
			String line;
			while ((line = file.readLine()) != null) {
				tree.add(line);
			}

			file.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return tree;
	}

	private void transferLearning(String stage) {
		for (int i = 2; i < simulationManager.getServersManager().getDatacenterList().size(); i++) {
			if (simulationManager.getServersManager().getDatacenterList().get(i)
					.getType() == SimulationParameters.TYPES.EDGE_DEVICE) {
				double distance = Math.abs(Math.sqrt(Math
						.pow((this.getMobilityManager().getCurrentLocation().getXPos() - simulationManager.getServersManager().getDatacenterList()
								.get(i).getMobilityManager().getCurrentLocation().getXPos()), 2)
						+ Math.pow((this.getMobilityManager().getCurrentLocation().getYPos() - simulationManager.getServersManager()
								.getDatacenterList().get(i).getMobilityManager().getCurrentLocation().getYPos()), 2)));
				if (distance <= SimulationParameters.EDGE_DEVICES_RANGE) {
					mergeFiles((CustomEdgeDevice) simulationManager.getServersManager().getDatacenterList().get(i),
							this, stage);
				}
			}
		}
	}

	private void mergeFiles(CustomEdgeDevice orch, CustomEdgeDevice device, String stage) {
		String filetoadd = "PureEdgeSim/A/tree"+stage + device.getId() + ".txt";

		File f2 = new File(filetoadd);
		if (!f2.exists())
			return;

		try {
			BufferedReader file = new BufferedReader(new FileReader(filetoadd));
			String line;
			while ((line = file.readLine()) != null) {

				replaceSelected(line, Shrink(line), stage, orch.getId());

			}

			file.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String Shrink(String line) {
		String[] a = line.split(",");
		String S = "";
		for (int i = 0; i < a.length - 1; i++) {
			S = S + a[i] + ",";
		}
		return S;
	}

	public void replaceSelected(String replaceWith, String original, String stage, long l) {
		try {
			// input the file content to the StringBuffer "input"
			BufferedReader file = new BufferedReader(new FileReader("PureEdgeSim/A/tree" +stage + l + ".txt"));
			String line;
			StringBuffer inputBuffer = new StringBuffer();

			while ((line = file.readLine()) != null) {
				if (!line.contains(original)) {
					inputBuffer.append(line);
					inputBuffer.append('\n');

				} else {
					String str = replaceWith;
					replaceWith = compare(line, replaceWith);
					// if(!str.equals(replaceWith))
					// System.err.println(str +" "+line+" "+ replaceWith);
				}
			}
			file.close();
			String inputStr = inputBuffer.toString();

			inputStr += replaceWith;
			// write the new String with the replaced line OVER the same file
			FileOutputStream fileOut = new FileOutputStream("PureEdgeSim/A/tree" +stage + l + ".txt");
			fileOut.write(inputStr.getBytes());
			fileOut.close();

		} catch (Exception e) {
			// System.out.println("Problem reading file.");
		}
	}

	private String compare(String line, String line2) {
		if (line.equals(line2))
			return line;
		String[] a = line.split(",");
		String[] b = line2.split(",");
		boolean same = true;
		if (a.length != b.length)
			same = false;
		for (int i = 0; i < a.length - 1; i++) {
			if (!a[i].equals(b[i])) {
				same = false;
			}
		}
		if (same) {
			  a[a.length - 1] = Min(a[a.length - 1], b[b.length - 1]);

		}
		String S = "";
		for (int i = 0; i < a.length - 1; i++) {
			S = S + a[i] + ",";
		}
		return S + a[a.length - 1];
	}

	private String Min(String string, String string2) {
		if (string.equals("low") || string2.equals("low"))
			return "low";
		if (string.equals("medium") || string2.equals("medium"))
			return "medium";
		return "high";
	}

	private void cluster() {

		int taskid = 999900000 + (int) (this.getId() + getSimulation().clock() * 100);
		originalWeight = getOriginalWeight();

		if (this.getOrchestratorWeight() < originalWeight) {
			// System.err.println(this.getOrchestratorWeight()+" < "+originalWeight);
			setOrchestrator(this);
			this.weight = originalWeight;
		}

		double distance = 0;
		if (this.getFather() != null) {
			distance = Math.abs(
					Math.sqrt(Math.pow((this.getMobilityManager().getCurrentLocation().getXPos() - this.getFather().getMobilityManager().getCurrentLocation().getXPos()), 2)
							+ Math.pow((this.getMobilityManager().getCurrentLocation().getYPos() - this.getFather().getMobilityManager().getCurrentLocation().getYPos()), 2)));
			if (distance > SimulationParameters.EDGE_DEVICES_RANGE) {
				setOrchestrator(this);
				this.weight = originalWeight;
			}

		}

		for (int i = 2; i < simulationManager.getServersManager().getDatacenterList().size(); i++) {
			if (simulationManager.getServersManager().getDatacenterList().get(i)
					.getType() == SimulationParameters.TYPES.EDGE_DEVICE) {
				distance = Math.abs(Math.sqrt(Math
						.pow((this.getMobilityManager().getCurrentLocation().getXPos() - simulationManager.getServersManager().getDatacenterList()
								.get(i).getMobilityManager().getCurrentLocation().getXPos()), 2)
						+ Math.pow((this.getMobilityManager().getCurrentLocation().getYPos() - simulationManager.getServersManager()
								.getDatacenterList().get(i).getMobilityManager().getCurrentLocation().getYPos()), 2)));
				if (distance <= SimulationParameters.EDGE_DEVICES_RANGE) {
					// neighbors
					if (this.weight < ((CustomEdgeDevice) simulationManager.getServersManager().getDatacenterList()
							.get(i)).weight) {// &&
						// simulationManager.getServersManager().getDatacenterList().get(i).getOrchestrator().cluster.size()<15
						setOrchestrator((CustomEdgeDevice) simulationManager.getServersManager().getDatacenterList()
								.get(i).getOrchestrator());
						this.setFather(
								(CustomEdgeDevice) simulationManager.getServersManager().getDatacenterList().get(i));
						this.weight = ((CustomEdgeDevice) simulationManager.getServersManager().getDatacenterList()
								.get(i)).weight
								* ((CustomEdgeDevice) simulationManager.getServersManager().getDatacenterList().get(i)
										.getOrchestrator()).rate;
						//if (Transfer )
						//	mergeFiles(this.Orchestrator, this);
						/*
						 * taskid++; this.simulationManager.getSimulationLogger().incrementTasksSent();
						 * Task task = new Task(taskid, 40, 1);// 1 cpu usage, we will just evaluate the
						 * // network usage, besides the 1 cpu usage will help destinguish the real
						 * tasks // from these fake tasks
						 * 
						 * task.setFileSize(2);// 20kb will be transferred to the orchestrator
						 * task.setTime(simulation.clock()); UtilizationModel utilizationModel = new
						 * UtilizationModelFull();
						 * task.setOutputSize(2).setUtilizationModel(utilizationModel);
						 * task.setContainerSize(2); task.setMaxLatency(60); task.setEdgeDevice(this);//
						 * set the device that generated this task
						 * task.setOrchestrator(simulationManager.getServersManager().getDatacenterList(
						 * ).get(i)); tasksList.add(task);
						 * simulationManager.getSimulationLogger().setGeneratedTasks(tasksList.size());
						 * scheduleNow(simulationManager.getNetworkModel(),
						 * NetworkModel.SEND_REQUEST_FROM_DEVICE_TO_ORCH, task);
						 */
					}
				}
			}
		}

	}

	public CustomEdgeDevice getOrchestrator() {
		if (Orchestrator == null)
			Orchestrator = this;
		return this.Orchestrator;
	}

	public CustomEdgeDevice getFather() {
		return father;
	}

	public void setFather(CustomEdgeDevice father) {
		this.father = father;
	}

	public Vm getVM() {
		return this.getVmList().get(0);
	}

	public boolean hasContainer2(double appId) {
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getApplicationID() == appId)
				return true;
		}
		return false;
	}

	/*
	 * public FuzzyDecisionTree getDecisiontree() { return FDT; }
	 */
	public void writeTreeFile(String stage, boolean b) {
		firstTime = true;
		String fileName = "PureEdgeSim/A/tree"+stage + this.getId() + ".txt";
		System.out.println("new file: "+fileName); // uncomment for test
		File f = new File(fileName);
		String str = "";
		if (b) 
			str="#Latency:High Medium Low-"+
		"#WanBandwidth:High Medium Low-"+
		"#TaskLength:High Medium Low-"+
		"#Mob:High Medium Low-"+
		"#DestUsage:High Medium Low-"+
		"@Offload:High Medium Low-"+
		"=Data=-"+
		"high,medium,low,low,low,medium-";
		else
			str="#DestCPU:High Medium Low-"+
					"#TaskLength:High Medium Low-"+
					"#DestRem:High Medium Low-"+
					"#DestMob:High Medium Low-"+
					"@Offload:High Medium Low-"+
					"=Data=-"+
					"low,low,low,low,low-";
	
String[] tab= str.split("-");
		try {
			writer = new BufferedWriter(new FileWriter(fileName, true));
			BufferedReader file = new BufferedReader(new FileReader("PureEdgeSim/A/tree"+stage + this.getId() + ".txt"));
			String line;
			for(int i=0;i<tab.length;i++) {
				firstTime = true;
			while ((line = file.readLine()) != null) {
				if (line.contains(tab[i]))
					firstTime = false;

			}
			if (firstTime) {
				writer.append(tab[i]);
				writer.newLine();
			}
			}

			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean hasContainer(double appId) {
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getApplicationID() == appId)
				return true;
		}
		return false;
	}

	public int countContainer(double appId) {
		int j = 0;
		for (int i = 0; i < Remotecache.size(); i++) {
			if (Remotecache.get(i)[0] == appId)
				j++;
		}
		return j;
	}

	public boolean hasRemoteContainer(double appId) {
		for (int i = 0; i < Remotecache.size(); i++) {
			if (Remotecache.get(i)[0] == appId)
				return true;
		}
		return false;
	}

	public double findReplica(double appId) {
		for (int i = 0; i < Remotecache.size(); i++) {
			if (Remotecache.get(i)[0] == appId)
				return Remotecache.get(i)[1];
		}
		return -1;
	}

	public double getMinContainerCost() {
		double mincost = -1;
		for (int i = 0; i < cache.size(); i++) {
			if (getCost(cache.get(i)) < mincost || mincost == -1)
				mincost = getCost(cache.get(i));
		}
		return mincost;
	}

	public double getCost(Task task) {
		double maxSize = 1;
		double T = 3;
		double MaxP = 1;
		for (int i = 0; i < cache.size(); i++) {
			if (cache.get(i).getContainerSize() > maxSize)
				maxSize = cache.get(i).getContainerSize();
			if (getProbability(cache.get(i).getApplicationID()) > MaxP)
				MaxP = getProbability(cache.get(i).getApplicationID());
		}
		// System.err.println("p="+getProbability(task.getApplicationID())+ "
		// maxp="+MaxP+" T="+countT(task)+ " maxT=3"+" size="+task.getContainerSize()+"
		// maxSize="+maxSize);
		// System.err.println(1- (getProbability(task.getApplicationID())/MaxP)*
		// (countT(task)*task.getContainerSize()/ (T*maxSize)));
		return 1 - (getProbability(task.getApplicationID()) / MaxP) * countT(task)
				* (task.getContainerSize() / (T * maxSize));
	}

	private double countT(Task task) {
		int count = 0;
		CustomEdgeDevice orch;
		if (isOrchestrator)
			orch = this;
		else
			orch = this.getOrchestrator();
		for (int i = 0; i < orch.Remotecache.size(); i++)
			if (orch.Remotecache.get(i)[0] == task.getApplicationID())
				count++;
		return count;
	}

	private double getProbability(double appId) {
		CustomEdgeDevice orch;
		if (isOrchestrator)
			orch = this;
		else
			orch = this.getOrchestrator();
		for (int i = 0; i < orch.probability.size(); i++)
			if (orch.probability.get(i)[0] == appId)
				return orch.probability.get(i)[1];
		return 0;
	}

	public void addRequest(Task task) { // update application requesting probability
		boolean found = false;
		CustomEdgeDevice orch;
		if (isOrchestrator)
			orch = this;
		else
			orch = this.getOrchestrator();
		for (int i = 0; i < orch.probability.size(); i++) {
			if (orch.probability.get(i)[0] == task.getApplicationID()) {
				found = true;
				orch.probability.get(i)[1]++;
			}
		}
		if (!found) {
			double[] array = new double[2];
			array[0] = task.getApplicationID();
			array[1] = 1;
			orch.probability.add(array);

		}
	}

	public void deleteMinAapp() {
		double mincost = -1;
		int app = -1;
		for (int i = 0; i < cache.size(); i++) {
			if (getCost(cache.get(i)) < mincost || mincost == -1) {
				mincost = getCost(cache.get(i));
				app = i;
			}
		}
		if (app != -1) {
			this.getResources().setAvailableMemory(this.getResources().getAvailableStorage() + cache.get(app).getContainerSize());
			CustomEdgeDevice orch;
			if (isOrchestrator)
				orch = this;
			else
				orch = this.getOrchestrator();
			removeFromRemote(orch, cache.get(app));
			cache.remove(app);
		}
	}

	private void removeFromRemote(CustomEdgeDevice orch, Task task) {
		for (int i = 0; i < orch.Remotecache.size(); i++) {
			if (orch.Remotecache.get(i)[0] == task.getApplicationID()) {
				orch.Remotecache.get(i)[1]--;
			}
		}
	}

    public static class CustomOrchestratorCpuDelayCR extends com.mechalikh.pureedgesim.TasksOrchestration.Orchestrator {
        private TreeNode root;
        FuzzyDecisionTree fuzzydecisionTree;
        FIS fis;
        Vector<Q_table_row> Q_Table = new Vector<Q_table_row>();
        public int exploration = 0;
        public  int exploitation = 0;


        public CustomOrchestratorCpuDelayCR(SimulationManager simulationManager) throws IOException {
            super(simulationManager);
            // initialize datasets files
            UpdateFuzzyDecisionTree(Q_Table);
        }

        @Override
        protected int findVM(String[] architecture, Task task) {
            if (simulationManager.getScenario().getStringOrchAlgorithm().equals("FDT"))
                try {
                    System.out.println("lets find a Vm for task : " + task.getUid());

                    fuzzydecisionTree = getDecisionTree();
                    return DecisionTree(architecture, task);
                } catch (Exception e) {
                    e.printStackTrace();
                }else {
                SimLog.println("");
                SimLog.println("Custom Orchestrator- Unknnown orchestration algorithm '" + algorithm
                        + "', please check the simulation parameters file...");
                // Cancel the simulation
                Runtime.getRuntime().exit(0);
            }
            return -1;
        }

        private FuzzyDecisionTree getDecisionTree() {
            Dataset d = new Dataset("Sample1");

            BufferedReader br = null;
            try {
                br = new BufferedReader(new FileReader("PureEdgeSim/MyWork/tree" + ".txt"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            String line = "";
            String className = "";
            try {
                while ((line = br.readLine()) != null) {

                    if (line.startsWith("#")) {
                        String[] terms = line.substring(line.indexOf(":") + 1).split(" ");
                        d.addAttribute(new Attribute(line.substring(1, line.indexOf(":")), terms));
                    } else if (line.startsWith("@")) {
                        String[] terms = line.substring(line.indexOf(":") + 1).split(" ");
                        d.addAttribute(new Attribute(line.substring(1, line.indexOf(":")), terms));
                        className = line.substring(1, line.indexOf(":"));
                        d.setClassName(className);
                    } else if (!line.startsWith("=Data=")) {
                        String[] data = line.split(" ");
                        Object[] crispRow = new Object[d.getAttributesCount()];
                        double[][] fuzzyValues = new double[d.getAttributesCount()][];
                        int k = 0;
                        //System.out.println("crispRow.length: " + crispRow.length);
                        for (int i = 0; i < crispRow.length; i++) {
                            crispRow[i] = "Dummy";
                            // fuzzyValues[i] = getFuzzyValues(data[i]);
                            fuzzyValues[i] = new double[d.getAttribute(i).getLinguisticTermsCount()];
                           // System.out.println("fuzzyValues[i].length " + fuzzyValues[i].length );
                            for (int j = 0; j < fuzzyValues[i].length; j++) {
                                fuzzyValues[i][j] = Double.parseDouble(data[k++]);
                           //     System.out.println("i:  " + i + " j: " + j + " k: " + k + " fuzzyValues[i][j]: " + fuzzyValues[i][j]);
                            }
                        }

                        d.addRow(new Row(crispRow, fuzzyValues));

                    }
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            PreferenceMeasure preferenceMeasure;
            LeafDeterminer leafDeterminer;
            FuzzyDecisionTree descisionTree;
            preferenceMeasure = new AmbiguityMeasure(0.5);
            leafDeterminer = new LeafDeterminerBase(0.9);

            descisionTree = new FuzzyDecisionTree(preferenceMeasure, leafDeterminer);

            root = descisionTree.buildTree(d);


            // Uncomment to print fuzzy rules

            String[] rulesArray = descisionTree.generateRules(root);
            String rules = "======================================== \r\n ===================================== \r\n";
            for (String rule : rulesArray)
                rules += rule + "\r\n";
            File file = new File("PureEdgeSim/MyWork/readable_rules_" );
            try { DataOutputStream outstream = new DataOutputStream(new FileOutputStream(file, true));
                outstream.write(rules.getBytes()); outstream.close(); }
            catch (IOException e)
            { e.printStackTrace(); }



            return descisionTree;
        }

        private int DecisionTree(String[] architecture, Task task) throws Exception {
            System.out.println("task : " + task.getUid() + "in DecisionTree ");
            double max = -1;
            int vm = -1;
            int edgeDateCenter = 0;
            double minDistance = maxDistance() * 3;
            double minResource = Double.POSITIVE_INFINITY;
            String rule = "";
            String ruleValues = "";
            double[] lat = getLat(task.getMaxLatency());
            double[] taskLength = getTaskLength(task.getLength());
            double[] wanUsage = getWanUsage(simulationManager.getNetworkModel().getWanUtilization());
            double[] userMobi = getMobi(task.getEdgeDevice().getMobilityManager().isMobile());

            System.out.println("orchestrationHistory: " + orchestrationHistory.size());
    //        int decision;
    //        double random = new Random().nextFloat();
    //        if (random <= 6 / simulationManager.getSimulation().clock()) { // explore
    //            vm = new Random().nextInt(orchestrationHistory.size());
    //            exploration++;
    //            System.out.println(" Random decision  " + vm);
    //        } else {

                for (int i = 0; i < orchestrationHistory.size(); i++) { //orchestrationHistory.size() is equal to
                                                                        // number of all VMs in the system
                    if (offloadingIsPossible(task, vmList.get(i), architecture)
                            && vmList.get(i).getStorage().getCapacity() > 0) {
                        if (((DataCenter) vmList.get(i).getHost().getDatacenter())
                        .getType() == SimulationParameters.TYPES.EDGE_DATACENTER){
                            System.out.println(" VM " + i + " is Edge Data center ");
                            edgeDateCenter++;
                        }

                        double[] destcpuUsage = getDesCPUusage(
                                ((DataCenter) vmList.get(i).getHost().getDatacenter()).getResources()
                                        .getCurrentCpuUtilization()); // I changed * 100 to / 10
                                        System.out.println("destcpuUsage: " + destcpuUsage[0] + destcpuUsage[1] + destcpuUsage[2]);
                        double[] destCpuMips =getDesCPU(((DataCenter) vmList.get(i).getHost().getDatacenter())
                                .getResources().getTotalMips());

                        double[] destRemai = getDestRemain((DataCenter) vmList.get(i).getHost().getDatacenter());
                        double[] desMobi = this.getDestMobi((DataCenter) vmList.get(i).getHost().getDatacenter(),
                                task.getEdgeDevice().getMobilityManager().isMobile());
                        Dataset d = getDataset(lat, taskLength, wanUsage, userMobi, destcpuUsage, destCpuMips, destRemai, desMobi);

                        double[] cVals = fuzzydecisionTree.classify(0, d, "Offload", fuzzydecisionTree.generateRules(root));

                        // to save the rule used to offload this task, this rule will be used later when
                        // updating the Q values
                        // the rule will be stored in the task metadata object for easy access
                        rule = getLatTerm(task.getMaxLatency()) + ","
                                + getWanUsageTerm(simulationManager.getNetworkModel().getWanUtilization()) + ","
                                + getTaskLengthTerm(task.getLength()) + ","
                                + (task.getEdgeDevice().getMobilityManager().isMobile() ? "high" : "low") + ","
                                + getDesCPUusageTerm(((DataCenter) vmList.get(i).getHost().getDatacenter())
                                .getResources().getCurrentCpuUtilization())+","
                                +getDesCPUTerm(((DataCenter) vmList.get(i).getHost().getDatacenter())
                                .getResources().getTotalMips()) + ","
                                +getDestRemainTerm((DataCenter) vmList.get(i).getHost().getDatacenter()) + ","
                                + getDestMobiTerm((DataCenter) vmList.get(i).getHost().getDatacenter(),
                                task.getEdgeDevice().getMobilityManager().isMobile());

                        ruleValues = task.getMaxLatency() + ","
                                + simulationManager.getNetworkModel().getWanUtilization()+ ","
                                + Double.toString(task.getLength())+ ","
                                + (task.getEdgeDevice().getMobilityManager().isMobile() ? "high" : "low") + ","
                                + ((DataCenter) vmList.get(i).getHost().getDatacenter())
                                .getResources().getCurrentCpuUtilization() +","
                                + ((DataCenter) vmList.get(i).getHost().getDatacenter())
                                .getResources().getAvgCpuUtilization() +","
                                +((DataCenter) vmList.get(i).getHost().getDatacenter())
                                .getResources().getTotalMips() + ","
                                +getDestRemainValue (((DataCenter) vmList.get(i).getHost().getDatacenter()))+ ","
                                + (((DataCenter) vmList.get(i).getHost().getDatacenter())
                                .getMobilityManager().isMobile() ? "high" : "low");
                        System.out.println(" Rule  " + rule);
                        System.out.println(" Rule  " + ruleValues);

                        System.out.println(" cVals  " + cVals[0] + cVals[1] + cVals[2]);
                        double distance = ((DataCenter) vmList.get(i).getHost().getDatacenter()).getMobilityManager()
                                .distanceTo(task.getEdgeDevice());
                        double checkResource = checkEnoughResource(task , i);
                        System.out.println(" checkResource: " + checkResource + " minResourcs:  " + minResource);
                        System.out.println(" Distance to orchestrator  " + distance);
                        // if the class = high, high = the expected success rate is high or the
                        // suitability of the device, so we should offload to this device
                        // cVals is an array in which the fuzzified values are stored.
                        // cVals[0]= the membership of degree of fuzzy set "high"
                        // cVals{1] is for the fuzzy set "medium"
                        // and cVals[2] is for the low one.
                        //if (cVals[0] >= cVals[1] && (max == -1 || max <= cVals[0] + 2) && minDistance > distance) {
                        if (cVals[0] >= cVals[1] && (checkResource >= 0) &&  ((minResource == Double.POSITIVE_INFINITY ||
                                checkResource <= minResource) || minDistance > distance)){

                        // it keeps the highest VM
                            // if the membership degree of "high" is bigger than "medium"(
                            // cVals[0]>=cVals[1]) and if it is the first time (max==-1), or it is not the
                            // first time but the old max is below the membership degree of "high".
                            // then select the this device/vm as to execute the task
                            vm = i;
                            minResource = checkResource;
                            System.out.println("task: " + task.getUid() + " vm1:" + vm);

                            // update the max value with the new membership degree of "high" fuzzy set
                            // if the wake it here, this means that we have found at least one device that
                            // is classifed as "high".This means that any other device that is classified as
                            // "medium" or "low" should be ignored. Since we are using a signle variable
                            // "max" to store the highest membership degree for any of those fuzzy sets
                            // (high, medium, low). We need to keep the superiority of "high" over "medium"
                            // and "medium" over "low".
                            // e.g., if the classification of a device gives 0.6 for the "high" fuzzy set.
                            // and the next one gives 1 but for "low" fuzzy set. In this case 0.6>1, to do
                            // this we simply add 2 to 0.6, so we will have 2.6 >
                            // 1.
                            //max = cVals[0] + 2;

                        }// if the class = medium
                        else if (cVals[1] >= cVals[2]) {
                            if ((max == -1 || max <= cVals[1] + 1) && (checkResource >= 0) && minDistance > distance + maxDistance() ) {
                                max = cVals[1] + 1; // I changed cVals[0] + 1 to cVals[1] + 1
                                vm = i;
                                System.out.println("task: " + task.getUid() + " vm2:" + vm);

                            }
                        } else {
                            // the class= low
                            if ((max == -1 || max < cVals[2]) && (checkResource >= 0) && (minDistance > distance + (2 * maxDistance()))) {
                                max = cVals[2];
                                vm = i;
                                System.out.println("task: " + task.getUid() + " vm3:" + vm);
                            }
                        }

                    }

                }
     //       }
            System.out.println("task: " + task.getUid() + " vm4:" + vm);
            if (vm != -1) {
                System.out.println("task: " + task.getUid() + " vm5:" + vm);
                // save the rule in the task metadata for later use
                task.setMetaData(new String[] { rule, "" });
                // return the offloading destination
                System.out.println("task: " + task.getUid() + " Dest Vm:" + vm);
                exploitation++;

                return vm;
            } else
                return -1;
        }
        private double checkEnoughResource(Task task , int i ){
            double freeResource = (((DataCenter) vmList.get(i).getHost().getDatacenter())
                    .getResources().getTotalMips()) * (1 - (((DataCenter) vmList.get(i).getHost().getDatacenter())
                    .getResources().getCurrentCpuUtilization() / 100) );
            double minCapacity = (freeResource - (task.getLength() * 1.3
            ));
            return  minCapacity;

        }

        private Dataset getDataset(double[] lat, double[] wanusage, double[] tasklength, double[] mobi, double[] destcpuusage,
                                   double[] destcpumips, double[] destremai, double[] desmobi) {
            Dataset d = new Dataset("Sample1");

            // Add the attributes with Linguistic terms
            d.addAttribute(new Attribute("Latency", new String[] { "High", "Medium", "Low" }));
            d.addAttribute(new Attribute("WanUsage", new String[] { "High", "Medium", "Low" }));
            d.addAttribute(new Attribute("TaskLength", new String[] { "High", "Medium", "Low" }));
            d.addAttribute(new Attribute("Mob", new String[] { "High", "Medium", "Low" }));
            d.addAttribute(new Attribute("DestCPUUsage", new String[] { "High", "Medium", "Low" }));
            d.addAttribute(new Attribute("DestCPUMips", new String[] { "High", "Medium", "Low" }));
            d.addAttribute(new Attribute("DestRem", new String[] { "High", "Medium", "Low" }));
            d.addAttribute(new Attribute("DestMob", new String[] { "High", "Medium", "Low" }));
            d.addAttribute(new Attribute("Offload", new String[] { "High", "Medium", "Low" }));
            double[][] columns = { lat, wanusage,tasklength,mobi, destcpuusage, destcpumips, destremai, desmobi };

            d.addRow(new Row(new Object[] { "Dummy", "Dummy", "Dummy", "Dummy", "Dummy", "Dummy", "Dummy", "Dummy", "Dummy" }, columns));

            return d;
        }

    //    private double[] getDestination(String destiantion) {
    //        if (destiantion.equals("cloud"))
    //            return new double[] { 1.0, 0.0, 0.0 };
    //        else if (destiantion.equals("edge"))
    //            return new double[] { 0.0, 1.0, 0.0 };
    //        else
    //            return new double[] { 0.0, 0.0, 1.0 };
    //    }
        private double maxDistance() {
            return Math.abs(Math.sqrt(Math.pow((SimulationParameters.AREA_WIDTH / 2) , 2)
                    + Math.pow(((SimulationParameters.AREA_LENGTH / 2) ), 2)));
    }

        private double[] getDesCPU(double d) {
            double l = 30000, m = 65000, h = 1300000;
            if (d >= h)
                return new double[] { 1.0, 0.0, 0.0 };
            if (d < l)
                return new double[] { 0.0, 0.0, 1.0 };
            if (d >= l && d < m)
                return new double[] { 0.0, (d - l) / (m - l), (m - d) / (m - l) };
            if (d > m && d < h)
                return new double[] { (d - m) / (h - m), (h - d) / (h - m), 0.0 };
            if (d == m)
                return new double[] { 0.0, 1.0, 0.0 };
            return null;
        }

        private String getDesCPUTerm(double mips) {
            if (mips >= 130000)
                return "high";
            else if (mips < 30000)
                return "low";
            else
                return "medium";
        }

        private String getDesCPUUsageTerm(double u) {
            if (u >= 4)
                return "high";
            else if (u <= 1)
                return "low";
            else
                return "medium";
        }

        private String getDestRemainTerm(DataCenter datacenter) {
            double e;
            if (datacenter.getEnergyModel().isBatteryPowered())
                e = datacenter.getEnergyModel().getBatteryLevelPercentage();
            else
                e = 100;
            if (e >= 75)
                return "high";
            else if (e <= 25)
                return "low";
            else
                return "medium";
        }

        private String getDestRemainValue(DataCenter datacenter) {
            double e;
            if (datacenter.getEnergyModel().isBatteryPowered())
                e = datacenter.getEnergyModel().getBatteryLevelPercentage();
            else
                e = 100;
            return Double.toString(e);
        }

        private double[] getDesCPUusage(double d) {
            double l = 50;
            double h = 100;
            if (d <= 0)
                return new double[] { 0.0, 0.0, 1.0 };
            if (d >= h)
                return new double[] { 1.0, 0.0, 0.0 };
            if (d > 0 && d < l)
                return new double[] { 0.0, (d - 0) / (h - l), (l - d) / (h - l) };
            if (d > l && d < h)
                return new double[] { (d - l) / (h - l), (h - d) / (h - l), 0.0 };

            return new double[] { 0.0, 1.0, 0.0 };
        }
    //    private String getDesCPUusageTerm(double d) {
    //        if (d > 90)
    //            return "veryhigh";
    //        else if (d <= 90 && d > 70)
    //            return "high";
    //        else if (d <= 40 && d > 15)
    //            return "low";
    //        else if (d <= 15)
    //            return "verylow";
    //        else
    //            return "medium";
    //    }
        private String getDesCPUusageTerm(double d) {
            if (d > 75)
                return "high";
            else if (d < 50)
                return "low";
            else
                return "medium";
        }

        private double[] getDestMobi(DataCenter datacenter, boolean mobile) {
            if (mobile && datacenter.getMobilityManager().isMobile())
                return new double[] { 1.0, 0.0, 0.0 };
            else if (mobile || datacenter.getMobilityManager().isMobile())
                return new double[] { 0.0, 1.0, 0.0 };
            return new double[] { 0.0, 0.0, 1.0 };
        }

        private String getDestMobiTerm(DataCenter datacenter, boolean mobile) {
            if (mobile && datacenter.getMobilityManager().isMobile())
                return "high";
            else if (mobile || datacenter.getMobilityManager().isMobile())
                return "medium";
            return "low";
        }

        private double[] getDestRemain(DataCenter datacenter) {
            double e;
            if (datacenter.getEnergyModel().isBatteryPowered())
                e = datacenter.getEnergyModel().getBatteryLevelPercentage();
            else
                e = 100;
            double l = 0;
            double h = 0;
            double m = 0;
            if (e <= 50) {
                h = 0;
                m = (e) / 50;
                l = (50 - e) / 50;
            } else {
                h = (e - 50) / 50;
                m = (100 - e) / 50;
                l = 0;
            }
            return new double[] { h, m, l };
        }

        private double[] getMobi(boolean mobile) {
            if (mobile)
                return new double[] { 1.0, 0.0, 0.0 };
            return new double[] { 0.0, 0.0, 1.0 };
        }

        private String getLatTerm(double maxLatency) {
            if (maxLatency < 20)
                return "low"; // low means delay sensitive
            else
                return "high";
        }

        private double[] getWanUsage(double d) {
            double l = 6, m = 10, h = 14;
            if (d >= h)
                return new double[] { 1.0, 0.0, 0.0 };
            if (d < l)
                return new double[] { 0.0, 0.0, 1.0 };
            if (d >= l && d < m)
                return new double[] { 0.0, (d - l) / (m - l), (m - d) / (m - l) };
            if (d > m && d < h)
                return new double[] { (d - m) / (h - m), (h - d) / (h - m), 0.0 };
            if (d == m)
                return new double[] { 0.0, 1.0, 0.0 };
            return null;
        }

        private String getWanUsageTerm(double d) {
            if (d <= 6)
                return "low";
            else if (d >= 14)
                return "high";
            else
                return "medium";
        }

        private double[] getTaskLength(double d) {
            if (d < 2000)
                return new double[] { 0.0, 0.0, 1.0 };
            if (d >= 18000)
                return new double[] { 1.0, 0.0, 0.0 };
            if (d <= 10000)
                return new double[] { 0.0, (d - 2000) / 8000, (10000 - d) / 8000 };
            return new double[] { (d - 10000) / 8000, (18000 - d) / 8000, 0.0 };
        }

        private String getTaskLengthTerm(long length) {
            if (length <= 6000)
                return "low";
            else if (length >= 14000)
                return "high";
            return "medium";
        }

        private double[] getLat(double maxLatency) {
            if (maxLatency <= 20)
                return new double[] { 0.0, 0.0, 1.0 };
            return new double[] { 1.0, 0.0, 0.0 };
        }




        @Override
        public void resultsReturned(Task task) {
            try {
                // if the used algorithm is called DECISION_TREE_RL
                if (simulationManager.getScenario().getStringOrchAlgorithm().equals("FDT")) {
                    // receive reinforcement
                    reinforcementRecieved(task, task.getStatus() == Cloudlet.Status.SUCCESS, true); // why it be modified just when status is success?
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }

        }


        /**
         * This function updates QS values and updates the decision tree. the Q table is
         * saved in a file and then used to generate the new tree, in order to get the
         * fuzzy rules, that are used in classification.
         */
        public void reinforcementRecieved(Task task, boolean reinforcement, boolean modify) throws IOException, InterruptedException {
            double delayPenalty = 0;
            if (!reinforcement){
                delayPenalty = task.getDelayTime() / task.getMaxLatency();
            }
            System.out.println("delayPenalty: " + delayPenalty + "task: " + task.getUid());
            // get first rule from the array (the rule of stage 1)
            String ruleToUpdate = ((String[]) task.getMetaData())[0];
           // String ruleToUpdate1 = ((String[]) task.getMetaData())[1];
            System.out.println("ruleToUpdate11: " + ruleToUpdate + "task: " + task.getUid());
            //System.out.println("ruleToUpdate22: " + ruleToUpdate1 + "task: " + task.getUid());
            System.out.println("action: " + ((DataCenter) task.getVm().getHost().getDatacenter()).getType() + "task: " + task.getUid() );

            String actionStates = ruleToUpdate+ "\r\n";
            File file = new File("PureEdgeSim/MyWork/action-state1.csv");
            try { DataOutputStream outstream
                    = new DataOutputStream(new FileOutputStream(file, true));
                outstream.write(actionStates.getBytes()); outstream.close(); } catch (IOException e)
            { e.printStackTrace(); }

            SimulationParameters.TYPES action = ((DataCenter) task.getVm().getHost().getDatacenter()).getType();
            updateQTable(Q_Table, ruleToUpdate, reinforcement ? 1 : 0, delayPenalty);
        }

        private void updateQTable( Vector<Q_table_row> q_Table, String ruleToUpdate, double reinforcement, double delayPenalty) throws InterruptedException {
            // browse the Q_table, in order to update the Q value of that rule
            boolean found = false;
            double Q_value = reinforcement;
            double actionReward = 0;
            double weightDone = 0.8;
            double weightCPU = 0.2;
            double avgCPU = avgCpuUtilization();
            System.out.println("avgCPU: " + avgCPU);


            for (int i = 0; i < q_Table.size(); i++) {
                if (q_Table.get(i).getRule().equals(ruleToUpdate)) {
                    found = true;

                    Q_value = q_Table.get(i).getQ_value();
                    //System.out.println("reinforcement:" + reinforcement);
                    //System.out.println("old+Q_value:" + Q_value);
                    //System.out.println("NumberOfReinforcements:" + q_Table.get(i).getNumberOfReinforcements());
                    //System.out.println("action" + action);
                    // update Q-value
                    q_Table.get(i).incrementNumberOfReinforcements();
                    //System.out.println("Q_table:" + q_Table);
                    double k = Math.min(q_Table.get(i).getNumberOfReinforcements(), 100);
                    //Q_value += 1 / k * (reinforcement - Q_value);
                    Q_value += 1 / k * ((weightDone * reinforcement) + (weightCPU * avgCPU) - delayPenalty - Q_value);
                    q_Table.get(i).setQ_value(Q_value);
                    //System.out.println("new_Q_value:" + Q_value);
                    //System.out.println("NumberOfReinforcements:" + q_Table.get(i).getNumberOfReinforcements());
                    //TimeUnit.SECONDS.sleep(1);
                    //System.out.println("Q_table2:" + q_Table.toString());
                    break;
                }
            }
            if (!found) {
                Q_table_row row = new Q_table_row(ruleToUpdate, 1, 1);
                q_Table.add(row);
            }
            //uncomment this part
    //		 if (reinforcement == 1) System.out.println("ruleToUpdate:" + ruleToUpdate); else
    //		 System.err.println("ruleToUpdate + Qvalue:" + ruleToUpdate + "   " + Q_value);


            try {
                UpdateFuzzyDecisionTree(q_Table);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void UpdateFuzzyDecisionTree(Vector<Q_table_row> q_Table) throws IOException {
            String rules = "";
                rules += "#Latency:High Medium Low\r\n" + "#WanUsage:High Medium Low\r\n"
                        + "#TaskLength:High Medium Low\r\n" + "#Mob:High Medium Low\r\n"
                        + "#DestCPUUsage:High Medium Low\r\n" + "#DestCPUMips:High Medium Low\r\n"
                        + "#DestRem:High Medium Low\r\n" + "#DestMob:High Medium Low\r\n"
                        + "@Offload:High Medium Low\r\n" + "=Data=";

            String readableRule = rules;
            for (int i = 0; i < q_Table.size(); i++) {
                rules += "\r\n" + convertRule(q_Table.get(i).getRule()) + " " + getClass(q_Table, q_Table.get(i));
                readableRule += "\r\n" + q_Table.get(i).getRule() + " " + getClass(q_Table, q_Table.get(i));
    //			System.out.println(q_Table.get(i).getRule());
    //			System.out.println(getClass(q_Table, q_Table.get(i), stage));
            }

            File file = new File("PureEdgeSim/MyWork/tree" + ".txt");
            DataOutputStream outstream = new DataOutputStream(new FileOutputStream(file, false));
            outstream.write(rules.getBytes());
            outstream.close();
            File file1 = new File("PureEdgeSim/MyWork/readabletree" + ".txt");
            DataOutputStream outstream1 = new DataOutputStream(new FileOutputStream(file1, false));
            outstream1.write(readableRule.getBytes());
            outstream.close();

        }

        private String convertRule(String rule) {
            return rule.replace("high", "1.0 0.0 0.0").replace("medium", "0.0 1.0 0.0").replace("low", "0.0 0.0 1.0")
                    .replace(",", " ");
        }

        private String getClass(Vector<Q_table_row> q_Table, Q_table_row row) {
                return fuzzify(row.getQ_value());
        }

        private String fuzzify(double new_Q_value) {
            double high = new_Q_value >= 0.5 ? (new_Q_value - 0.5) / 0.5 : 0;
            double medium = new_Q_value >= 0.5 ? (1 - new_Q_value) / 0.5 : new_Q_value / 0.5;
            double low = new_Q_value <= 0.5 ? (0.5 - new_Q_value) / 0.5 : 0;
            return high + " " + medium + " " + low;
        }

        private double avgCpuUtilization(){
            int edgeDevicesCount = 0;
            double averageMistCpuUtilization = 0;
            List<? extends DataCenter> datacentersList = simulationManager.getServersManager().getDatacenterList();
            for (DataCenter dc : datacentersList) {
                 if (dc.getType() == SimulationParameters.TYPES.EDGE_DEVICE && dc.getVmList().size() > 0) {
                    // only devices with computing capability
                    // the devices that have no VM are considered simple sensors, and will not be
                    // counted here
                    averageMistCpuUtilization += dc.getResources().getAvgCpuUtilization();
                    edgeDevicesCount++;
                }

            }
            System.out.println("in Avgcpu fun: " + (averageMistCpuUtilization/ edgeDevicesCount));
            return (averageMistCpuUtilization/ edgeDevicesCount) / 100;
        }

    }
}
