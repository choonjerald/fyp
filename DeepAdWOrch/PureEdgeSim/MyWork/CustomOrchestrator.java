package MyWork;

import com.mechalikh.pureedgesim.DataCentersManager.DataCenter;
import com.mechalikh.pureedgesim.ScenarioManager.SimulationParameters;
import com.mechalikh.pureedgesim.SimulationManager.SimLog;
import com.mechalikh.pureedgesim.SimulationManager.SimulationManager;
import com.mechalikh.pureedgesim.TasksGenerator.Task;
import com.mechalikh.pureedgesim.TasksOrchestration.Orchestrator;
import net.sourceforge.jFuzzyLogic.FIS;
import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import src.com.fdtkit.fuzzy.data.Attribute;
import src.com.fdtkit.fuzzy.data.Dataset;
import src.com.fdtkit.fuzzy.data.Row;
import src.com.fdtkit.fuzzy.fuzzydt.FuzzyDecisionTree;
import src.com.fdtkit.fuzzy.fuzzydt.TreeNode;
import src.com.fdtkit.fuzzy.utils.AmbiguityMeasure;
import src.com.fdtkit.fuzzy.utils.LeafDeterminer;
import src.com.fdtkit.fuzzy.utils.LeafDeterminerBase;
import src.com.fdtkit.fuzzy.utils.PreferenceMeasure;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class CustomOrchestrator extends Orchestrator {
    private TreeNode root;
    FuzzyDecisionTree fuzzydecisionTree;
    FIS fis;
    Vector<Q_table_row> Q_Table = new Vector<Q_table_row>();
    public int exploration = 0;
    public  int exploitation = 0;


    public CustomOrchestrator(SimulationManager simulationManager) throws IOException {
        super(simulationManager);
        // initialize datasets files
        UpdateFuzzyDecisionTree(Q_Table);
    }

    @Override
    protected int findVM(String[] architecture, Task task) {
        if (simulationManager.getScenario().getStringOrchAlgorithm().equals("FDT"))
            try {
              //  System.out.println("lets find a Vm for task : " + task.getUid());

                fuzzydecisionTree = getDecisionTree();
                return DecisionTree(architecture, task);
            } catch (Exception e) {
                e.printStackTrace();
            } else if (simulationManager.getScenario().getStringOrchAlgorithm().equals("FUZZY_LOGIC")) {
            String fileName = "PureEdgeSim/MyWork/stage.fcl";
            fis = FIS.load(fileName, true);
            // Error while loading?
            if (fis == null) {
                System.err.println("Can't load file: '" + fileName + "'");
                return -1;
            }
            return FuzzyLogic(architecture, task);
        } else {
            SimLog.println("");
            SimLog.println("Custom Orchestrator- Unknnown orchestration algorithm '" + algorithm
                    + "', please check the simulation parameters file...");
            // Cancel the simulation
            Runtime.getRuntime().exit(0);
        }
        return -1;
    }

    private FuzzyDecisionTree getDecisionTree() {
        Dataset d = new Dataset("Sample1");

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("PureEdgeSim/MyWork/tree" + ".txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String line = "";
        String className = "";
        try {
            while ((line = br.readLine()) != null) {

                if (line.startsWith("#")) {
                    String[] terms = line.substring(line.indexOf(":") + 1).split(" ");
                    d.addAttribute(new Attribute(line.substring(1, line.indexOf(":")), terms));
                } else if (line.startsWith("@")) {
                    String[] terms = line.substring(line.indexOf(":") + 1).split(" ");
                    d.addAttribute(new Attribute(line.substring(1, line.indexOf(":")), terms));
                    className = line.substring(1, line.indexOf(":"));
                    d.setClassName(className);
                } else if (!line.startsWith("=Data=")) {
                    String[] data = line.split(" ");
                    Object[] crispRow = new Object[d.getAttributesCount()];
                    double[][] fuzzyValues = new double[d.getAttributesCount()][];
                    int k = 0;
                    //System.out.println("crispRow.length: " + crispRow.length);
                    for (int i = 0; i < crispRow.length; i++) {
                        crispRow[i] = "Dummy";
                        // fuzzyValues[i] = getFuzzyValues(data[i]);
                        fuzzyValues[i] = new double[d.getAttribute(i).getLinguisticTermsCount()];
                       // System.out.println("fuzzyValues[i].length " + fuzzyValues[i].length );
                        for (int j = 0; j < fuzzyValues[i].length; j++) {
                            fuzzyValues[i][j] = Double.parseDouble(data[k++]);
                       //     System.out.println("i:  " + i + " j: " + j + " k: " + k + " fuzzyValues[i][j]: " + fuzzyValues[i][j]);
                        }
                    }

                    d.addRow(new Row(crispRow, fuzzyValues));

                }
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        PreferenceMeasure preferenceMeasure;
        LeafDeterminer leafDeterminer;
        FuzzyDecisionTree descisionTree;
        preferenceMeasure = new AmbiguityMeasure(0.5);
        leafDeterminer = new LeafDeterminerBase(0.9);

        descisionTree = new FuzzyDecisionTree(preferenceMeasure, leafDeterminer);

        root = descisionTree.buildTree(d);


        // Uncomment to print fuzzy rules

        String[] rulesArray = descisionTree.generateRules(root);
        String rules = "======================================== \r\n ===================================== \r\n";
        for (String rule : rulesArray)
            rules += rule + "\r\n";
        File file = new File("PureEdgeSim/MyWork/readable_rules_" );
        try { DataOutputStream outstream = new DataOutputStream(new FileOutputStream(file, false));
            outstream.write(rules.getBytes()); outstream.close(); }
        catch (IOException e)
        { e.printStackTrace(); }



        return descisionTree;
    }

    private int DecisionTree(String[] architecture, Task task) throws Exception {
     //   System.out.println("task : " + task.getUid() + "in DecisionTree ");
        double max = -1;
        int vm = -1;
        int edgeDateCenter = 0;
        double minDistance = maxDistance() * 3;
        String rule = "";
        String ruleValues = "";
        double[] lat = getLat(task.getMaxLatency());
        double[] taskLength = getTaskLength(task.getLength());
        double[] wanUsage = getWanUsage(simulationManager.getNetworkModel().getWanUtilization());
        double[] userMobi = getMobi(task.getEdgeDevice().getMobilityManager().isMobile());

     //   System.out.println("orchestrationHistory: " + orchestrationHistory.size());
//        int decision;
//        double random = new Random().nextFloat();
//        if (random <= 6 / simulationManager.getSimulation().clock()) { // explore
//            vm = new Random().nextInt(orchestrationHistory.size());
//            exploration++;
//            System.out.println(" Random decision  " + vm);
//        } else {

            for (int i = 0; i < orchestrationHistory.size(); i++) { //orchestrationHistory.size() is equal to
                                                                    // number of all VMs in the system
                if (offloadingIsPossible(task, vmList.get(i), architecture)
                        && vmList.get(i).getStorage().getCapacity() > 0) {
                    if (((DataCenter) vmList.get(i).getHost().getDatacenter())
					.getType() == SimulationParameters.TYPES.EDGE_DATACENTER){
                        System.out.println(" VM " + i + " is Edge Data center ");
                        edgeDateCenter++;
                    }

                    double[] destcpuUsage = getDesCPUusage(
                            ((DataCenter) vmList.get(i).getHost().getDatacenter()).getResources()
                                    .getCurrentCpuUtilization()); // I changed * 100 to / 10
                                  //  System.out.println("destcpuUsage: " + destcpuUsage[0] + destcpuUsage[1] + destcpuUsage[2]);
                    double[] destCpuMips =getDesCPU(((DataCenter) vmList.get(i).getHost().getDatacenter())
                            .getResources().getTotalMips());

                    double[] destRemai = getDestRemain((DataCenter) vmList.get(i).getHost().getDatacenter());
                    double[] desMobi = this.getDestMobi((DataCenter) vmList.get(i).getHost().getDatacenter(),
                            task.getEdgeDevice().getMobilityManager().isMobile());
                    Dataset d = getDataset(lat, taskLength, wanUsage, userMobi, destcpuUsage, destCpuMips, destRemai, desMobi);

                    double[] cVals = fuzzydecisionTree.classify(0, d, "Offload", fuzzydecisionTree.generateRules(root));

                    // to save the rule used to offload this task, this rule will be used later when
                    // updating the Q values
                    // the rule will be stored in the task metadata object for easy access
                    rule = generateRule( i , task);
                    ruleValues = generateRuleValues( i , task);

//                    System.out.println(" Rule  " + rule);
//                    System.out.println(" Rule  " + ruleValues);
//
//
//                    System.out.println(" cVals  " + cVals[0] + cVals[1] + cVals[2]);
                    double distance = ((DataCenter) vmList.get(i).getHost().getDatacenter()).getMobilityManager()
                            .distanceTo(task.getEdgeDevice());
                  //  System.out.println(" Distance to orchestrator  " + distance);
                    // if the class = high, high = the expected success rate is high or the
                    // suitability of the device, so we should offload to this device
                    // cVals is an array in which the fuzzified values are stored.
                    // cVals[0]= the membership of degree of fuzzy set "high"
                    // cVals{1] is for the fuzzy set "medium"
                    // and cVals[2] is for the low one.
                    if (cVals[0] >= cVals[1] && (max == -1 || max <= cVals[0] + 2) && minDistance > distance) {
                    // it keeps the highest VM
                        // if the membership degree of "high" is bigger than "medium"(
                        // cVals[0]>=cVals[1]) and if it is the first time (max==-1), or it is not the
                        // first time but the old max is below the membership degree of "high".
                        // then select the this device/vm as to execute the task
                        vm = i;
                      //  System.out.println("task: " + task.getUid() + " vm1:" + vm);

                        // update the max value with the new membership degree of "high" fuzzy set
                        // if the wake it here, this means that we have found at least one device that
                        // is classifed as "high".This means that any other device that is classified as
                        // "medium" or "low" should be ignored. Since we are using a signle variable
                        // "max" to store the highest membership degree for any of those fuzzy sets
                        // (high, medium, low). We need to keep the superiority of "high" over "medium"
                        // and "medium" over "low".
                        // e.g., if the classification of a device gives 0.6 for the "high" fuzzy set.
                        // and the next one gives 1 but for "low" fuzzy set. In this case 0.6>1, to do
                        // this we simply add 2 to 0.6, so we will have 2.6 >
                        // 1.
                        max = cVals[0] + 2;

                    }// if the class = medium
                    else if (cVals[1] >= cVals[2]) {
                        if ((max == -1 || max <= cVals[1] + 1)  && minDistance > distance + maxDistance() ) {
                            max = cVals[1] + 1; // I changed cVals[0] + 1 to cVals[1] + 1
                            vm = i;
                          //  System.out.println("task: " + task.getUid() + " vm2:" + vm);

                        }
                    } else {
                        // the class= low
                        if ((max == -1 || max < cVals[2])  && (minDistance > distance + (2 * maxDistance()))) {
                            max = cVals[2];
                            vm = i;
                           // System.out.println("task: " + task.getUid() + " vm3:" + vm);
                        }
                    }

                }

            }
 //       }
       // System.out.println("task: " + task.getUid() + " vm4:" + vm);
        if (vm != -1) {
//            System.out.println("task: " + task.getUid() + " vm5:" + vm);

            // save the rule in the task metadata for later use
            task.setMetaData(new String[] { rule, ruleValues });
            // return the offloading destination
            System.out.println("task: " + task.getUid() + " Dest Vm:" + vm);
            return vm;
        } else
            return -1;
    }
    private double checkEnoughResource(Task task , int i ){
        double freeResource = (((DataCenter) vmList.get(i).getHost().getDatacenter())
                .getResources().getTotalMips()) * (1 - (((DataCenter) vmList.get(i).getHost().getDatacenter())
                .getResources().getCurrentCpuUtilization() / 100) );
        double minCapacity = (freeResource - (task.getLength() * 1.3));
        return  minCapacity;

    }

    private Dataset getDataset(double[] lat, double[] wanusage, double[] tasklength, double[] mobi, double[] destcpuusage,
                               double[] destcpumips, double[] destremai, double[] desmobi) {
        Dataset d = new Dataset("Sample1");

        // Add the attributes with Linguistic terms
        d.addAttribute(new Attribute("Latency", new String[] { "High", "Medium", "Low" }));
        d.addAttribute(new Attribute("WanUsage", new String[] { "High", "Medium", "Low" }));
        d.addAttribute(new Attribute("TaskLength", new String[] { "High", "Medium", "Low" }));
        d.addAttribute(new Attribute("Mob", new String[] { "High", "Medium", "Low" }));
        d.addAttribute(new Attribute("DestCPUUsage", new String[] { "High", "Medium", "Low" }));
        d.addAttribute(new Attribute("DestCPUMips", new String[] { "High", "Medium", "Low" }));
        d.addAttribute(new Attribute("DestRem", new String[] { "High", "Medium", "Low" }));
        d.addAttribute(new Attribute("DestMob", new String[] { "High", "Medium", "Low" }));
        d.addAttribute(new Attribute("Offload", new String[] { "High", "Medium", "Low" }));
        double[][] columns = { lat, wanusage,tasklength,mobi, destcpuusage, destcpumips, destremai, desmobi };

        d.addRow(new Row(new Object[] { "Dummy", "Dummy", "Dummy", "Dummy", "Dummy", "Dummy", "Dummy", "Dummy", "Dummy" }, columns));

        return d;
    }


    private String generateRule(int vm , Task task){
        String rule = "";
        rule = getLatTerm(task.getMaxLatency()) + ","
                + getWanUsageTerm(simulationManager.getNetworkModel().getWanUtilization()) + ","
                + getTaskLengthTerm(task.getLength()) + ","
                + (task.getEdgeDevice().getMobilityManager().isMobile() ? "high" : "low") + ","
                + getDesCPUusageTerm(((DataCenter) vmList.get(vm).getHost().getDatacenter())
                .getResources().getCurrentCpuUtilization())+","
                +getDesCPUTerm(((DataCenter) vmList.get(vm).getHost().getDatacenter())
                .getResources().getTotalMips()) + ","
                +getDestRemainTerm((DataCenter) vmList.get(vm).getHost().getDatacenter()) + ","
                + getDestMobiTerm((DataCenter) vmList.get(vm).getHost().getDatacenter(),
                task.getEdgeDevice().getMobilityManager().isMobile());
        return rule;
    }

    private String generateRuleValues( int vm, Task task){
        String ruleValues = "";
        ruleValues = task.getMaxLatency() + ","
                + simulationManager.getNetworkModel().getWanUtilization()+ ","
                + Double.toString(task.getLength())+ ","
                + (task.getEdgeDevice().getMobilityManager().isMobile() ? "high" : "low") + ","
                + ((DataCenter) vmList.get(vm).getHost().getDatacenter())
                .getResources().getCurrentCpuUtilization() +","
                + ((DataCenter) vmList.get(vm).getHost().getDatacenter())
                .getResources().getAvgCpuUtilization() +","
                +((DataCenter) vmList.get(vm).getHost().getDatacenter())
                .getResources().getTotalMips() + ","
                +getDestRemainValue (((DataCenter) vmList.get(vm).getHost().getDatacenter()))+ ","
                + (((DataCenter) vmList.get(vm).getHost().getDatacenter())
                .getMobilityManager().isMobile() ? "high" : "low");
        return ruleValues;
    }

//    private double[] getDestination(String destiantion) {
//        if (destiantion.equals("cloud"))
//            return new double[] { 1.0, 0.0, 0.0 };
//        else if (destiantion.equals("edge"))
//            return new double[] { 0.0, 1.0, 0.0 };
//        else
//            return new double[] { 0.0, 0.0, 1.0 };
//    }
    private double maxDistance() {
        return Math.abs(Math.sqrt(Math.pow((SimulationParameters.AREA_WIDTH / 2) , 2)
                + Math.pow(((SimulationParameters.AREA_LENGTH / 2) ), 2)));
}

    private double[] getDesCPU(double d) {
        double l = 30000, m = 65000, h = 1300000;
        if (d >= h)
            return new double[] { 1.0, 0.0, 0.0 };
        if (d < l)
            return new double[] { 0.0, 0.0, 1.0 };
        if (d >= l && d < m)
            return new double[] { 0.0, (d - l) / (m - l), (m - d) / (m - l) };
        if (d > m && d < h)
            return new double[] { (d - m) / (h - m), (h - d) / (h - m), 0.0 };
        if (d == m)
            return new double[] { 0.0, 1.0, 0.0 };
        return null;
    }

    private String getDesCPUTerm(double mips) {
        if (mips >= 130000)
            return "high";
        else if (mips < 30000)
            return "low";
        else
            return "medium";
    }

    private String getDesCPUUsageTerm(double u) {
        if (u >= 4)
            return "high";
        else if (u <= 1)
            return "low";
        else
            return "medium";
    }

    private String getDestRemainTerm(DataCenter datacenter) {
        double e;
        if (datacenter.getEnergyModel().isBatteryPowered())
            e = datacenter.getEnergyModel().getBatteryLevelPercentage();
        else
            e = 100;
        if (e >= 75)
            return "high";
        else if (e <= 25)
            return "low";
        else
            return "medium";
    }

    private String getDestRemainValue(DataCenter datacenter) {
        double e;
        if (datacenter.getEnergyModel().isBatteryPowered())
            e = datacenter.getEnergyModel().getBatteryLevelPercentage();
        else
            e = 100;
        return Double.toString(e);
    }

    private double[] getDesCPUusage(double d) {
        double l = 50;
        double h = 100;
        if (d <= 0)
            return new double[] { 0.0, 0.0, 1.0 };
        if (d >= h)
            return new double[] { 1.0, 0.0, 0.0 };
        if (d > 0 && d < l)
            return new double[] { 0.0, (d - 0) / (h - l), (l - d) / (h - l) };
        if (d > l && d < h)
            return new double[] { (d - l) / (h - l), (h - d) / (h - l), 0.0 };

        return new double[] { 0.0, 1.0, 0.0 };
    }
    private String getDesCPUusageTerm(double d) {
        if (d > 75)
            return "high";
        else if (d < 50)
            return "low";
        else
            return "medium";
    }

    private double[] getDestMobi(DataCenter datacenter, boolean mobile) {
        if (mobile && datacenter.getMobilityManager().isMobile())
            return new double[] { 1.0, 0.0, 0.0 };
        else if (mobile || datacenter.getMobilityManager().isMobile())
            return new double[] { 0.0, 1.0, 0.0 };
        return new double[] { 0.0, 0.0, 1.0 };
    }

    private String getDestMobiTerm(DataCenter datacenter, boolean mobile) {
        if (mobile && datacenter.getMobilityManager().isMobile())
            return "high";
        else if (mobile || datacenter.getMobilityManager().isMobile())
            return "medium";
        return "low";
    }

    private double[] getDestRemain(DataCenter datacenter) {
        double e;
        if (datacenter.getEnergyModel().isBatteryPowered())
            e = datacenter.getEnergyModel().getBatteryLevelPercentage();
        else
            e = 100;
        double l = 0;
        double h = 0;
        double m = 0;
        if (e <= 50) {
            h = 0;
            m = (e) / 50;
            l = (50 - e) / 50;
        } else {
            h = (e - 50) / 50;
            m = (100 - e) / 50;
            l = 0;
        }
        return new double[] { h, m, l };
    }

    private double[] getMobi(boolean mobile) {
        if (mobile)
            return new double[] { 1.0, 0.0, 0.0 };
        return new double[] { 0.0, 0.0, 1.0 };
    }

    private String getLatTerm(double maxLatency) {
        if (maxLatency < 20)
            return "low"; // low means delay sensitive
        else
            return "high";
    }

    private double[] getWanUsage(double d) {
        double l = 6, m = 10, h = 14;
        if (d >= h)
            return new double[] { 1.0, 0.0, 0.0 };
        if (d < l)
            return new double[] { 0.0, 0.0, 1.0 };
        if (d >= l && d < m)
            return new double[] { 0.0, (d - l) / (m - l), (m - d) / (m - l) };
        if (d > m && d < h)
            return new double[] { (d - m) / (h - m), (h - d) / (h - m), 0.0 };
        if (d == m)
            return new double[] { 0.0, 1.0, 0.0 };
        return null;
    }

    private String getWanUsageTerm(double d) {
        if (d <= 6)
            return "low";
        else if (d >= 14)
            return "high";
        else
            return "medium";
    }

    private double[] getTaskLength(double d) {
        if (d < 2000)
            return new double[] { 0.0, 0.0, 1.0 };
        if (d >= 18000)
            return new double[] { 1.0, 0.0, 0.0 };
        if (d <= 10000)
            return new double[] { 0.0, (d - 2000) / 8000, (10000 - d) / 8000 };
        return new double[] { (d - 10000) / 8000, (18000 - d) / 8000, 0.0 };
    }

    private String getTaskLengthTerm(long length) {
        if (length <= 6000)
            return "low";
        else if (length >= 14000)
            return "high";
        return "medium";
    }

    private double[] getLat(double maxLatency) {
        if (maxLatency <= 20)
            return new double[] { 0.0, 0.0, 1.0 };
        return new double[] { 1.0, 0.0, 0.0 };
    }




    @Override
    public void resultsReturned(Task task) {
        try {
            // if the used algorithm is called DECISION_TREE_RL
            if (simulationManager.getScenario().getStringOrchAlgorithm().equals("FDT")) {
                // receive reinforcement
                reinforcementRecieved(task, task.getStatus() == Cloudlet.Status.SUCCESS, true); // why it be modified just when status is success?
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

    }


    /**
     * This function updates QS values and updates the decision tree. the Q table is
     * saved in a file and then used to generate the new tree, in order to get the
     * fuzzy rules, that are used in classification.
     */
    public void reinforcementRecieved(Task task, boolean reinforcement, boolean modify) throws IOException, InterruptedException {
//        if (!reinforcement)
//            double delay = task.getDelayTime();
//        else
//            delay = 0;
        // get first rule from the array (the rule of stage 1)
        String ruleToUpdate = ((String[]) task.getMetaData())[0];
        String ruleToUpdateValues = ((String[]) task.getMetaData())[1];
       // System.out.println("ruleToUpdate11: " + ruleToUpdate + "task: " + task.getUid());
        //System.out.println("ruleToUpdate22: " + ruleToUpdate1 + "task: " + task.getUid());
       // System.out.println("action: " + ((DataCenter) task.getVm().getHost().getDatacenter()).getType() + "task: " + task.getUid() );

        String actionStates = task.getTime() + "," + task.getUid() + "," + ruleToUpdate + "," +  task.getEdgeDevice()+
                ","+ ((DataCenter) task.getVm().getHost().getDatacenter()).getId()+ "," + reinforcement+ "\r\n";
        String actionStatesValue = task.getTime() + "," + task.getUid() + "," + ruleToUpdateValues + ","
                +  task.getEdgeDevice()+ ","+ (task.getVm().getHost().getDatacenter()).getId()+ ","
                + reinforcement+ "\r\n";
        File file = new File("PureEdgeSim/MyWork/action-state_rule.csv");
        try { DataOutputStream outstream
                = new DataOutputStream(new FileOutputStream(file, true));
            outstream.write(actionStates.getBytes()); outstream.close(); } catch (IOException e)
        { e.printStackTrace(); }
        File file1 = new File("PureEdgeSim/MyWork/action-state_ruleValue.csv");
        try { DataOutputStream outstream
                = new DataOutputStream(new FileOutputStream(file1, true));
            outstream.write(actionStatesValue.getBytes()); outstream.close(); } catch (IOException e)
        { e.printStackTrace(); }

        SimulationParameters.TYPES action = ((DataCenter) task.getVm().getHost().getDatacenter()).getType();
        updateQTable(Q_Table, ruleToUpdate, reinforcement ? 1 : 0, action);
    }

    private void updateQTable( Vector<Q_table_row> q_Table, String ruleToUpdate, double reinforcement, SimulationParameters.TYPES action) throws InterruptedException {
        // browse the Q_table, in order to update the Q value of that rule
        boolean found = false;
        double Q_value = reinforcement;
        double actionReward = 0;
        double weightDone = 0.8;
        double weightCPU = 0.2;

        for (int i = 0; i < q_Table.size(); i++) {
            if (q_Table.get(i).getRule().equals(ruleToUpdate)) {
                found = true;

                Q_value = q_Table.get(i).getQ_value();
                //System.out.println("reinforcement:" + reinforcement);
                //System.out.println("old+Q_value:" + Q_value);
                //System.out.println("NumberOfReinforcements:" + q_Table.get(i).getNumberOfReinforcements());
                //System.out.println("action" + action);
                // update Q-value
                q_Table.get(i).incrementNumberOfReinforcements();
                //System.out.println("Q_table:" + q_Table);
                double k = Math.min(q_Table.get(i).getNumberOfReinforcements(), 100);
                Q_value += 1 / k * (reinforcement - Q_value);
               // Q_value += 1 / k * ((weightDone * reinforcement) + (weightCPU * avgCPU) - Q_value);
                q_Table.get(i).setQ_value(Q_value);
                //System.out.println("new_Q_value:" + Q_value);
                //System.out.println("NumberOfReinforcements:" + q_Table.get(i).getNumberOfReinforcements());
                //TimeUnit.SECONDS.sleep(1);
                //System.out.println("Q_table2:" + q_Table.toString());
                break;
            }
        }
        if (!found) {
            Q_table_row row = new Q_table_row(ruleToUpdate, 1, 1);
            q_Table.add(row);
        }
        //uncomment this part
//		 if (reinforcement == 1) System.out.println("ruleToUpdate:" + ruleToUpdate); else
//		 System.err.println("ruleToUpdate + Qvalue:" + ruleToUpdate + "   " + Q_value);


        try {
            UpdateFuzzyDecisionTree(q_Table);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void UpdateFuzzyDecisionTree(Vector<Q_table_row> q_Table) throws IOException {
        String rules = "";
            rules += "#Latency:High Medium Low\r\n" + "#WanUsage:High Medium Low\r\n"
                    + "#TaskLength:High Medium Low\r\n" + "#Mob:High Medium Low\r\n"
                    + "#DestCPUUsage:High Medium Low\r\n" + "#DestCPUMips:High Medium Low\r\n"
                    + "#DestRem:High Medium Low\r\n" + "#DestMob:High Medium Low\r\n"
                    + "@Offload:High Medium Low\r\n" + "=Data=";

        String readableRule = rules;
        for (int i = 0; i < q_Table.size(); i++) {
            rules += "\r\n" + convertRule(q_Table.get(i).getRule()) + " " + getClass(q_Table, q_Table.get(i));
            readableRule += "\r\n" + q_Table.get(i).getRule() + " " + getClass(q_Table, q_Table.get(i));
//			System.out.println(q_Table.get(i).getRule());
//			System.out.println(getClass(q_Table, q_Table.get(i), stage));
        }

        File file = new File("PureEdgeSim/MyWork/tree" + ".txt");
        DataOutputStream outstream = new DataOutputStream(new FileOutputStream(file, false));
        outstream.write(rules.getBytes());
        outstream.close();
        File file1 = new File("PureEdgeSim/MyWork/readabletree" + ".txt");
        DataOutputStream outstream1 = new DataOutputStream(new FileOutputStream(file1, false));
        outstream1.write(readableRule.getBytes());
        outstream.close();

    }

    private String convertRule(String rule) {
        return rule.replace("high", "1.0 0.0 0.0").replace("medium", "0.0 1.0 0.0").replace("low", "0.0 0.0 1.0")
                .replace(",", " ");
    }

    private String getClass(Vector<Q_table_row> q_Table, Q_table_row row) {
            return fuzzify(row.getQ_value());
    }

    private String fuzzify(double new_Q_value) {
        double high = new_Q_value >= 0.5 ? (new_Q_value - 0.5) / 0.5 : 0;
        double medium = new_Q_value >= 0.5 ? (1 - new_Q_value) / 0.5 : new_Q_value / 0.5;
        double low = new_Q_value <= 0.5 ? (0.5 - new_Q_value) / 0.5 : 0;
        return high + " " + medium + " " + low;
    }

    private double avgCpuUtilization(){
        int edgeDevicesCount = 0;
        double averageMistCpuUtilization = 0;
        List<? extends DataCenter> datacentersList = simulationManager.getServersManager().getDatacenterList();
        for (DataCenter dc : datacentersList) {
             if (dc.getType() == SimulationParameters.TYPES.EDGE_DEVICE && dc.getVmList().size() > 0) {
                // only devices with computing capability
                // the devices that have no VM are considered simple sensors, and will not be
                // counted here
                averageMistCpuUtilization += dc.getResources().getAvgCpuUtilization();
                edgeDevicesCount++;
            }

        }
       // System.out.println("in Avgcpu fun: " + (averageMistCpuUtilization/ edgeDevicesCount));
        return (averageMistCpuUtilization/ edgeDevicesCount) / 100;
    }


    private int FuzzyLogic(String[] architecture, Task task) {
        System.out.println(" Task: " + task.getUid() + " is in FuzzyLogic with architecture " + architecture.toString());

        double min = -1;
        int vm = -1;

        fis.setVariable("wan", simulationManager.getNetworkModel().getWanUtilization());
        fis.setVariable("tasklength", task.getLength());
        fis.setVariable("delay", task.getMaxLatency());
       // fis.setVariable("vm", vmUsage * 10 / count); //10?
        String[] architecture2 = { "Mist" };
        for (int i = 0; i < orchestrationHistory.size(); i++) {
            if (offloadingIsPossible(task, vmList.get(i), architecture2)
                    && vmList.get(i).getStorage().getCapacity() > 0) {
                vmList.get(i).getHost().getId();
                if (!task.getEdgeDevice().getMobilityManager().isMobile())
                    fis.setVariable("vm_local", 0);
                else
                    fis.setVariable("vm_local", 0);
                fis.setVariable("vm", (1 - vmList.get(i).getCpuPercentUtilization()) * vmList.get(i).getMips() / 1000);
                fis.evaluate();
                System.out.println("Task: " + task.getUid() + " vm variable : " + fis.getVariable("vm")
                        + " vm variable : " + fis.getVariable("vm_local"));
                System.out.println( "offload : " + fis.getVariable("offload"));

                if (min == -1 || min > fis.getVariable("offload").defuzzify()) {
                    min = fis.getVariable("offload").defuzzify();
                    System.out.println( "min : " + min);
                    vm = i;
                }
            }
        }
        System.out.println("stag2 : Dest VM : " +  vm);
        return vm;

    }




    private List<Integer> accessableVMs(Task task, String[] architecture){
        int vmNum = 0;
        List<Integer> accessableVMs =new ArrayList<Integer>();
        //int accessableVMs[] = new int[orchestrationHistory.size()];
        int edgeDateCenter = 0;
        for (int i = 0; i < orchestrationHistory.size(); i++) { //orchestrationHistory.size() is equal to
            // number of all VMs in the system
            if (offloadingIsPossible(task, vmList.get(i), architecture)
                    && vmList.get(i).getStorage().getCapacity() > 0) {
                if (((DataCenter) vmList.get(i).getHost().getDatacenter())
                        .getType() == SimulationParameters.TYPES.EDGE_DATACENTER) {
                    System.out.println(" VM " + i + " is Edge Data center ");
                    edgeDateCenter++;

                }
                accessableVMs.add(vmNum , i);
                vmNum++;
            }
        }
        return accessableVMs;
    }

}
