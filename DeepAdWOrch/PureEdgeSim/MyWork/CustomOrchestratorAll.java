package MyWork;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mechalikh.pureedgesim.DataCentersManager.DataCenter;
import com.mechalikh.pureedgesim.ScenarioManager.SimulationParameters;
import com.mechalikh.pureedgesim.SimulationManager.SimLog;
import com.mechalikh.pureedgesim.SimulationManager.SimulationManager;
import com.mechalikh.pureedgesim.TasksGenerator.Task;
import com.mechalikh.pureedgesim.TasksOrchestration.Orchestrator;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import net.sourceforge.jFuzzyLogic.FIS;
import org.apache.commons.lang3.StringEscapeUtils;
import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import src.com.fdtkit.fuzzy.data.Attribute;
import src.com.fdtkit.fuzzy.data.Dataset;
import src.com.fdtkit.fuzzy.data.Row;
import src.com.fdtkit.fuzzy.fuzzydt.FuzzyDecisionTree;
import src.com.fdtkit.fuzzy.fuzzydt.TreeNode;
import src.com.fdtkit.fuzzy.utils.AmbiguityMeasure;
import src.com.fdtkit.fuzzy.utils.LeafDeterminer;
import src.com.fdtkit.fuzzy.utils.LeafDeterminerBase;
import src.com.fdtkit.fuzzy.utils.PreferenceMeasure;

import java.io.*;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class CustomOrchestratorAll extends Orchestrator {
    private TreeNode root;

    FuzzyDecisionTree fuzzydecisionTree;
    FIS fis;
    Vector<Q_table_row> Q_Table = new Vector<Q_table_row>();
    public int exploration = 0;
    public  int exploitation = 0;

    private static HttpServer server;

    static {
        try {
            ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(4);
            server = HttpServer.create(new InetSocketAddress("localhost", 5001), 100);
            server.createContext("/get_reward", new ActionHandler());
            server.setExecutor(threadPoolExecutor);
            server.start();
            System.out.println("Started http server on " + server.getAddress());
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(-1);
        }
    }

    //    public static String sendState(String state) throws IOException {
//
//        var url = "http://localhost:5000/agent?" + state;
//        System.out.println("url: " + url);
//
//        try {
//
//            var myurl = new URL(url);
//            con = (HttpURLConnection) myurl.openConnection();
//            con.setRequestMethod("GET");
//
//            StringBuilder content;
//
//            try (BufferedReader in = new BufferedReader(
//                    new InputStreamReader(con.getInputStream()))) {
//
//                String line;
//                content = new StringBuilder();
//
//                while ((line = in.readLine()) != null) {
//
//                    content.append(line);
//                    content.append(System.lineSeparator());
//                }
//            }
//
//            System.out.println(content.toString());
//            return content.toString();
//
//        } finally {
//
//            con.disconnect();
//        }
//    }

    static class ActionHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange exchange) throws IOException {
            InputStreamReader inputStreamReader = new InputStreamReader(exchange.getRequestBody(), StandardCharsets.UTF_8);
            BufferedReader br = new BufferedReader(inputStreamReader);
            int b;
            StringBuilder stringBuilder = new StringBuilder();
            while ((b = br.read()) != -1) {
                stringBuilder.append((char) b);
            }
            br.close();
            inputStreamReader.close();
            String jsonString = stringBuilder.toString();
            System.out.println(jsonString);

            // Get the reward for this action

            double randomNum = Math.random();
            ObjectMapper objectMapper = new ObjectMapper();
            Map<String, Double> data = new HashMap<>();
            data.put("reward", randomNum);
            OutputStream outputStream = exchange.getResponseBody();
            String response = objectMapper.writeValueAsString(data);
            response = StringEscapeUtils.escapeHtml4(response);
            exchange.sendResponseHeaders(200, response.length());
            outputStream.write(response.getBytes(StandardCharsets.UTF_8));
            outputStream.flush();
            outputStream.close();
        }
    }

    public CustomOrchestratorAll(SimulationManager simulationManager) throws IOException {
        super(simulationManager);
        // initialize datasets files
        UpdateFuzzyDecisionTree(Q_Table);
    }

    @Override
    protected int findVM(String[] architecture, Task task) {
        if (simulationManager.getScenario().getStringOrchAlgorithm().equals("FDT"))
            try {
                System.out.println("lets find a Vm for task : " + task.getUid());

                fuzzydecisionTree = getDecisionTree();
                return DecisionTree(architecture, task);
            } catch (Exception e) {
                e.printStackTrace();
            }else {
            SimLog.println("");
            SimLog.println("Custom Orchestrator- Unknnown orchestration algorithm '" + algorithm
                    + "', please check the simulation parameters file...");
            // Cancel the simulation
            Runtime.getRuntime().exit(0);
        }
        return -1;
    }

    private FuzzyDecisionTree getDecisionTree() {
        Dataset d = new Dataset("Sample1");

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("PureEdgeSim/MyWork/tree" + ".txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String line = "";
        String className = "";
        try {
            while ((line = br.readLine()) != null) {

                if (line.startsWith("#")) {
                    String[] terms = line.substring(line.indexOf(":") + 1).split(" ");
                    d.addAttribute(new Attribute(line.substring(1, line.indexOf(":")), terms));
                } else if (line.startsWith("@")) {
                    String[] terms = line.substring(line.indexOf(":") + 1).split(" ");
                    d.addAttribute(new Attribute(line.substring(1, line.indexOf(":")), terms));
                    className = line.substring(1, line.indexOf(":"));
                    d.setClassName(className);
                } else if (!line.startsWith("=Data=")) {
                    String[] data = line.split(" ");
                    Object[] crispRow = new Object[d.getAttributesCount()];
                    double[][] fuzzyValues = new double[d.getAttributesCount()][];
                    int k = 0;
                    //System.out.println("crispRow.length: " + crispRow.length);
                    for (int i = 0; i < crispRow.length; i++) {
                        crispRow[i] = "Dummy";
                        // fuzzyValues[i] = getFuzzyValues(data[i]);
                        fuzzyValues[i] = new double[d.getAttribute(i).getLinguisticTermsCount()];
                       // System.out.println("fuzzyValues[i].length " + fuzzyValues[i].length );
                        for (int j = 0; j < fuzzyValues[i].length; j++) {
                            fuzzyValues[i][j] = Double.parseDouble(data[k++]);
                       //     System.out.println("i:  " + i + " j: " + j + " k: " + k + " fuzzyValues[i][j]: " + fuzzyValues[i][j]);
                        }
                    }

                    d.addRow(new Row(crispRow, fuzzyValues));

                }
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        PreferenceMeasure preferenceMeasure;
        LeafDeterminer leafDeterminer;
        FuzzyDecisionTree descisionTree;
        preferenceMeasure = new AmbiguityMeasure(0.5);
        leafDeterminer = new LeafDeterminerBase(0.9);

        descisionTree = new FuzzyDecisionTree(preferenceMeasure, leafDeterminer);

        root = descisionTree.buildTree(d);


        // Uncomment to print fuzzy rules

        String[] rulesArray = descisionTree.generateRules(root);
        String rules = "======================================== \r\n ===================================== \r\n";
        for (String rule : rulesArray)
            rules += rule + "\r\n";
        File file = new File("PureEdgeSim/MyWork/readable_rules_" );
        try { DataOutputStream outstream = new DataOutputStream(new FileOutputStream(file, false));
            outstream.write(rules.getBytes()); outstream.close(); }
        catch (IOException e)
        { e.printStackTrace(); }



        return descisionTree;
    }

    private int DecisionTree(String[] architecture, Task task) throws Exception {
        System.out.println("task : " + task.getUid() + "in DecisionTree ");
        double max = -1;
        int vm = -1;
        int backupVM = -1;
        int edgeDateCenter = 0;
        double minDistance = maxDistance() * 3;
        String rule = "";
        String ruleValues = "";
        String backupRule = "";
        String backupRuleValues = "";
        double minResource = Double.POSITIVE_INFINITY;
        double maxResource = Double.NEGATIVE_INFINITY;
        List<Integer> accessableVMs = accessableVMs(task , architecture);

        double[] lat = getLat(task.getMaxLatency());
        double[] taskLength = getTaskLength(task.getLength());
        double[] wanUsage = getWanUsage(simulationManager.getNetworkModel().getWanUtilization());
        double[] userMobi = getMobi(task.getEdgeDevice().getMobilityManager().isMobile());
//        System.out.println("accessableVMs: " + accessableVMs.size());
//        System.out.println("orchestrationHistory.size(): " + orchestrationHistory.size());

        double random = new Random().nextFloat();
        if (random <= 6 / simulationManager.getSimulation().clock()) { // explore
            vm = accessableVMs.get(new Random().nextInt(accessableVMs.size()));
            exploration++;
            rule = generateRule(vm, task);
            ruleValues = generateRuleValues( vm , task);

//            System.out.println(" Rule  " + rule);
//            System.out.println(" Rule  " + ruleValues);
//            System.out.println(" Random decision:  " + vm + " for task: " +  task.getUid() + " exploration: " + exploration);

        } else {
            exploitation++;

            for (int i = 0; i < accessableVMs.size(); i++){
                int vmNum = accessableVMs.get(i);
//                System.out.println(" vMNum  " + vmNum + " i : " + i );
                double[] destcpuUsage = getDesCPUusage(
                            ((DataCenter) vmList.get(vmNum).getHost().getDatacenter()).getResources()
                                    .getCurrentCpuUtilization()); // I changed * 100 to / 10
//                System.out.println("destcpuUsage: " + destcpuUsage[0] + destcpuUsage[1] + destcpuUsage[2]);
                double[] destCpuMips =getDesCPU(((DataCenter) vmList.get(vmNum).getHost().getDatacenter())
                            .getResources().getTotalMips());
                double[] destRemai = getDestRemain((DataCenter) vmList.get(vmNum).getHost().getDatacenter());
                double[] desMobi = this.getDestMobi((DataCenter) vmList.get(vmNum).getHost().getDatacenter(),
                            task.getEdgeDevice().getMobilityManager().isMobile());
                    Dataset d = getDataset(lat, taskLength, wanUsage, userMobi, destcpuUsage, destCpuMips, destRemai, desMobi);


                    // Here we should send state to the python code
//                HttpClient client = HttpClient.newHttpClient();
//                String data = getJson(lat, taskLength, wanUsage, userMobi, destcpuUsage, destCpuMips, destRemai, desMobi);
//                System.out.println(data);
//                System.out.println(data.length());
//                URI uri = new URI("http://localhost:5000/predict");
//                HttpRequest request = HttpRequest.newBuilder().uri(uri).POST(HttpRequest.BodyPublishers.ofString(data)).build();
//                System.out.println("Received Response: " + client.sendAsync(request, HttpResponse.BodyHandlers.ofString()).get());
//                try {
//				    TimeUnit.SECONDS.sleep(
//						15);
//			        } catch (InterruptedException e) {
//				        e.printStackTrace();
//			        }
                double[] cVals = fuzzydecisionTree.classify(0, d, "Offload", fuzzydecisionTree.generateRules(root));

                    // to save the rule used to offload this task, this rule will be used later when
                    // updating the Q values
                    // the rule will be stored in the task metadata object for easy access
                rule = generateRule(vmNum , task);
                ruleValues = generateRuleValues(vmNum , task);
//                System.out.println(" Rule  " + rule);
//                System.out.println(" Rule  " + ruleValues);

                double distance = ((DataCenter) vmList.get(vmNum).getHost().getDatacenter()).getMobilityManager()
                        .distanceTo(task.getEdgeDevice());
                double checkResource = checkEnoughResource(task , vmNum);
//                    System.out.println(" checkResource: " + checkResource + " minResourcs:  " + minResource);
//                    System.out.println(" Distance to orchestrator  " + distance);

                if (maxResource < checkResource){
                    backupVM = vmNum;
                    maxResource = checkResource;
                    backupRule = rule;
                    backupRuleValues = ruleValues;
                }
//                System.out.println(" cVals  " + cVals[0] + cVals[1] + cVals[2] +cVals[3] + cVals[4] + cVals[5]);
                double high = cVals[0] + cVals[1];
                double medium = cVals[2] + cVals[3];
                double low = cVals[4] + cVals[5];
//                System.out.println(" Distance to orchestrator  " + distance);
                // if the class = high, high = the expected success rate is high or the
                // suitability of the device, so we should offload to this device
                // cVals is an array in which the fuzzified values are stored.
                // cVals[0]= the membership of degree of fuzzy set "high"
                // cVals{1] is for the fuzzy set "medium"
                // and cVals[2] is for the low one.
               // if (high >= medium && (max == -1 || max <= high + 2) && minDistance > distance) {
                if (high >= medium && (checkResource >= 0) &&  ((minResource == Double.POSITIVE_INFINITY ||
                            checkResource <= minResource) || minDistance > distance)){ // check distance just in case that the score is high.
                    // In medium or low it is better to assign to highest score
                // it keeps the highest VM
                    // if the membership degree of "high" is bigger than "medium"(
                    // cVals[0]>=cVals[1]) and if it is the first time (max==-1), or it is not the
                    // first time but the old max is below the membership degree of "high".
                    // then select the this device/vm as to execute the task
                    vm = vmNum;
                    minResource = checkResource;
                    minDistance = distance; ///?????
                    max = high + 2;
                    //System.out.println("task: " + task.getUid() + " vm1:" + vm);
                    // update the max value with the new membership degree of "high" fuzzy set
                    // if the wake it here, this means that we have found at least one device that
                    // is classifed as "high".This means that any other device that is classified as
                    // "medium" or "low" should be ignored. Since we are using a signle variable
                    // "max" to store the highest membership degree for any of those fuzzy sets
                    // (high, medium, low). We need to keep the superiority of "high" over "medium"
                    // and "medium" over "low".
                    // e.g., if the classification of a device gives 0.6 for the "high" fuzzy set.
                    // and the next one gives 1 but for "low" fuzzy set. In this case 0.6>1, to do
                    // this we simply add 2 to 0.6, so we will have 2.6 >
                    // 1.
                }// if the class = medium
                else if (medium >= low) {
                    if ((max == -1 || max <= medium + 1) && (checkResource >= 0) && minDistance > distance + maxDistance()) {
                        max = medium + 1; // I changed cVals[0] + 1 to cVals[1] + 1
                        vm = vmNum;
 //                       minDistance = distance;
//                        System.out.println("task: " + task.getUid() + " vm2:" + vm);
                    }
                } else {
                    // the class= low
                    if ((max == -1 || max < low) && (checkResource >= 0) && (minDistance > distance + (2 * maxDistance()))) {
                        max = low;
                        vm = vmNum;
                    //    minDistance = distance;
//                        System.out.println("task: " + task.getUid() + " vm3:" + vm);
                    }
                }
            }
        }

//        System.out.println("task: " + task.getUid() + " vm4:" + vm);
        if (vm != -1) {
//            System.out.println("task: " + task.getUid() + " vm5:" + vm);
            // save the rule in the task metadata for later use
            task.setMetaData(new String[] { rule, ruleValues });
            // return the offloading destination
            System.out.println("task: " + task.getUid() + " Dest Vm:" + vm);
            exploitation++;

            return vm;
        } else if (backupVM != -1) {
            task.setMetaData(new String[] { backupRule, backupRuleValues });
            System.out.println("task: " + task.getUid() + " Dest backup Vm:" + backupVM);
            return backupVM;
        } else
            return -1;
    }

    private double checkEnoughResource(Task task , int i ){
        double freeResource = (((DataCenter) vmList.get(i).getHost().getDatacenter())
                .getResources().getTotalMips()) * (1 - (((DataCenter) vmList.get(i).getHost().getDatacenter())
                .getResources().getCurrentCpuUtilization() / 100) );
        double minCapacity = (freeResource - (task.getLength() * 1.3));
        return  minCapacity;
    }

    private List<Integer> accessableVMs(Task task, String[] architecture){
        int vmNum = 0;
        List<Integer> accessableVMs =new ArrayList<Integer>();
        //int accessableVMs[] = new int[orchestrationHistory.size()];
        int edgeDateCenter = 0;
        for (int i = 0; i < orchestrationHistory.size(); i++) { //orchestrationHistory.size() is equal to
            // number of all VMs in the system
            if (offloadingIsPossible(task, vmList.get(i), architecture)
                    && vmList.get(i).getStorage().getCapacity() > 0) {
                if (((DataCenter) vmList.get(i).getHost().getDatacenter())
                        .getType() == SimulationParameters.TYPES.EDGE_DATACENTER) {
                    System.out.println(" VM " + i + " is Edge Data center ");
                    edgeDateCenter++;

                }
                accessableVMs.add(vmNum , i);
                vmNum++;
            }
        }
        return accessableVMs;
    }
    private String generateRule(int vm , Task task){
        String rule = "";
        rule = getLatTerm(task.getMaxLatency()) + ","
                + getWanUsageTerm(simulationManager.getNetworkModel().getWanUtilization()) + ","
                + getTaskLengthTerm(task.getLength()) + ","
                + (task.getEdgeDevice().getMobilityManager().isMobile() ? "high" : "low") + ","
                + getDesCPUusageTerm(((DataCenter) vmList.get(vm).getHost().getDatacenter())
                .getResources().getCurrentCpuUtilization())+","
                +getDesCPUTerm(((DataCenter) vmList.get(vm).getHost().getDatacenter())
                .getResources().getTotalMips()) + ","
                +getDestRemainTerm((DataCenter) vmList.get(vm).getHost().getDatacenter()) + ","
                + getDestMobiTerm((DataCenter) vmList.get(vm).getHost().getDatacenter(),
                task.getEdgeDevice().getMobilityManager().isMobile());
        return rule;
    }

    private String generateRuleValues( int vm, Task task){
        String ruleValues = "";
        ruleValues = task.getMaxLatency() + ","
                + simulationManager.getNetworkModel().getWanUtilization()+ ","
                + Double.toString(task.getLength())+ ","
                + (task.getEdgeDevice().getMobilityManager().isMobile() ? "high" : "low") + ","
                + ((DataCenter) vmList.get(vm).getHost().getDatacenter())
                .getResources().getCurrentCpuUtilization() +","
                + ((DataCenter) vmList.get(vm).getHost().getDatacenter())
                .getResources().getAvgCpuUtilization() +","
                +((DataCenter) vmList.get(vm).getHost().getDatacenter())
                .getResources().getTotalMips() + ","
                +getDestRemainValue (((DataCenter) vmList.get(vm).getHost().getDatacenter()))+ ","
                + (((DataCenter) vmList.get(vm).getHost().getDatacenter())
                .getMobilityManager().isMobile() ? "high" : "low");
        return ruleValues;
    }

    private String getJson(double[] lat, double[] wanusage, double[] tasklength, double[] mobi, double[] destcpuusage,
                               double[] destcpumips, double[] destremai, double[] desmobi) {
        Map<String, Object> data = new HashMap<>();
        data.put("lat", lat);
        data.put("wanusage", wanusage);
        data.put("taskLength", tasklength);
        data.put("mobi", mobi);
        data.put("destcpuusage", destcpuusage);
        data.put("destcpumips", destcpumips);
        data.put("destremai", destremai);
        data.put("desmobi", desmobi);

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(data);
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }



    private Dataset getDataset(double[] lat, double[] wanusage, double[] tasklength, double[] mobi, double[] destcpuusage,
                               double[] destcpumips, double[] destremai, double[] desmobi) {
        Dataset d = new Dataset("Sample1");

        // Add the attributes with Linguistic terms
        d.addAttribute(new Attribute("Latency", new String[] { "VeryHigh", "High", "UpperMedium", "LowerMedium", "Low", "VeryLow" }));
        d.addAttribute(new Attribute("WanUsage", new String[] { "VeryHigh", "High", "UpperMedium", "LowerMedium", "Low", "VeryLow" }));
        d.addAttribute(new Attribute("TaskLength", new String[] { "VeryHigh", "High", "UpperMedium", "LowerMedium", "Low", "VeryLow" }));
        d.addAttribute(new Attribute("Mob", new String[] { "VeryHigh", "High", "UpperMedium", "LowerMedium", "Low", "VeryLow" }));
        d.addAttribute(new Attribute("DestCPUUsage", new String[] { "VeryHigh", "High", "UpperMedium", "LowerMedium", "Low", "VeryLow" }));
        d.addAttribute(new Attribute("DestCPUMips", new String[] { "VeryHigh", "High", "UpperMedium", "LowerMedium", "Low", "VeryLow" }));
        d.addAttribute(new Attribute("DestRem", new String[] { "VeryHigh", "High", "UpperMedium", "LowerMedium", "Low", "VeryLow" }));
        d.addAttribute(new Attribute("DestMob", new String[] { "VeryHigh", "High", "UpperMedium", "LowerMedium", "Low", "VeryLow" }));
        d.addAttribute(new Attribute("Offload", new String[] { "VeryHigh", "High", "UpperMedium", "LowerMedium", "Low", "VeryLow" }));
        double[][] columns = { lat, wanusage,tasklength,mobi, destcpuusage, destcpumips, destremai, desmobi };

        d.addRow(new Row(new Object[] { "Dummy", "Dummy", "Dummy", "Dummy", "Dummy", "Dummy", "Dummy", "Dummy", "Dummy" }, columns));

        return d;
    }

//    private double[] getDestination(String destiantion) {
//        if (destiantion.equals("cloud"))
//            return new double[] { 1.0, 0.0, 0.0 };
//        else if (destiantion.equals("edge"))
//            return new double[] { 0.0, 1.0, 0.0 };
//        else
//            return new double[] { 0.0, 0.0, 1.0 };
//    }
    private double maxDistance() {
        return Math.abs(Math.sqrt(Math.pow((SimulationParameters.AREA_WIDTH / 2) , 2)
                + Math.pow(((SimulationParameters.AREA_LENGTH / 2) ), 2)));
}

    private double[] calculateFuzzyValue(double d, double vh, double h, double m, double l, double vl ) {
        if (d >= vh)
            return new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
        if (d >= h && d < vh)
            return new double[] { (d - h) / (vh - h), (vh - d) / (vh - h), 0.0, 0.0, 0.0, 0.0 };
        if (d > m && d < h)
            return new double[] {0.0, (d - m) / (h - m), (h - d) / (h - m), 0.0,0.0, 0.0};
        if (d == m)
            return new double[] { 0.0, 0.0, 1.0, 0.0, 0.0, 0.0 };
        if (d >= l && d < m)
            return new double[] { 0.0, 0.0, 0.0, (d - l) / (m - l), (m - d) / (m - l), 0.0 };
        if (d >= vl && d < l)
            return new double[] { 0.0, 0.0, 0.0, 0.0, (d - vl) / (l - vl), (l - d) / (l - vl) };
        if (d < vl)
            return new double[] { 0.0, 0.0, 0.0, 0.0,0.0, 1.0 };
        return null;
    }

    private String calculateFuzzyTerm(double d, double vh, double h, double m, double l, double vl ) {
        if (d >= vh)
            return "veryHigh";
        else if (d < vh && d >= h)
            return "high";
        else if (d < h && d >= m)
            return "upperMedium";
        else if (d < m && d >= l)
            return "lowerMedium";
        else if (d < l && d >= vl)
            return "low";
        else
            return "veryLow";
    }

    private double[] getDesCPU(double d) {
        double vl = 20000, l = 40000, m = 70000, h = 100000, vh = 1300000;
        return calculateFuzzyValue(d , vh, h, m, l, vl);
    }

    private String getDesCPUTerm(double mips) {
        double vl = 20000, l = 40000, m = 70000, h = 100000, vh = 1300000;
        return calculateFuzzyTerm(mips , vh, h, m, l, vl);
    }

    private String getDesCPUUsageTerm(double u) {
        if (u >= 4)
            return "high";
        else if (u <= 1)
            return "low";
        else
            return "medium";
    }

    private double[] getDestRemain(DataCenter datacenter) {
        double e = getDestRemainValue(datacenter);
        double vl = 15, l = 30, m = 50, h = 70, vh = 85;
        return calculateFuzzyValue(e, vh, h , m , l, vl);
//        double l = 0;
//        double h = 0;
//        double m = 0;
//        if (e <= 50) {
//            h = 0;
//            m = (e) / 50;
//            l = (50 - e) / 50;
//        } else {
//            h = (e - 50) / 50;
//            m = (100 - e) / 50;
//            l = 0;
//        }
//        return new double[] { h, m, l };
    }

    private String getDestRemainTerm(DataCenter datacenter) {
        double e = getDestRemainValue(datacenter);
        double vl = 15, l = 30, m = 50, h = 70, vh = 85;
        return calculateFuzzyTerm(e, vh, h , m , l, vl);
    }

    private double getDestRemainValue(DataCenter datacenter) {
        double e;
        if (datacenter.getEnergyModel().isBatteryPowered())
            e = datacenter.getEnergyModel().getBatteryLevelPercentage();
        else
            e = 100;
        return e;
    }

    private double[] getDesCPUusage(double d) {

        double vl = 20, l = 40, m = 60, h = 90, vh = 100;
        return calculateFuzzyValue(d, vh, h, m, l, vl );


    }
    private String getDesCPUusageTerm(double d) {
        double vl = 20, l = 40, m = 60, h = 90, vh = 100;
        return calculateFuzzyTerm(d, vh, h , m , l, vl);
    }

    private double[] getDestMobi(DataCenter datacenter, boolean mobile) {
        if (mobile && datacenter.getMobilityManager().isMobile())
            return new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
        else if (mobile || datacenter.getMobilityManager().isMobile())
            return new double[] { 0.0,0.0, 1.0, 0.0, 0.0, 0.0 };
        return new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0 };
    }

    private String getDestMobiTerm(DataCenter datacenter, boolean mobile) {
        if (mobile && datacenter.getMobilityManager().isMobile())
            return "veryHigh";
        else if (mobile || datacenter.getMobilityManager().isMobile())
            return "upperMedium";
        return "veryLow";
    }

    private double[] getMobi(boolean mobile) {
        if (mobile)
            return new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
        return new double[] { 0.0, 0.0, 0.0,0.0, 0.0, 1.0 };
    }

    private double[] getWanUsage(double d) {
        double vl = 3, l = 6, m = 10, h = 14, vh = 20;
        return calculateFuzzyValue(d, vh, h , m , l, vl);
    }

    private String getWanUsageTerm(double d) {
        double vl = 3, l = 6, m = 10, h = 14, vh = 20;
        return calculateFuzzyTerm(d, vh, h , m , l, vl);
    }

    private double[] getTaskLength(double d) {
        double vl = 2000, l = 5000, m = 10000, h = 20000, vh = 40000;
        return calculateFuzzyValue(d, vh, h , m , l, vl);
    }

    private String getTaskLengthTerm(double d) {
        double vl = 2000, l = 5000, m = 10000, h = 20000, vh = 40000;
        return calculateFuzzyTerm(d, vh, h , m , l, vl);
    }

    private double[] getLat(double maxLatency) {
        double vl = 15, l = 30, m = 60, h = 120, vh = 180;
        return calculateFuzzyValue(maxLatency, vh, h , m , l, vl);
    }

    private String getLatTerm(double maxLatency) {
        double vl = 15, l = 30, m = 60, h = 120, vh = 180;

        return calculateFuzzyTerm(maxLatency, vh, h , m , l, vl);
    }




    @Override
    public void resultsReturned(Task task) {
        try {
            // if the used algorithm is called DECISION_TREE_RL
            if (simulationManager.getScenario().getStringOrchAlgorithm().equals("FDT")) {
                // receive reinforcement
                reinforcementRecieved(task, task.getStatus() == Cloudlet.Status.SUCCESS, true); // why it be modified just when status is success?
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

    }


    /**
     * This function updates QS values and updates the decision tree. the Q table is
     * saved in a file and then used to generate the new tree, in order to get the
     * fuzzy rules, that are used in classification.
     */
    public void reinforcementRecieved(Task task, boolean reinforcement, boolean modify) throws IOException, InterruptedException {
        double delayPenalty = 0;
        if (!reinforcement){
            delayPenalty = task.getDelayTime() / task.getMaxLatency();
        }
//        System.out.println("delayPenalty: " + delayPenalty + "task: " + task.getUid());
        String ruleToUpdate = ((String[]) task.getMetaData())[0];
        String ruleToUpdateValues = ((String[]) task.getMetaData())[1];
//        System.out.println("ruleToUpdate11: " + ruleToUpdate + "task: " + task.getUid());
//        System.out.println("ruleToUpdateValues: " + ruleToUpdateValues + "task: " + task.getUid());
        System.out.println("action: " + (task.getVm().getHost().getDatacenter()).getId() + " task: " + task.getUid());

        String actionStates = task.getTime() + "," + task.getUid() + "," + ruleToUpdate + "," +  task.getEdgeDevice()+
                ","+ ((DataCenter) task.getVm().getHost().getDatacenter()).getId()+ "," + reinforcement+ "\r\n";
        String actionStatesValue = task.getTime() + "," + task.getUid() + "," + ruleToUpdateValues + ","
                +  task.getEdgeDevice()+ ","+ (task.getVm().getHost().getDatacenter()).getId()+ ","
                + reinforcement+ "\r\n";
        File file = new File("PureEdgeSim/MyWork/action-state_rule.csv");
        try { DataOutputStream outstream
                = new DataOutputStream(new FileOutputStream(file, true));
            outstream.write(actionStates.getBytes()); outstream.close(); } catch (IOException e)
        { e.printStackTrace(); }
        File file1 = new File("PureEdgeSim/MyWork/action-state_ruleValue.csv");
        try { DataOutputStream outstream
                = new DataOutputStream(new FileOutputStream(file1, true));
            outstream.write(actionStatesValue.getBytes()); outstream.close(); } catch (IOException e)
        { e.printStackTrace(); }

        SimulationParameters.TYPES action = ((DataCenter) task.getVm().getHost().getDatacenter()).getType();
        // Result should return to env
        updateQTable(Q_Table, ruleToUpdate, reinforcement ? 1 : 0, delayPenalty);
    }

    private void updateQTable( Vector<Q_table_row> q_Table, String ruleToUpdate, double reinforcement,
                               double delayPenalty) throws InterruptedException {
        // browse the Q_table, in order to update the Q value of that rule
        boolean found = false;
        double Q_value = reinforcement;
        double weightDone = 0.8;
        double weightCPU = 0.2;
        double avgCPU = avgCpuUtilization();

        for (int i = 0; i < q_Table.size(); i++) {
            if (q_Table.get(i).getRule().equals(ruleToUpdate)) {
                found = true;

                Q_value = q_Table.get(i).getQ_value();
                q_Table.get(i).incrementNumberOfReinforcements();
                double k = Math.min(q_Table.get(i).getNumberOfReinforcements(), 100);
                //Q_value += 1 / k * (reinforcement - Q_value);
                //Q_value += 1 / k * ((weightDone * reinforcement) + (weightCPU * avgCPU) - delayPenalty - Q_value);
                Q_value += 1 / k * (reinforcement - delayPenalty - Q_value);
                q_Table.get(i).setQ_value(Q_value);

                break;
            }
        }
        if (!found) {
            Q_table_row row = new Q_table_row(ruleToUpdate, 1, 1);
            q_Table.add(row);
        }
        //uncomment this part
//		 if (reinforcement == 1) System.out.println("ruleToUpdate:" + ruleToUpdate); else
//		 System.err.println("ruleToUpdate + Qvalue:" + ruleToUpdate + "   " + Q_value);


        try {
            UpdateFuzzyDecisionTree(q_Table);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void UpdateFuzzyDecisionTree(Vector<Q_table_row> q_Table) throws IOException {
        String rules = "";
            rules += "#Latency:VeryHigh High UpperMedium LowerMedium Low VeryLow\r\n"
                    + "#WanUsage:VeryHigh High UpperMedium LowerMedium Low VeryLow\r\n"
                    + "#TaskLength:VeryHigh High UpperMedium LowerMedium Low VeryLow\r\n"
                    + "#Mob:VeryHigh High UpperMedium LowerMedium Low VeryLow\r\n"
                    + "#DestCPUUsage:VeryHigh High UpperMedium LowerMedium Low VeryLow\r\n"
                    + "#DestCPUMips:VeryHigh High UpperMedium LowerMedium Low VeryLow\r\n"
                    + "#DestRem:VeryHigh High UpperMedium LowerMedium Low VeryLow\r\n"
                    + "#DestMob:VeryHigh High UpperMedium LowerMedium Low VeryLow\r\n"
                    + "@Offload:VeryHigh High UpperMedium LowerMedium Low VeryLow\r\n" + "=Data=";

        String readableRule = rules;
        for (int i = 0; i < q_Table.size(); i++) {
            rules += "\r\n" + convertRule(q_Table.get(i).getRule()) + " " + getClass(q_Table, q_Table.get(i));
            readableRule += "\r\n" + q_Table.get(i).getRule() + " " + getClass(q_Table, q_Table.get(i));
//			System.out.println(q_Table.get(i).getRule());
//			System.out.println(getClass(q_Table, q_Table.get(i), stage));
        }

        File file = new File("PureEdgeSim/MyWork/tree" + ".txt");
        DataOutputStream outstream = new DataOutputStream(new FileOutputStream(file, false));
        outstream.write(rules.getBytes());
        outstream.close();
        File file1 = new File("PureEdgeSim/MyWork/readabletree" + ".txt");
        DataOutputStream outstream1 = new DataOutputStream(new FileOutputStream(file1, false));
        outstream1.write(readableRule.getBytes());
        outstream.close();

    }

    private String convertRule(String rule) {
        return rule.replace("veryHigh", "1.0 0.0 0.0 0.0 0.0 0.0")
                .replace("high", "0.0 1.0 0.0 0.0 0.0 0.0")
                .replace("upperMedium", "0.0 0.0 1.0 0.0 0.0 0.0")
                .replace("lowerMedium", "0.0 0.0 0.0 1.0 0.0 0.0")
                .replace("low", "0.0 0.0 0.0 0.0 1.0 0.0")
                .replace("veryLow", "0.0 0.0 0.0 0.0 0.0 1.0")
                .replace(",", " ");
    }

    private String getClass(Vector<Q_table_row> q_Table, Q_table_row row) {
            return fuzzify(row.getQ_value());
    }

    private String fuzzify(double new_Q_value) {
        double vl = 0.1, l = 0.3, m = 0.5, h = 0.7, vh = 0.9;
        double[] fuzzyValue = calculateFuzzyValue(new_Q_value, vh, h , m , l, vl);
        return  fuzzyValue[0] + " " + fuzzyValue[1] + " " + fuzzyValue[2]+ " " +
                fuzzyValue[3] + " " + fuzzyValue[4] + " " + fuzzyValue[5];
    }

    private double avgCpuUtilization(){
        int edgeDevicesCount = 0;
        double averageMistCpuUtilization = 0;
        List<? extends DataCenter> datacentersList = simulationManager.getServersManager().getDatacenterList();
        for (DataCenter dc : datacentersList) {
             if (dc.getType() == SimulationParameters.TYPES.EDGE_DEVICE && dc.getVmList().size() > 0) {
                // only devices with computing capability
                // the devices that have no VM are considered simple sensors, and will not be counted here
                averageMistCpuUtilization += dc.getResources().getAvgCpuUtilization();
                edgeDevicesCount++;
            }

        }
        System.out.println("in Avgcpu fun: " + (averageMistCpuUtilization/ edgeDevicesCount));
        return (averageMistCpuUtilization/ edgeDevicesCount) / 100;
    }

}
