package MyWork;

import com.mechalikh.pureedgesim.DataCentersManager.DataCenter;
import com.mechalikh.pureedgesim.ScenarioManager.SimulationParameters;
import com.mechalikh.pureedgesim.SimulationManager.SimLog;
import com.mechalikh.pureedgesim.SimulationManager.SimulationManager;
import com.mechalikh.pureedgesim.TasksGenerator.Task;
import com.mechalikh.pureedgesim.TasksOrchestration.Orchestrator;
import net.sourceforge.jFuzzyLogic.FIS;
import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import org.json.JSONException;
import org.json.JSONObject;
import src.com.fdtkit.fuzzy.fuzzydt.FuzzyDecisionTree;
import src.com.fdtkit.fuzzy.fuzzydt.TreeNode;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CustomOrchestratorDeepEdge extends Orchestrator {
    private TreeNode root;
    FuzzyDecisionTree fuzzydecisionTree;
    FIS fis;
    Vector<Q_table_row> Q_Table = new Vector<Q_table_row>();
    public int receivedTask = 0;


    public CustomOrchestratorDeepEdge(SimulationManager simulationManager) throws IOException {
        super(simulationManager);
    }

    @Override
    protected int findVM(String[] architecture, Task task) {
        if (simulationManager.getScenario().getStringOrchAlgorithm().equals("Adworch"))
            try {
                System.out.println("lets find a Vm for task : " + task.getUid());
                
                return getRLResult( architecture, task);
            } catch (Exception e) {
                e.printStackTrace();
            }
        else if (simulationManager.getScenario().getStringOrchAlgorithm().equals("FUZZY_LOGIC")) {
            String fileName = "PureEdgeSim/MyWork/stage.fcl";
            fis = FIS.load(fileName, true);
            // Error while loading?
            if (fis == null) {
                System.err.println("Can't load file: '" + fileName + "'");
                return -1;
            }
            return FuzzyLogic(architecture, task);
        } else {
            SimLog.println("");
            SimLog.println("Custom Orchestrator- Unknnown orchestration algorithm '" + algorithm
                    + "', please check the simulation parameters file...");
            // Cancel the simulation
            Runtime.getRuntime().exit(0);
        }
        return -1;
    }

    private int getRLResult( String[] architecture, Task task) throws Exception{
//        System.out.println("task : " + task.getUid() + "in GetRLResult ");
        int vm = -1;
        String rule = "";
        String ruleValues = "";
        JSONObject state = new JSONObject();
        String action = null;
        receivedTask = receivedTask + 1;
        task.setTaskNumber(receivedTask);
        JSONObject fuzzifiedState = getFuzzifiedState(architecture, task);
        JSONObject ActualValueState = getActualValueState(architecture, task);
        state.put("SimulationTime", task.getTime());
        state.put("FuzzyTask", fuzzifiedState.get("task"));
        state.put("ActualTask", ActualValueState.get("task"));
        state.put("FuzzyVMs", fuzzifiedState.get("vms"));
        state.put("ActualVMs", ActualValueState.get("vms"));
        // send state by kafka producer
        // waiting in loop until receiving action
        String ack = ProducerConsumer.producer(state.toString(), "state");
        while(action == null) {
//            System.out.println(" waiting for action receive: " + action);
            action = ProducerConsumer.consumer("action");
        }
        String task_id = task.getUid().split("-", 3)[2];
        vm = actionProcessing(action , task_id);
        if (vm != -1 ) {
            rule = generateRule(vm, task);
            ruleValues = generateRuleValues(vm, task);
            task.setMetaData(new String[]{rule, ruleValues});
        }

        return vm;
    }

    private JSONObject getFuzzifiedState( String[] architecture, Task task) throws Exception {

        int offloadable = 0;
        JSONObject fuzzyState = new JSONObject();
        String task_id = task.getUid().split("-", 3)[2];
//        System.out.println("task_id " + task_id);
        List<Integer> taskProp = new ArrayList<>();
        taskProp.add((int) task.getTaskNumber());
        taskProp.add(Integer.valueOf(task_id));
        taskProp.add(getTaskLengthTerm(task.getLength()));
        taskProp.add(getLatTerm(task.getMaxLatency()));
        taskProp.add(getMobiTerm(task.getEdgeDevice().getMobilityManager().isMobile()));
        taskProp.add((task.getDone()? 1: 0 ));
        fuzzyState.put("task",String.valueOf(taskProp));

        List<String> vmsList = new ArrayList<>();
        for (int i = 0; i < orchestrationHistory.size(); i++) { //orchestrationHistory.size() is equal to
            // number of all VMs in the system
            if (vmList.get(i).getStorage().getCapacity() > 0 && ((DataCenter) vmList.get(i).getHost().getDatacenter())
                    .getType() == SimulationParameters.TYPES.EDGE_DEVICE) {
                //System.out.println(" VM " + i + " is Edge Data center ");
                List<Integer> vmProperties = new ArrayList<>();
                vmProperties.add(getDesCPUusageTerm(((DataCenter) vmList.get(i).getHost().getDatacenter()).getResources()
                        .getCurrentCpuUtilization()));
                vmProperties.add(getDesCPUTerm(((DataCenter) vmList.get(i).getHost().getDatacenter())
                        .getResources().getTotalMips()));
                vmProperties.add(getDestRemainTerm((DataCenter) vmList.get(i).getHost().getDatacenter()));
                vmProperties.add(this.getDestMobiTerm((DataCenter) vmList.get(i).getHost().getDatacenter(),
                        task.getEdgeDevice().getMobilityManager().isMobile()));
                vmProperties.add(Integer.valueOf(vmList.get(i).getUid().split("-")[1]));
                if (offloadingIsPossible(task, vmList.get(i), architecture))
                    offloadable = 1;
                else
                    offloadable = 0;
                vmProperties.add(offloadable);
               // System.out.println("vmProperties: " + vmProperties);
                vmsList.add(String.valueOf(vmProperties));
            }
        }

        fuzzyState.put("vms",String.valueOf(vmsList));
//        System.out.println(" FuzzyState : " + fuzzyState);
        return fuzzyState;
    }
    private JSONObject getActualValueState( String[] architecture, Task task) throws Exception {

        Double offloadable;
        JSONObject actualState = new JSONObject();
        String task_id = task.getUid().split("-", 3)[2];
//        System.out.println("task_id " + task_id);
        List<Double> taskProp = new ArrayList<>();
        taskProp.add((double) task.getTaskNumber());
        taskProp.add(Double.valueOf(task_id));
        taskProp.add((double) task.getLength());
        taskProp.add(task.getMaxLatency());
        taskProp.add(task.getEdgeDevice().getMobilityManager().isMobile()? 1.0 : 3.0);
        taskProp.add((task.getDone()? 1.0: 0.0 ));
        actualState.put("task",String.valueOf(taskProp));

        List<String> vmsList = new ArrayList<>();
        for (int i = 0; i < orchestrationHistory.size(); i++) { //orchestrationHistory.size() is equal to
            // number of all VMs in the system
            if (vmList.get(i).getStorage().getCapacity() > 0 && ((DataCenter) vmList.get(i).getHost().getDatacenter())
                    .getType() == SimulationParameters.TYPES.EDGE_DEVICE) {
                //System.out.println(" VM " + i + " is Edge Data center ");
                List<Double> vmProperties = new ArrayList<>();
                vmProperties.add(((DataCenter) vmList.get(i).getHost().getDatacenter()).getResources()
                        .getCurrentCpuUtilization());
                vmProperties.add(((DataCenter) vmList.get(i).getHost().getDatacenter())
                        .getResources().getTotalMips());
                vmProperties.add(getDestRemainValue((DataCenter) vmList.get(i).getHost().getDatacenter()));
                vmProperties.add((double) getDestMobiTerm((DataCenter) vmList.get(i).getHost().getDatacenter(),
                        task.getEdgeDevice().getMobilityManager().isMobile()));
                vmProperties.add(Double.valueOf(vmList.get(i).getUid().split("-")[1]));
                if (offloadingIsPossible(task, vmList.get(i), architecture))
                    offloadable = 1.0;
                else
                    offloadable = 0.0;
                vmProperties.add(offloadable);
                // System.out.println("vmProperties: " + vmProperties);
                vmsList.add(String.valueOf(vmProperties));
            }
        }
        actualState.put("vms",String.valueOf(vmsList));
//        System.out.println(" ActualState : " + actualState);
        return actualState;
    }
    private int  actionProcessing( String action , String task_id) throws JSONException {
        action = action.substring(1, action.length() - 1);
        action = action.replace("\\","");
        JSONObject actionJson= new JSONObject(action);
        String actionTaskID = actionJson.get("taskID").toString();
        String actionString = actionJson.get("action").toString();
        //System.out.println(" actionString : " + actionString);
        Integer vm = Integer.valueOf(actionString);
        if(!task_id.equals(actionTaskID)) {
            System.err.println("The taskID is not matched");
//            System.out.println(" received task_id: " + actionTaskID);
//            System.out.println(" current task_id : " + task_id);

        }

        //System.out.println(" vm : " + vm);
//        System.out.println(" vmactionTaskID : " + actionTaskID);
        return vm;
    }
    public static <T, U> List<U> convertStringListTodoubleList(List<T> listOfString, Function<T, U> function)
    {
        return listOfString.stream()
                .map(function)
                .collect(Collectors.toList());
    }

    private String generateRule(int vm , Task task){
        String rule = "";
        rule =  getLatTerm(task.getMaxLatency()) + ","
                + getWanUsageTerm(simulationManager.getNetworkModel().getWanUtilization()) + ","
                + getTaskLengthTerm(task.getLength()) + ","
                + (task.getEdgeDevice().getMobilityManager().isMobile() ? 1 : 3) + ","
                + getDesCPUusageTerm(((DataCenter) vmList.get(vm).getHost().getDatacenter())
                .getResources().getCurrentCpuUtilization())+","
                +getDesCPUTerm(((DataCenter) vmList.get(vm).getHost().getDatacenter())
                .getResources().getTotalMips()) + ","
                +getDestRemainTerm((DataCenter) vmList.get(vm).getHost().getDatacenter()) + ","
                + getDestMobiTerm((DataCenter) vmList.get(vm).getHost().getDatacenter(),
                task.getEdgeDevice().getMobilityManager().isMobile());
        return rule;
    }

    private String generateRuleValues( int vm, Task task){
        String ruleValues = "";
        ruleValues = task.getMaxLatency() + ","
                + simulationManager.getNetworkModel().getWanUtilization()+ ","
                + Double.toString(task.getLength())+ ","
                + (task.getEdgeDevice().getMobilityManager().isMobile() ? 1 : 3) + ","
                + ((DataCenter) vmList.get(vm).getHost().getDatacenter())
                .getResources().getCurrentCpuUtilization() +","
                + ((DataCenter) vmList.get(vm).getHost().getDatacenter())
                .getResources().getAvgCpuUtilization() +","
                +((DataCenter) vmList.get(vm).getHost().getDatacenter())
                .getResources().getTotalMips() + ","
                +getDestRemainValue (((DataCenter) vmList.get(vm).getHost().getDatacenter()))+ ","
                + (((DataCenter) vmList.get(vm).getHost().getDatacenter())
                .getMobilityManager().isMobile() ? 1 : 3);
        return ruleValues;
    }
    
    
    private double maxDistance() {
        return Math.abs(Math.sqrt(Math.pow((SimulationParameters.AREA_WIDTH / 2) , 2)
                + Math.pow(((SimulationParameters.AREA_LENGTH / 2) ), 2)));
}



    private int calculateFuzzyTerm(double d, double h, double m, double l) {
        if (d >= h)
            return 1; // High
        else if (d < h && d >= m)
            return 2; // Medium
        else
            return 3; // low
    }

    private int getDesCPUTerm(double mips) {
        double l = 40000, m = 70000, h = 100000;
        return calculateFuzzyTerm(mips , h, m, l);
    }

    private int getDesCPUUsageTerm(double u) {
        if (u >= 4)
            return 1; //"High" 
        else if (u <= 1)
            return 2; //"Low"
        else
            return 3; //"Medium"
    }

    private int getDestRemainTerm(DataCenter datacenter) {
        double e = getDestRemainValue(datacenter);
        double  l = 30, m = 50, h = 70;
        return calculateFuzzyTerm(e, h , m , l);
    }

    private double getDestRemainValue(DataCenter datacenter) {
        double e;
        if (datacenter.getEnergyModel().isBatteryPowered())
            e = datacenter.getEnergyModel().getBatteryLevelPercentage();
        else
            e = 100;
        return e;
    }

    private int getDesCPUusageTerm(double d) {
        double l = 40, m = 60, h = 90;
        return calculateFuzzyTerm(d, h , m , l);
    }

    private int getDestMobiTerm(DataCenter datacenter, boolean mobile) {
        if (mobile && datacenter.getMobilityManager().isMobile())
            return 1; //High
        else if (mobile || datacenter.getMobilityManager().isMobile())
            return 2; //Medium
        return 3; //Low
    }

    private int getMobiTerm(boolean mobile) {
        if (mobile)
            return 1;
        return 3;
    }

    private int getWanUsageTerm(double d) {
        double l = 6, m = 10, h = 14;
        return calculateFuzzyTerm(d, h , m , l);
    }

    private int getTaskLengthTerm(double d) {
        double l = 5000, m = 10000, h = 20000;
        return calculateFuzzyTerm(d, h , m , l);
    }

    private int getLatTerm(double maxLatency) {
        double l = 30, m = 60, h = 120;
        return calculateFuzzyTerm(maxLatency, h , m , l);
    }

    @Override
    public void resultsReturned(Task task) {
        try {
            // if the used algorithm is called DECISION_TREE_RL
            if (simulationManager.getScenario().getStringOrchAlgorithm().equals("Adworch")) {
                // receive reinforcement
                reinforcementRecieved(task, task.getStatus() == Cloudlet.Status.SUCCESS, true); // why it be modified just when status is success?
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void reinforcementRecieved(Task task, boolean reinforcement, boolean modify) throws IOException, InterruptedException, JSONException {
        double delayPenalty = 0;
        if (!reinforcement){
            delayPenalty = task.getDelayTime() / task.getMaxLatency();
        }
        // get first rule from the array (the rule of stage 1)
        String ruleToUpdate = ((String[]) task.getMetaData())[0];
        String ruleToUpdateValues = ((String[]) task.getMetaData())[1];
        // send reward to kafka reward topic by a producer
//        System.out.println("ruleToUpdate11: " + ruleToUpdate + "task: " + task.getUid());
//        System.out.println("ruleToUpdate22: " + ruleToUpdateValues + "task: " + task.getUid());
       // System.out.println("action: " + ((DataCenter) task.getVm().getHost().getDatacenter()).getType() + "task: " + task.getUid() );

        String actionStates = task.getTime() + "," + task.getUid() + "," + ruleToUpdate + "," +  task.getEdgeDevice()+
                ","+ ((DataCenter) task.getVm().getHost().getDatacenter()).getId()+ "," + reinforcement+ "\r\n";
        String actionStatesValue = task.getTime() + "," + task.getUid() + "," + ruleToUpdateValues + ","
                +  task.getEdgeDevice()+ ","+ (task.getVm().getHost().getDatacenter()).getId()+ ","
                + reinforcement+ "\r\n";
        File file = new File("PureEdgeSim/MyWork/action-state_rule.csv");
        try { DataOutputStream outstream
                = new DataOutputStream(new FileOutputStream(file, true));
            outstream.write(actionStates.getBytes()); outstream.close(); } catch (IOException e)
        { e.printStackTrace(); }
        File file1 = new File("PureEdgeSim/MyWork/action-state_ruleValue.csv");
        try { DataOutputStream outstream
                = new DataOutputStream(new FileOutputStream(file1, true));
            outstream.write(actionStatesValue.getBytes()); outstream.close(); } catch (IOException e)
        { e.printStackTrace(); }

        int length = task.getUid().split("-", 3).length;
        String taskID = task.getUid().split("-", 3)[length-1];
//        System.out.println("taskID int: " + length + "---" + taskID);

        JSONObject reward = new JSONObject();
        reward.put("taskID", taskID);
        reward.put("taskNum", task.getTaskNumber());
        reward.put("reward", (reinforcement ? 1: 0));
        //System.out.println("RewardString: " + reward);
        String totalReward = ProducerConsumer.producer(reward.toString(), "reward");
        //System.out.println("totalReward: " + totalReward);
    }


    private int FuzzyLogic(String[] architecture, Task task) {
        //System.out.println(" Task: " + task.getUid() + " is in FuzzyLogic with architecture " + architecture.toString());

        double min = -1;
        int vm = -1;

        fis.setVariable("wan", simulationManager.getNetworkModel().getWanUtilization());
        fis.setVariable("tasklength", task.getLength());
        fis.setVariable("delay", task.getMaxLatency());
       // fis.setVariable("vm", vmUsage * 10 / count); //10?
        String[] architecture2 = { "Mist" };
        for (int i = 0; i < orchestrationHistory.size(); i++) {
            if (offloadingIsPossible(task, vmList.get(i), architecture2)
                    && vmList.get(i).getStorage().getCapacity() > 0) {
                vmList.get(i).getHost().getId();
                if (!task.getEdgeDevice().getMobilityManager().isMobile())
                    fis.setVariable("vm_local", 0);
                else
                    fis.setVariable("vm_local", 0);
                fis.setVariable("vm", (1 - vmList.get(i).getCpuPercentUtilization()) * vmList.get(i).getMips() / 1000);
                fis.evaluate();
//                System.out.println("Task: " + task.getUid() + " vm variable : " + fis.getVariable("vm")
//                        + " vm variable : " + fis.getVariable("vm_local"));
//                System.out.println( "offload : " + fis.getVariable("offload"));

                if (min == -1 || min > fis.getVariable("offload").defuzzify()) {
                    min = fis.getVariable("offload").defuzzify();
                    //System.out.println( "min : " + min);
                    vm = i;
                }
            }
        }
        System.out.println("stag2 : Dest VM : " +  vm);
        return vm;

    }

}
