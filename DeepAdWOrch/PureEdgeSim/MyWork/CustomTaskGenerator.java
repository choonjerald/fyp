package MyWork;
/**
 *     PureEdgeSim:  A Simulation Framework for Performance Evaluation of Cloud, Edge and Mist Computing Environments
 *
 *     This file is part of PureEdgeSim Project.
 *
 *     PureEdgeSim is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PureEdgeSim is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PureEdgeSim. If not, see <http://www.gnu.org/licenses/>.
 *
 *     @author Mechalikh
 **/

import com.mechalikh.pureedgesim.ScenarioManager.SimulationParameters;
import com.mechalikh.pureedgesim.SimulationManager.SimulationManager;
import com.mechalikh.pureedgesim.TasksGenerator.Task;
import com.mechalikh.pureedgesim.TasksGenerator.TasksGenerator;
import com.opencsv.CSVWriter;
import org.cloudbus.cloudsim.utilizationmodels.UtilizationModel;
import org.cloudbus.cloudsim.utilizationmodels.UtilizationModelDynamic;
import org.cloudbus.cloudsim.utilizationmodels.UtilizationModelFull;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class CustomTaskGenerator extends TasksGenerator {
    //List<Integer> tasktime =new ArrayList<Integer>();
    int [] tasktime = new int[20000];
    int maxTime = 0;
    int lastTaskID;
    //Map<Integer,Integer> tasktime = new HashMap<Integer,Integer>();
    public CustomTaskGenerator(SimulationManager simulationManager) {
        super(simulationManager);
    }
    double simulationTime = (SimulationParameters.SIMULATION_TIME - SimulationParameters.INITIALIZATION_TIME) / 60;
    public List<Task> generate() {
        System.out.println("Task generator pattern");
        // get simulation time in minutes (excluding the initialization time)
//        double simulationTime = (SimulationParameters.SIMULATION_TIME - SimulationParameters.INITIALIZATION_TIME) / 60;
        System.out.println("simulationTime: " + simulationTime);
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (int dev = 0; dev < datacentersList.size(); dev++) { // for each device
            if (datacentersList.get(dev).isGeneratingTasks()) {

                int app = new Random().nextInt(SimulationParameters.APPLICATIONS_LIST.size()); // pickup a random
                // application type for
                // every device
                //for (int app = 0 ; app < SimulationParameters.APPLICATIONS_LIST.size(); app++ ) {
                    datacentersList.get(dev).setApplication(app); // assign this application to that device
                    System.out.println("device: " + dev + "app: " + app);
                    for (int st = 0; st < simulationTime; st++) { // for each minute
                        // generating tasks
                        // first get time in seconds
                        int time = st * 60;
                        System.out.println("time1: " + time);
                        // Then pick up random second in this minute "st"
                        // Shift the time by the defined value "INITIALIZATION_TIME"
                        // in order to start after generating all the resources
                        time += new Random().nextInt(15) + SimulationParameters.INITIALIZATION_TIME;
                        System.out.println("time2: " + time);
                        try {
                            TimeUnit.SECONDS.sleep(0);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        insert(time, app, dev, st);
                    }
                }
            }
      //  }
        for( int ts = 0; ts < taskList.size(); ts++){
            //System.out.println("taskList.get(ts).getId()" + taskList.get(ts).getId());
            if (taskList.get(ts).getId() == lastTaskID) {
                taskList.get(ts).setDone(true);
                System.out.println("last taskid is  " + taskList.get(ts).getUid() +
                        "and done is " + taskList.get(ts).getDone());
                break;
            }
        }
       // taskList.get(taskList.size()-1).setDone(true);
//        System.out.println("last taskid is  " + taskList.get(taskList.size()-1).getUid() +
//                "and done is " + taskList.get(taskList.size()-1).getDone());
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("TaskTime: " + Arrays.toString(tasktime));
        //tasktime.stream().forEach(System.out::println);
        File workload = new File("PureEdgeSim/MyWork/workload.csv");
        try {
            // create FileWriter object with file as parameter
            FileWriter outputfile = new FileWriter(workload);

            // create CSVWriter object filewriter object as parameter
            CSVWriter writer = new CSVWriter(outputfile);

            // adding header to csv
            String[] header = { "Second", "TaskCount" };
            writer.writeNext(header);

            // add data to csv
            for (int i = 0; i < tasktime.length; i++) {
                String[] data1 = {Integer.toString(i), Integer.toString(tasktime[i])};
                writer.writeNext(data1);
            }

            // closing writer connection
            writer.close();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return this.getTaskList();
    }

    private void insert(int time, int app, int dev, int st) {
        System.out.println("Insert " +
                "--- time: " + time + " app: " + app + " dev: " + dev  + " st: " + st);
        int time2 = time;
        double maxLatency = SimulationParameters.APPLICATIONS_LIST.get(app).getLatency(); // Load length from
        // application file
        long length = (long) SimulationParameters.APPLICATIONS_LIST.get(app).getTaskLength(); // Load length from
        // application file
        long requestSize = SimulationParameters.APPLICATIONS_LIST.get(app).getRequestSize();
        long outputSize = SimulationParameters.APPLICATIONS_LIST.get(app).getResultsSize();
        int pesNumber = SimulationParameters.APPLICATIONS_LIST.get(app).getNumberOfCores();
        long containerSize = SimulationParameters.APPLICATIONS_LIST.get(app).getContainerSize(); // the size of the
        // container
        int appType = SimulationParameters.APPLICATIONS_LIST.get(app).getAppType();
        Task[] task = new Task[(int)(Math.ceil(SimulationParameters.APPLICATIONS_LIST.get(app).getRate() * 4.5))]; // 1 + 2 + 1.5
        int id;
        System.out.println(" length: " + length + " maxLatency: " + maxLatency + " requestSize: " + requestSize + " outputSize: " + outputSize +
                " pesNumber: " + pesNumber + " containerSize: " + containerSize +  " apptype: " + appType);

        // generate tasks for every edge device
        int rate = SimulationParameters.APPLICATIONS_LIST.get(app).getRate() ;
 //       rate = (int) (rate + Math.ceil(rate * 0.15));
//
        System.out.println(" simulationTime * (2/3) " + simulationTime * 2 /3 + "simulationTime * (1/3)" + simulationTime / 3);
        if (st > simulationTime * 2/3) {
            rate = (int) Math.ceil(rate * 1.5);
            System.out.println(" rate1 " + rate);
        }
        else if (st >= (int)(simulationTime / 3)  && st <= (int)(simulationTime * 2/3)){
            rate *= 2;
            System.out.println(" rate2: " + rate);
        }
        System.out.println(" rate3: " + rate);

        for (int i = 0; i < rate; i++) {
            id = taskList.size();
            UtilizationModel utilizationModeldynamic = new UtilizationModelDynamic();
            task[i] = new Task(id, length, pesNumber);
            task[i].setFileSize(requestSize).setOutputSize(outputSize).setUtilizationModelBw(utilizationModeldynamic)
                    .setUtilizationModelRam(utilizationModeldynamic).setUtilizationModelCpu(new UtilizationModelFull());
            time2 += 60 / rate ;
            tasktime[time2]++;
//            try {
//                tasktime.get(time2);
//            } catch ( IndexOutOfBoundsException e ) {
//                tasktime.put(time2 ,0);
//            }
//            int temp = (tasktime.get(time2));
//            tasktime.put(time2 , temp++);
            task[i].setTime(time2);
            task[i].setContainerSize(containerSize);
            task[i].setMaxLatency(maxLatency);
            task[i].setEdgeDevice(datacentersList.get(dev)); // the device that generate this task (the origin)
            task[i].setRegistry(datacentersList.get(0)); // set the cloud as registry
            task[i].setDone(false);
            taskList.add(task[i]);
            if(time2 >= maxTime){
                maxTime = time2;
                lastTaskID = id;
            }
            getSimulationManager().getSimulationLogger()
                    .deepLog("BasicTasksGenerator, Task " + id + "type " + appType + " with execution time " + time2 + " (s) generated.");
            System.out.println("BasicTasksGenerator, Task "  + id + " type " + appType + " with execution time " + time2 + " (s) generated.");

        }
    }

}



