package MyWork;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class State{
    @JsonProperty
    public List<Integer> task;

    @JsonProperty
    public List<List> vms;

    @JsonProperty
    public long timeStamp;

    public State() {}

    public void setTask(List<Integer> task) {
        this.task = task;
    }

    public void setTimeStamp() {
        this.timeStamp = System.currentTimeMillis();
    }

    public void setVms(List<List> vms) {
        this.vms = vms;
    }
}


