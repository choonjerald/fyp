/**
 *     PureEdgeSim:  A Simulation Framework for Performance Evaluation of Cloud, Edge and Mist Computing Environments 
 *
 *     This file is part of PureEdgeSim Project.
 *
 *     PureEdgeSim is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PureEdgeSim is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PureEdgeSim. If not, see <http://www.gnu.org/licenses/>.
 *     
 *     @author Mechalikh
 **/
package com.mechalikh.pureedgesim.TasksOrchestration;

import com.mechalikh.pureedgesim.DataCentersManager.DataCenter;
import com.mechalikh.pureedgesim.ScenarioManager.SimulationParameters;
import com.mechalikh.pureedgesim.ScenarioManager.SimulationParameters.TYPES;
import com.mechalikh.pureedgesim.SimulationManager.SimLog;
import com.mechalikh.pureedgesim.SimulationManager.SimulationManager;
import com.mechalikh.pureedgesim.TasksGenerator.Task;
import org.cloudbus.cloudsim.cloudlets.Cloudlet;
import org.cloudbus.cloudsim.vms.Vm;

import java.util.ArrayList;
import java.util.List;

public abstract class Orchestrator {
	protected List<List<Integer>> orchestrationHistory;
	protected List<List<Task>> queuedTasks;
	protected List<Vm> vmList;
	protected SimulationManager simulationManager;
	protected SimLog simLog;
	protected String algorithm;
	protected String architecture;

	public Orchestrator(SimulationManager simulationManager) {
		this.simulationManager = simulationManager;
		simLog = simulationManager.getSimulationLogger();
		orchestrationHistory = new ArrayList<>();
		queuedTasks = new ArrayList<>();
		vmList = simulationManager.getServersManager().getVmList();
		algorithm = simulationManager.getScenario().getStringOrchAlgorithm();
		architecture = simulationManager.getScenario().getStringOrchArchitecture();
		initHistoryList(vmList.size());
	}

	private void initHistoryList(int size) {
		for (int vm = 0; vm < size; vm++) {
			// Creating a list to store the orchestration history for each VM (virtual
			// machine)
			orchestrationHistory.add(new ArrayList<>());
			queuedTasks.add(new ArrayList<>());

		}
	}

	public void initialize(Task task) {
		if ("CLOUD_ONLY".equals(architecture)) {
			cloudOnly(task);
		} else if ("MIST_ONLY".equals(architecture)) {
			mistOnly(task);
		} else if ("EDGE_AND_CLOUD".equals(architecture)) {
			edgeAndCloud(task);
		} else if ("ALL".equals(architecture)) {
			all(task);
		} else if ("EDGE_ONLY".equals(architecture)) {
			edgeOnly(task);
		} else if ("MIST_AND_CLOUD".equals(architecture)) {
			mistAndCloud(task);
		}
	}

	// If the orchestration scenario is MIST_ONLY send Tasks only to edge devices
	private void mistOnly(Task task) {
		String[] Architecture = { "Mist" };
		sendTask(task, findVM(Architecture, task));
	}

	// If the orchestration scenario is ClOUD_ONLY send Tasks (cloudlets) only to
	// cloud virtual machines (vms)
	private void cloudOnly(Task task) {
		String[] Architecture = { "Cloud" };
		sendTask(task, findVM(Architecture, task));
	}

	// If the orchestration scenario is EDGE_AND_CLOUD send Tasks only to edge data
	// centers or cloud virtual machines (vms)
	private void edgeAndCloud(Task task) {
		String[] Architecture = { "Cloud", "Edge" };
		sendTask(task, findVM(Architecture, task));
	}

	// If the orchestration scenario is MIST_AND_CLOUD send Tasks only to edge
	// devices or cloud virtual machines (vms)
	private void mistAndCloud(Task task) {
		String[] Architecture = { "Cloud", "Mist" };
		sendTask(task, findVM(Architecture, task));
	}

	// If the orchestration scenario is EDGE_ONLY send Tasks only to edge data
	// centers
	private void edgeOnly(Task task) {
		String[] Architecture = { "Edge" };
		sendTask(task, findVM(Architecture, task));
	}

	// If the orchestration scenario is ALL send Tasks to any virtual machine (vm)
	// or device
	private void all(Task task) {
		String[] Architecture = { "Cloud", "Edge", "Mist" };
		sendTask(task, findVM(Architecture, task));
	}

	protected abstract int findVM(String[] architecture, Task task);

	protected void sendTask(Task task, int vm) {
		// assign the tasks to the vm found
		assignTaskToVm(vm, task);

		// Offload it only if resources are available (i.e. the offloading destination
		// is available)
		if (task.getVm() != Vm.NULL) // Send the task to execute it
			task.getEdgeDevice().getVmTaskMap().add(new VmTaskMapItem((Vm) task.getVm(), task));
	}

	protected void assignTaskToVm(int vmIndex, Task task) {
		if (vmIndex == -1) {
			simLog.incrementTasksFailedLackOfRessources(task);
		} else {
			task.setVm(vmList.get(vmIndex)); // send this task to this vm
			simLog.deepLog(simulationManager.getSimulation().clock() + " : EdgeOrchestrator, Task: " + task.getId()
					+ "(task length: " + task.getLength() + " ) assigned to " + ((DataCenter) vmList.get(vmIndex).getHost().getDatacenter()).getType() + " vm: "
					+ vmList.get(vmIndex).getId());
//			System.out.println(simulationManager.getSimulation().clock() + " : EdgeOrchestrator, Task: " + task.getId()
//					+ "(task length: " + task.getLength() + " ) assigned to " + ((DataCenter) vmList.get(vmIndex).getHost().getDatacenter()).getType() + " vm: "
//					+ vmList.get(vmIndex).getId());
			//System.out.println( "Orch_appType:" + task.getApplicationType());
			//System.out.println( "Orch_max:" + task.getLength());

			// update history
			orchestrationHistory.get(vmIndex).add((int) task.getId());
			queuedTasks.get(vmIndex).add((task));
			task.setStatus(Cloudlet.Status.QUEUED);
//			System.out.println(" assign to vm status " + task.getStatus());
			//
			//
			//checkFailedTasks(vmIndex);
//			try {
//				TimeUnit.SECONDS.sleep(1);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}

		}
	}

	public void checkFailedTasks( int vmIdex) {
		List<Task> removableTasks = new ArrayList<>();

		for (int i = 0; i < queuedTasks.get(vmIdex).size(); i++) {
			Task task = queuedTasks.get(vmIdex).get(i);

//			System.out.println("Before: " + task.getStatus());

			if (task.getStatus() == Cloudlet.Status.FAILED || task.getStatus() == Cloudlet.Status.SUCCESS )
				removableTasks.add(task);

				if ((simulationManager.getSimulation().clock() - task.getTime()) > task.getMaxLatency() &&
						(task.getStatus() != Cloudlet.Status.FAILED || task.getStatus() != Cloudlet.Status.SUCCESS)) {

				task.setFailureReason(Task.Status.FAILED_DUE_TO_LATENCY);
				task.setDelayTime((simulationManager.getSimulation().clock() - task.getTime()) - task.getMaxLatency());
				simLog.incrementTasksFailedLatency(task);
//				System.out.println("task " + task.getUid() + "is failed due to latency " + "delay =  " + task.getDelayTime());
//				System.out.println(" simulationManager.getSimulation().clock():  " + simulationManager.getSimulation().clock() +
//						           " task.getTime(): " + task.getTime() +
//						           " task.getMaxLatency(): " + task.getMaxLatency());

				boolean taskStatus = simulationManager.setFailed(task);
				if (taskStatus == true){
					removableTasks.add(task);
				}
			}
//			System.out.println("After: " + queuedTasks.get(vmIdex).get(i).getStatus());
		}
		for (int j = 0; j < removableTasks.size(); j++){
			queuedTasks.get(vmIdex).remove(removableTasks.get(j));
		}
//		for (int k = 0; k < queuedTasks.get(vmIdex).size(); k++){
//
////			System.out.println("task " + queuedTasks.get(vmIdex).get(k).getUid() +
////					" Status: " + queuedTasks.get(vmIdex).get(k).getStatus() +
////					" delay =  " + queuedTasks.get(vmIdex).get(k).getDelayTime());
////			System.out.println("task.getSimulation().clock():  " +
////					queuedTasks.get(vmIdex).get(k).getSimulation().clock() +
////					" simulationManager.getSimulation().clock()" +
////					simulationManager.getSimulation().clock() +
////					" task.getTime(): " + queuedTasks.get(vmIdex).get(k).getTime() +
////					" task.getMaxLatency(): " + queuedTasks.get(vmIdex).get(k).getMaxLatency());
//		}
	}

	protected boolean sameLocation(DataCenter device1, DataCenter device2, int RANGE , Task task) {
		if (device2.getType() == TYPES.CLOUD)
			return true;
		double distance = device1.getMobilityManager().distanceTo(device2);
//		System.out.println(" offloadingIsPossible: Distance between " + device1 + " and " + device2 + " is " + distance + " for Task " + task.getUid() );
		return (distance < RANGE);
	}

	protected boolean arrayContains(String[] Architecture, String value) {
		for (String s : Architecture) {
			if (s.equals(value))
				return true;
		}
		return false;
	}

	protected boolean offloadingIsPossible(Task task, Vm vm, String[] architecture) {
		//System.out.println("check the offloading feasibility for task " + task.getUid() + " and VM " + vm );
		SimulationParameters.TYPES vmType = ((DataCenter) vm.getHost().getDatacenter()).getType();
		return ((arrayContains(architecture, "Cloud") && vmType == SimulationParameters.TYPES.CLOUD) // cloud computing
				|| (arrayContains(architecture, "Edge") && vmType == SimulationParameters.TYPES.EDGE_DATACENTER // Edge
																												// computing
				// compare destination (edge data center) location and origin (edge device)
				// location, if they
				// are in same area offload to his device
						&& (sameLocation(((DataCenter) vm.getHost().getDatacenter()), task.getEdgeDevice(),
								SimulationParameters.EDGE_DATACENTERS_RANGE , task)
								// or compare the location of their orchestrators
								|| (SimulationParameters.ENABLE_ORCHESTRATORS
										&& sameLocation(((DataCenter) vm.getHost().getDatacenter()),
												task.getOrchestrator(), SimulationParameters.EDGE_DATACENTERS_RANGE, task))))

				|| (arrayContains(architecture, "Mist") && vmType == SimulationParameters.TYPES.EDGE_DEVICE // Mist
																											// computing
				// compare destination (edge device) location and origin (edge device) location,
				// if they are in same area offload to this device
						&& (sameLocation(((DataCenter) vm.getHost().getDatacenter()), task.getEdgeDevice(),
								SimulationParameters.EDGE_DEVICES_RANGE , task)
								// or compare the location of their orchestrators
								|| (SimulationParameters.ENABLE_ORCHESTRATORS
										&& sameLocation(((DataCenter) vm.getHost().getDatacenter()),
												task.getOrchestrator(), SimulationParameters.EDGE_DEVICES_RANGE, task)))
						&& !((DataCenter) vm.getHost().getDatacenter()).isDead()));
	}

	public abstract void resultsReturned(Task task);

}
