package RunForResults;

import MyWork.Main;


import java.io.IOException;
import java.nio.file.*;
import java.util.Arrays;
import java.util.List;


public class MultipleRun {
    public static void main(String[] args) throws IOException {


        for(int i = 1; i <= 3; i++) {
            String path = "PureEdgeSim/MyWork/output/Percom/NoMask/";
            System.out.println("Iteration: " + i);
            Main.main(args);
            String dir = getLastDirectory(path);
            System.out.println("dir: " + dir);
            copyAllFiles(path + dir);
        }
    }
    public static String getLastDirectory(String directoryFilePath)
    {
        Path dir = Paths.get(directoryFilePath);
        Path newestDir =Paths.get("");
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
            for (Path file: stream) {
                System.out.println("file: " + file.getFileName().toString() );
                if ( file.toFile().isDirectory()){
                       // && newestDir.toString().compareToIgnoreCase(file.getFileName().toString()) < 0){
                    newestDir = file.getFileName();
                }
            }
            System.out.println(newestDir.toString());

        } catch (IOException | DirectoryIteratorException x) {
            // IOException can never be thrown by the iteration.
            // In this snippet, it can only be thrown by newDirectoryStream.
            System.err.println(x);
        }

        return newestDir.toString();
    }

    public static void copyAllFiles(String dir) throws IOException {
        List<String> files = Arrays.asList("action-state_rule.csv", "action-state_ruleValue.csv", "Failed_tasks.csv",
                "readableTree.txt", "tree.txt", "workload.csv");
        for (String file : files) {
            Path source = Paths.get("PureEdgeSim/MyWork/" + file);
            if (source.toFile().isFile()) {
                Path newDir = Paths.get(dir);
                System.out.println("newDir: " + newDir);
                Files.move(source, newDir.resolve(source.getFileName()),
                        StandardCopyOption.REPLACE_EXISTING);
            }
        }

    }
}


