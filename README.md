# FYP
>DQN for task orchestration

# Download UbuntuVM on Oracle VM VirtualBox Manager

Pull this codebase within an Ubuntu virtual machine in order to run the program

# Docker Commands

To start the docker in detatched mode `docker-compose -f docker-compose.yml up -d`

To stop the docker `docker-compose -f docker-compose.yml down`

Check running images `docker ps`

Execute docker container `docker exec -it <CONTAINER ID> /bin/sh`

### When in Kafka Container
Navigate to `/opt/kafka/bin` to use kafka scripts.

**Creating a topic in kafka** 
`kafka-topics.sh --create --topic <TOPIC_NAME> --bootstrap-server localhost:9092 --replication-factor 1 --partitions <PARTITION_SIZE>`

**Check list of topics** 
`kafka-topics.sh --list --bootstrap-server localhost:9092`

**To create a producer** 
`kafka-console-producer.sh --topic <TOPIC_NAME> --bootstrap-server localhost:9092`

**To create a consumer** 
`kafka-console-consumer.sh --topic <TOPIC_NAME> --bootstrap-server localhost:9092`
>In order to create a consumer, open a new terminal to execute another docker container

# Kafka Python 
> Ensure that a kafka topic is created

# Run the DQN after the docker is started
> Run `python DQN.py`

# Run the DeepAdWOrch java simulator
> Go to PureEdgeSim/MyWork and run main.java
